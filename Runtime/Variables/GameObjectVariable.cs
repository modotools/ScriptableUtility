using Core.Unity.Attributes;
using UnityEngine;


namespace ScriptableUtility.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/GameObject", fileName = nameof(GameObjectVariable))]
    public class GameObjectVariable : ScriptableVariable<GameObject>, IOverrideMapping
    {
        public void AddContext(IContext context, OverrideValue value) => AddContext(context, (GameObject) value.ObjectValue);
    }
}
