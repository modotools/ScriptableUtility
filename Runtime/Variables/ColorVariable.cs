using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility.OverrideValueConverter;


namespace ScriptableUtility.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/Color", fileName = nameof(ColorVariable))]
    public class ColorVariable : ScriptableVariable<Color>, IOverrideMapping
    {
        public void AddContext(IContext context, OverrideValue value) 
            => AddContext(context, ColorOverrideValueConverter.Get(value));
    }
}
