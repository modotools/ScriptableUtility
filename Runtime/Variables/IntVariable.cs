using Core.Unity.Attributes;
using UnityEngine;
using ScriptableUtility.OverrideValueConverter;


namespace ScriptableUtility.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/Int", fileName = nameof(IntVariable))]
    public class IntVariable : ScriptableVariable<int>, IOverrideMapping
    {
        public void AddContext(IContext context, OverrideValue value) 
            => AddContext(context, IntOverrideValueConverter.Get(value));
    }
}
