using Core.Unity.Attributes;
using UnityEngine;


namespace ScriptableUtility.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/AudioClip", fileName = nameof(AudioClipVariable))]
    public class AudioClipVariable : ScriptableVariable<AudioClip>, IOverrideMapping
    {
        public void AddContext(IContext context, OverrideValue value) => AddContext(context, (AudioClip) value.ObjectValue);
    }
}
