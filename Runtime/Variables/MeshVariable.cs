using Core.Unity.Attributes;
using UnityEngine;


namespace ScriptableUtility.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/Mesh", fileName = nameof(MeshVariable))]
    public class MeshVariable : ScriptableVariable<Mesh>, IOverrideMapping
    {
        public void AddContext(IContext context, OverrideValue value) => AddContext(context, (Mesh) value.ObjectValue);
    }
}
