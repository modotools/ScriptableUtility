using Core.Unity.Attributes;
using UnityEngine;


namespace ScriptableUtility.Variables
{
    [EditorIcon("icon-variable")]
    [CreateAssetMenu(menuName = "Scriptable/Variables/Texture", fileName = nameof(TextureVariable))]
    public class TextureVariable : ScriptableVariable<Texture>, IOverrideMapping
    {
        public void AddContext(IContext context, OverrideValue value) => AddContext(context, (Texture) value.ObjectValue);
    }
}
