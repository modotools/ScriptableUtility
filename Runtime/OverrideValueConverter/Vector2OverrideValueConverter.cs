﻿using UnityEngine;

namespace ScriptableUtility.OverrideValueConverter
{
    public class Vector2OverrideValueConverter : OverrideValueConverter<Vector2>
    {
        public static Vector2 Get(OverrideValue value)
        {
            var vars = value.StringValue.Split(',');
            return vars.Length < 2 ? default : new Vector2(float.Parse(vars[0]), float.Parse(vars[1]));
        }

        public override Vector2 GetValue(OverrideValue value) => Get(value);
        public override OverrideValue GetValue(Vector2 value) => new OverrideValue() {StringValue = $"{value.x},{value.y}"};
    }
}