﻿using Core.Unity.Types;
using JetBrains.Annotations;

namespace ScriptableUtility.OverrideValueConverter
{
    [UsedImplicitly]
    public class EnumOverrideValueConverter : OverrideValueConverter<SerializedEnum>
    {
        public static SerializedEnum Get(OverrideValue value) 
        {
            var vars = value.StringValue.Split('$');
            return vars.Length < 3 
                ? default 
                : new SerializedEnum(new ClassTypeReference(vars[0]), int.Parse(vars[1]), vars[2]);
        }
        
        public override SerializedEnum GetValue(OverrideValue value) => Get(value);
        public override OverrideValue GetValue(SerializedEnum value)
            => new OverrideValue() { StringValue = $"{value.Type}${value.Value}${value.StringValue}" };
    }
}