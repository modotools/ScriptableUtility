﻿using Core.Extensions;
using JetBrains.Annotations;

namespace ScriptableUtility.OverrideValueConverter
{
    [UsedImplicitly]
    public class BoolOverrideValueConverter : OverrideValueConverter<bool>
    {
        public static bool Get(OverrideValue value)=> 
            !value.StringValue.IsNullOrEmpty() && bool.Parse(value.StringValue);

        public override bool GetValue(OverrideValue value) => Get(value);
        public override OverrideValue GetValue(bool value)
            => new OverrideValue() {StringValue = value.ToString()};
    }
}