﻿using JetBrains.Annotations;
using UnityEngine;

namespace ScriptableUtility.OverrideValueConverter
{
    [UsedImplicitly]
    public class RectOverrideValueConverter : OverrideValueConverter<Rect>
    {
        public static Rect Get(OverrideValue value)
        {
            var vars = value.StringValue.Split(',');
            if (vars.Length < 4)
                return default;
            return new Rect(float.Parse(vars[0]), float.Parse(vars[1]), float.Parse(vars[2]),
                float.Parse(vars[3]));
        }

        public override Rect GetValue(OverrideValue value) => Get(value);
        public override OverrideValue GetValue(Rect value)
            => new OverrideValue() { StringValue = $"{value.x},{value.y},{value.width},{value.height}" };
    }
}
