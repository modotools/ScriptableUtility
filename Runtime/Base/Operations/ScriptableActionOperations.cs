﻿using System.Collections.Generic;
using ScriptableUtility.ActionConfigs;

namespace ScriptableUtility.Operations
{
    public static class ScriptableActionOperations
    {
        static void GetAllActionsRecursively(this IActionConfig action, ICollection<IActionConfig> actions)
        {
            actions.Add(action);
            if (!(action is ILinkActionConfigs acCon))
                return;
            foreach (var a in acCon.GetActionConfigsLinked())
                a.GetAllActionsRecursively(actions);
        }
    }
}