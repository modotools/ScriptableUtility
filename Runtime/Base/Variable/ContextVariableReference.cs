﻿using Core.Interface;
using UnityEngine;
using nodegraph;
using nodegraph.Operations;

namespace ScriptableUtility
{
    public interface IContextVariableReference<T>: IContextVariableReference, IVar<T>
    {
        T IntrinsicValue { get; set; }
        ScriptableVariable<T> Scriptable { get; }
    }

    public static class ContextVariableReferenceExtension
    {
        public static bool IsNone<T>(this IVar<T> var)
        {
            if (!(var is IContextVariableReference<T> refVar))
                return false;

            return (refVar.ContextType != ReferenceContextType.Intrinsic && refVar.Scriptable == null);
        }

        public static T GetValue<T>(this IContextVariableReference<T> var)
        {
            if (var.ContextType == ReferenceContextType.Intrinsic)
                return var.IntrinsicValue;

            if (var.Scriptable == null)
            {
                //Debug.Log($"Scriptable null for variable of type {var.VariableType}_!");
                return default;
            }

            if (var.Scriptable.ContextType == ScriptableContextType.GlobalContext)
                return var.Scriptable.GlobalValue;

            var.Get(out var context);
            if (context != null)
                return var.Scriptable.GetWithContext(context);

            Debug.LogError($"Context null for variable of scriptable {var.Scriptable.name}!");
            return default;
        }

        public static IContextVariableReference<T> SetValueInternal<T>(this IContextVariableReference<T> var, T value)
        {
            if (var.ContextType == ReferenceContextType.Intrinsic)
            {
                var.IntrinsicValue = value;
                return var;
            }

            if (var.Scriptable == null)
            {
                //Debug.LogError("Scriptable null!");
                return var;
            }
            var.Get(out var context);

            if (var.Scriptable.ContextType == ScriptableContextType.GlobalContext)
                var.Scriptable.GlobalValue = value;
            else if (context != null)
                var.Scriptable.SetWithContext(context, value);
            else
                Debug.LogError("Context null!");
            
            return var;
        }

        public static TRef InitInternal<TRef, T>(this TRef @ref, IContext ctx) where TRef : IContextVariableReference<T>
        {
            if (@ref.ContextType != ReferenceContextType.Scriptable)
                return @ref;

            if (@ref.Scriptable == null)
            {
                //Debug.LogError("Scriptable null!");
                return @ref;
            }

            if (@ref.Scriptable.ContextType == ScriptableContextType.ObjectContext)
                @ref.SetContextProvider(ctx);

            return @ref;
        }

        public static TRef InitInternal<TRef, T>(this TRef @ref, string fieldName, INode n,
            IContext ctx) where TRef : IContextVariableReference<T>
        {
            if (n.TryGetInputValue(fieldName, out TRef fromInput))
                return fromInput;

            @ref.InitInternal<TRef, T>(ctx);
            return @ref;
        }
    }

}