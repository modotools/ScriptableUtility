﻿using System;
using System.Collections.Generic;
using Core.Unity.Attributes;
using Core.Unity.Interface;
using ScriptableUtility.Events;
using UnityEngine;

namespace ScriptableUtility
{
    [EditorIcon("icon-variable")]
    public class ScriptableVariable<T> : ScriptableEvent<T>, IScriptableVariable
    {
        public Type VariableType => typeof(T);
        public ScriptableContextType ContextType => m_contextType;

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] T m_value;

        [SerializeField] internal ScriptableContextType m_contextType;
        [SerializeField] ScriptableObject m_parent;
#pragma warning restore 0649 // wrong warnings for SerializeField

        readonly Dictionary<IContext, T> m_contextData = new Dictionary<IContext, T>();
        T m_globalValue;

        void OnEnable()
        {
            //Debug.Log("On Enable " + name);
            m_contextData.Clear();
            m_globalValue = GetInitialValue();
        }

        T GetInitialValue() => m_value;

        public T GlobalValue
        {
            get => m_globalValue;
            set
            {
                m_globalValue = value;
                RaiseGlobalEvent(m_globalValue);
            }
        }

        public ref T GetGlobalByRef() => ref m_globalValue;

        public override void AddContext(IContext context)
        {
            //Debug.Log($"Add {Name} to {context.Name} {m_contextData.Count}");
            if (!m_contextData.ContainsKey(context))
                m_contextData.Add(context, GetInitialValue());
            //Debug.Log($"Added {Name} to {context.Name} {m_contextData.Count}");

            base.AddContext(context);
        }

        protected void AddContext(IContext context, T value)
        {
            //Debug.Log($"AddContext with value {name} {value} {m_contextData.Count}");
            if (!m_contextData.ContainsKey(context))
                m_contextData.Add(context, value);
            else
                Debug.LogError($"Variable {name} was already initialized with a value. Unclear how to proceed.");

            base.AddContext(context);
        }

        public override void RemoveContext(IContext context)
        {
            m_contextData.Remove(context);
            base.RemoveContext(context);
        }

        public T GetWithContext(IContext context) // Debug.Log($"GetWithContext {context.Name} {m_contextData.Count}");
            => m_contextData[context];

        public void SetWithContext(IContext context, T value)
        {
            //Debug.Log($"SetWithContext with value {name} {value} {m_contextData.Count}");
            m_contextData[context] = value;
            RaiseEvent(context, value);
        }

#if UNITY_EDITOR
        public static string Editor_ValuePropName => nameof(m_value);
#endif
    }
}