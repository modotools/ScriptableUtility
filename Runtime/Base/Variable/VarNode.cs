﻿using Core.Interface;
using UnityEngine;
using nodegraph;
using nodegraph.Attributes;
using nodegraph.Data;
using nodegraph.Operations;
using ScriptableUtility.VariableNodes;

namespace ScriptableUtility
{
    [NodeWidth(300)]
    public abstract class VarNode<T> : Node, IFullNode
    {
        public override string Name => m_varRef != null ? m_varRef.Name : typeof(T).Name;

        [Input, SerializeField] protected ContextConnector m_contextIN;

        [Output(ConnectionType.Multiple, ShowBackingValue.Always), SerializeField] 
        protected VarReference<T> m_varRef;

        public override INodeData NodeData => this;
        public INode Node => this;

        public override object GetOutputValue(PortID port)
        {
            return this.TryGetInputValue<IProvider<IContext>>(nameof(m_contextIN), out var ctxProv) 
                ? new VarReference<T>(ctxProv, m_varRef) 
                : m_varRef;
        }
    }
}
