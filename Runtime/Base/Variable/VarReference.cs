using System;
using System.Collections.Generic;
using Core.Interface;
using UnityEngine;
using nodegraph;
using nodegraph.Operations;

namespace ScriptableUtility
{
    [Serializable]
    public class VarReference<T> : IContextVariableReference<T>, INamed
    {
        #region Constructors
        public VarReference()
        {
            m_contextType = ReferenceContextType.Scriptable;
            m_scriptable = null;
            m_contextProvider = null;
            m_intrinsicValue = default;
        }
        public VarReference(T value)
        {
            m_contextType = ReferenceContextType.Intrinsic;

            m_scriptable = null;
            m_contextProvider = null;
            m_intrinsicValue = value;
        }
        public VarReference(IProvider<IContext> ctx, ScriptableVariable<T> value)
        {
            m_contextType = ReferenceContextType.Scriptable;

            m_scriptable = value;
            m_contextProvider = new RefIContextProvider() { Result = ctx };
            m_intrinsicValue = default;
        }
        public VarReference(ScriptableVariable<T> scriptable)
        {
            m_contextType = ReferenceContextType.Scriptable;

            m_scriptable = scriptable;
            m_contextProvider = null;
            m_intrinsicValue = default;
        }
        public VarReference(VarReference<T> otherRef)
        {
            m_contextType = otherRef.m_contextType;

            m_scriptable = otherRef.m_scriptable;
            otherRef.Get(out var ctx);
            m_contextProvider = new RefIContextProvider() { Result = ctx };
            m_intrinsicValue = otherRef.IntrinsicValue;
        }
        public VarReference(IProvider<IContext> ctx, VarReference<T> otherRef)
        {
            m_contextType = otherRef.m_contextType;

            m_scriptable = otherRef.m_scriptable;
            m_contextProvider = new RefIContextProvider() { Result = ctx };
            m_intrinsicValue = otherRef.IntrinsicValue;
        }
        #endregion

        #region Serialized Fields
        [SerializeField] ReferenceContextType m_contextType;
        [SerializeField] ScriptableVariable<T> m_scriptable;
        [SerializeField] RefIContextProvider m_contextProvider;
        [SerializeField] T m_intrinsicValue;
        //IContext m_context;
        #endregion

        #region Properties
        public string Name => Scriptable ? Scriptable.name : VariableType.Name; 
        public Type VariableType => typeof(T);
        public T Value
        {
            get
            {
                if (ContextType == ReferenceContextType.Intrinsic)
                    return m_intrinsicValue;

                if (Scriptable == null)
                {
                    //Debug.Log($"Scriptable null for variable of type {var.VariableType}_!");
                    return default;
                }

                if (Scriptable.ContextType == ScriptableContextType.GlobalContext)
                    return Scriptable.GlobalValue;

                var ctx = Context;
                if (ctx != null) // Debug.Log(ctx.Name);
                    return Scriptable.GetWithContext(ctx);
                Debug.LogError($"Context null for variable of scriptable {Scriptable.name}!");
                return default;
            }
            set
            {
                if (ContextType == ReferenceContextType.Intrinsic)
                {
                    m_intrinsicValue = value;
                    return;
                }

                if (Scriptable == null)
                {
                    //Debug.LogError("Scriptable null!");
                    return;
                }

                IContext ctx;
                if (Scriptable.ContextType == ScriptableContextType.GlobalContext)
                    Scriptable.GlobalValue = value;
                else if ((ctx = Context) != null)
                    Scriptable.SetWithContext(ctx, value);
                else
                    Debug.LogError("Context null!");
            }
        }
        public object ValueObject
        {
            get => Value;
            set => Value = (T) value;
        }
        public T IntrinsicValue
        {
            get => m_intrinsicValue;
            set => m_intrinsicValue = value;
        }
        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public ReferenceContextType ContextType => m_contextType;
        public ScriptableVariable<T> Scriptable => m_scriptable;

        public IContext Context
        {
            get
            {
                var cp = m_contextProvider?.Result;
                if (cp == null)
                    return null;

                cp.Get(out var ctx);
                return ctx;
            }
        }
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter
        #endregion

        public void Get(out IContext provided) => provided = Context;

        public IProvider<IContext> GetContextProvider() => m_contextProvider?.Result;

        public void SetContextProvider(IProvider<IContext> c)
        {
            if (m_contextProvider != null)
                m_contextProvider.Result = c;
            else m_contextProvider = new RefIContextProvider() {Result = c};
        }

        public static implicit operator T(VarReference<T> reference) =>
            reference.Value;
        public static implicit operator VarReference<T>(T val) =>
            new VarReference<T>(val);

#if UNITY_EDITOR
        public static string Editor_ContextTypePropName => nameof(m_contextType);
        public static string Editor_ScriptablePropName => nameof(m_scriptable);
        public static string Editor_ContextProvPropName => nameof(m_contextProvider);
        public static string Editor_IntrinsicPropName => nameof(m_intrinsicValue);
#endif
    }

    public static class VarReferenceExt
    {
        // another way would be to always sync assetRef with the var-node already in editor
        public static IVar<T> Init<T>(this VarReference<T> assetRef, string fieldName, INode n)
        {
            // assetVariables can't store context but they can have multiple provider, default is the main-graph-asset itself
            // when we really want f.e. to link enemy-HP from specific enemy, we need to have different contextProvider for that variable as sub-asset
            // all contexts are then set before the action-tree is build with Init.
            // So variables are cloned, VarReferences on asset map to asset-based contextProvider,
            // & instantiated variables map to the runtime context.
            if (n != null && n.TryGetInputValue(fieldName, out VarReference<T> fromInput))
                assetRef = fromInput;

            // when we pool our parent-objects, instantiating variables shouldn't be a problem:
            var newRef = new VarReference<T>(assetRef); 
            return newRef;
            
            // another option would be to pool our variables also and copy values from assetRef:

            //SimplePool<VarReference<T>>.I.Get();
            //newRef.__SetContext(ctx);
        }

        public static IVar<T>[] Init<T>(this IList<VarReference<T>> assetRefs, string fieldName, INode n)
        {
            var vars = new IVar<T>[assetRefs.Count];
            for (var i = 0; i < assetRefs.Count; i++)
                vars[i] = assetRefs[i].Init($"{fieldName}[{i}]", n);
            return vars;
        }
    }
}