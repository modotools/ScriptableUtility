﻿using System;
using Core.Unity.Attributes;
using UnityEngine;
//using UnityEngine.Playables;
using nodegraph;
using nodegraph.Attributes;

namespace ScriptableUtility.ActionConfigs
{
    [EditorIcon("icon-action")]
    [NodeWidth(400)]
    public abstract class ScriptableBaseAction : ScriptableObject, IActionConfig, IOptionalNode, IGUIData
    {
        [HideInInspector]
        public bool Enabled = true;

        [HideInInspector]
        public string DebugName;

        public abstract string Name { get; }
        // please also implement static StaticFactoryType
        public abstract Type FactoryType { get; }
        public virtual INode Node => m_node as INode;
        public void SetNode(INode n) => m_node = (ScriptableObject) n;

        #region GUI Data
        [HideInInspector]
        public GUIData GUIData = GUIData.Default;

        public bool UseColor
        {
            get => GUIData.UseColor;
            set => GUIData.UseColor = value;
        }
        public Color GUIColor
        {
            get => GUIData.GUIColor;
            set => GUIData.GUIColor = value;
        }
        public Texture2D Icon
        {
            get => GUIData.Icon;
            set => GUIData.Icon = value;
        }
        #endregion

        #pragma warning disable 0649
        [SerializeField, HideInInspector] ScriptableObject m_node;
        #pragma warning restore 0649

        public abstract IBaseAction CreateAction();

        [Input(ConnectionType.Multiple), HideInInspector] public ScriptableActionConnector IN;
        //public override Playable CreatePlayable(PlayableGraph graph, UnityEngine.GameObject owner)
        //{
        //    var scriptPlayable = ScriptPlayable<PlayableAction>.Create(graph);
        //    var action = CreateAction(new Context(owner));
        //    scriptPlayable.GetBehaviour().SetAction(action);
        //    return scriptPlayable;
        //}
    }
}
