﻿using System.Collections.Generic;
using Core.Unity.Interface;
using UnityEngine;

namespace ScriptableUtility.Events
{
    [CreateAssetMenu(menuName = "Scriptable/Events/Default")]
    public class ScriptableEvent : ScriptableObject, IScriptableEvent<IScriptableEventListenerDefault>
    {
        public string Name => name;

        readonly Dictionary<IContext, List<IScriptableEventListenerDefault>> m_contextListeners 
            = new Dictionary<IContext, List<IScriptableEventListenerDefault>>();

        readonly List<IScriptableEventListenerDefault> m_globalListeners = new List<IScriptableEventListenerDefault>();

        public void AddContext(IContext context) => m_contextListeners.Add(context, new List<IScriptableEventListenerDefault>());
        public void RemoveContext(IContext context) => m_contextListeners.Remove(context);

        public void RegisterListener(IContext context, IScriptableEventListenerDefault listener)
            => m_contextListeners[context].Add(listener);
        public void UnregisterListener(IContext context, IScriptableEventListenerDefault listener)
            => m_contextListeners[context].Remove(listener);

        public void RegisterListenerGlobal(IScriptableEventListenerDefault listener)
            => m_globalListeners.Add(listener);
        public void UnregisterListenerGlobal(IScriptableEventListenerDefault listener)
            => m_globalListeners.Remove(listener);

        public void RaiseGlobalEvent()
        {
            foreach (var listener in m_globalListeners)
                listener?.OnEvent();
        }

        public void RaiseEvent(IContext context)
        {
            var listeners = m_contextListeners[context];
            foreach (var listener in listeners)
                listener?.OnEvent();
        }
    }

    public class ScriptableEvent<T> : ScriptableObject, IScriptableEvent<IScriptableEventListener<T>>
    {
        public string Name => name;

        readonly Dictionary<IContext, List<IScriptableEventListener<T>>> m_contextListeners
            = new Dictionary<IContext, List<IScriptableEventListener<T>>>();

        readonly List<IScriptableEventListener<T>> m_globalListeners 
            = new List<IScriptableEventListener<T>>();
        public virtual void AddContext(IContext context) => m_contextListeners.Add(context, new List<IScriptableEventListener<T>>());
        public virtual void RemoveContext(IContext context) => m_contextListeners.Remove(context);

        public void RegisterListener(IContext context, IScriptableEventListener<T> listener)
        {
            if (!m_contextListeners.TryGetValue(context, out var ctxListener))
                return;
            ctxListener.Add(listener);
        }

        public void UnregisterListener(IContext context, IScriptableEventListener<T> listener)
        {
            if (!m_contextListeners.TryGetValue(context, out var ctxListener))
                return;
            ctxListener.Remove(listener);
        }

        public void RegisterListenerGlobal(IScriptableEventListener<T> listener)
            => m_globalListeners.Add(listener);
        public void UnregisterListenerGlobal(IScriptableEventListener<T> listener)
            => m_globalListeners.Remove(listener);

        protected void RaiseGlobalEvent(T value)
        {
            foreach (var listener in m_globalListeners)
                listener?.OnEvent(value);
        }

        protected void RaiseEvent(IContext context, T value)
        {
            if (!m_contextListeners.TryGetValue(context, out var listeners))
                return;

            foreach (var listener in listeners)
                listener?.OnEvent(value);
        }
    }

    public interface IScriptableEvent : IScriptableObject, IMultiContextMapping {}
    public interface IScriptableEvent<in TListener> : IScriptableEvent where TListener : IScriptableEventListener
    {
        void RegisterListener(IContext context, TListener listener);
        void UnregisterListener(IContext context, TListener listener);
        void RegisterListenerGlobal(TListener listener);
        void UnregisterListenerGlobal(TListener listener);
    }
}