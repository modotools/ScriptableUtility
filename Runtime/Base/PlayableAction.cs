﻿using ScriptableUtility.Actions;
using UnityEngine.Playables;

namespace ScriptableUtility.ActionConfigs
{
    // todo 0: test actions as playable
    public class PlayableAction : PlayableBehaviour
    {
        IBaseAction m_action;

        public void SetAction(IBaseAction action) => m_action = action;
    
        public override void OnBehaviourPlay(Playable playable, FrameData info)
        {
            base.OnBehaviourPlay(playable, info);

            if (m_action is IDefaultAction defAction)
                defAction.Invoke();
            if (m_action is IUpdatingAction updatingAction)
                updatingAction.Init();
        }

        public override void OnBehaviourPause(Playable playable, FrameData info)
        {
            base.OnBehaviourPause(playable, info);
            //todo 0: test if this occurs
            if (m_action is IStoppableAction stoppableAction)
                stoppableAction.Stop();
        }

        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            if (m_action is IUpdatingAction updateAction)
                updateAction.Update(info.deltaTime);
        }
    }
}
