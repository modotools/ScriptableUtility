using System;

namespace ScriptableUtility
{
    [Serializable]
    public struct OverrideValue
    {
        public string StringValue;
        public UnityEngine.Object ObjectValue;
    }
}