using System;
using Core.Interface;
using Core.Unity.Types;

namespace ScriptableUtility
{
    public interface IContext : IProvider<IContext>, IVariableContainer, INamed
    {
        void Init();
    }

    [Serializable]
    public class RefIContext : InterfaceContainer<IContext> { }
    [Serializable]
    public class RefIContextProvider : InterfaceContainer<IProvider<IContext>> { }
}