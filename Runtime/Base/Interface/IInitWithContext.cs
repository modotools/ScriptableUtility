
using System;
using Core.Unity.Types;

namespace ScriptableUtility
{
    public interface IInitWithContext
    {
        void Init(IContext ctx);
    }

    [Serializable]
    public class RefIInitWithContext : InterfaceContainer<IInitWithContext>
    {
    }
}