using Core.Interface;

namespace ScriptableUtility
{
    public interface IMultiContextMapping : INamed
    {
        void AddContext(IContext context);
        void RemoveContext(IContext context);
    }
}