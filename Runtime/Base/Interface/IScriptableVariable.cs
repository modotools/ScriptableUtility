using System;
using Core.Unity.Interface;

namespace ScriptableUtility
{
    public interface IScriptableVariable : IScriptableObject
    {
        ScriptableContextType ContextType { get; }
        Type VariableType { get; }
    }
}