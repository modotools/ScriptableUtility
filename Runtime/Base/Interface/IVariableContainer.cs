using System;
using System.Collections.Generic;
using Core.Unity.Types;
using UnityEngine;

namespace ScriptableUtility
{
    public interface IVariableContainer
    {
        IEnumerable<IScriptableVariable> Variables { get; }

        List<ScriptableObject> VariableObjs { get; }
    }

    [Serializable]
    public class RefIVariableContainer : InterfaceContainer<IVariableContainer>{}
}