﻿using ScriptableUtility.Events;

namespace ScriptableUtility
{
    public interface IScriptableEventListener
    {
        //IScriptableEvent EventListenedTo { get; }
    }

    public interface IScriptableEventListenerDefault : IScriptableEventListener
    {
        void OnEvent();
    }

    public interface IScriptableEventListener<in T> : IScriptableEventListener
    {
        void OnEvent(T var);
    }
}