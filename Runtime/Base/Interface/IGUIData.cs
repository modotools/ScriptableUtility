﻿using Core.Interface;
using UnityEngine;

namespace ScriptableUtility
{
    public interface IGUIData : INamed
    {
        bool UseColor { get; set; }
        Color GUIColor { get; set; }
        Texture2D Icon { get; set;}
    }
}