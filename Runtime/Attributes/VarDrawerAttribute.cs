﻿using Core.Unity.Types.Attribute;

namespace ScriptableUtility.Attributes
{
    public class VarDrawerAttribute : ContextDrawerAttribute
    {
        public VarDrawerAttribute(string autoSelectName = AutoSelectNameDefault,
            Condition setToSelf = ContextSelfDefault, 
            Condition showContext = ShowContextSelectorDefault,
            bool hideLabel = HideLabelDefault,
            bool showOnlyIfUnlinked = ShowOnlyIfUnlinkedDefault,
            bool breakAfterLabel = BreakAfterLabelDefault)
        : base(autoSelectName, setToSelf, showContext, hideLabel, showOnlyIfUnlinked, breakAfterLabel)
        {}
    }
}