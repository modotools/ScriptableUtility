using Core.Unity.Attributes;
using UnityEngine;


namespace ScriptableUtility.Events
{
    [EditorIcon("icon-event")]
    [CreateAssetMenu(menuName = "Scriptable/Events/Mesh")]
    public class MeshEvent : ScriptableEvent<Mesh> {}
}
