using Core.Unity.Attributes;
using UnityEngine;
using Core.Unity.Types;


namespace ScriptableUtility.Events
{
    [EditorIcon("icon-event")]
    [CreateAssetMenu(menuName = "Scriptable/Events/Enum")]
    public class EnumEvent : ScriptableEvent<SerializedEnum> {}
}
