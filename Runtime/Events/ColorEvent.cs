using Core.Unity.Attributes;
using UnityEngine;


namespace ScriptableUtility.Events
{
    [EditorIcon("icon-event")]
    [CreateAssetMenu(menuName = "Scriptable/Events/Color")]
    public class ColorEvent : ScriptableEvent<Color> {}
}
