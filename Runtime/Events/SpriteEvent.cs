using Core.Unity.Attributes;
using UnityEngine;


namespace ScriptableUtility.Events
{
    [EditorIcon("icon-event")]
    [CreateAssetMenu(menuName = "Scriptable/Events/Sprite")]
    public class SpriteEvent : ScriptableEvent<Sprite> {}
}
