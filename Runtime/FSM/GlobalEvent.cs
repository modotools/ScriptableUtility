using System;
using Core.Interface;
using UnityEngine;
using nodegraph;
using nodegraph.Attributes;
using nodegraph.Data;
using nodegraph.Operations;

namespace ScriptableUtility.FSM
{
    [NodeTint(0.2f,0.2f,0.2f),
     NodeWidth(150)]
    public class GlobalEvent : Node, IFullNode, INameable
    {
        public Type GetEntryType() => typeof(State);

        [HideInInspector, Output] 
        public StateConnector OUT;
        public string Description;

        public override INodeData NodeData => this;
        public INode Node => this;
        public Event EventData => new Event()
        {
            Name = Name,
            TargetState = GetConnectedState(),
            Condition = null,
        };

        public void SetName(string newName) => name = newName;

        public override object GetOutputValue(PortID port)
        {
            var node = Graph.GetConnectionFrom(port).To.Node as INode;
            return node?.NodeData;
        }

        public State GetConnectedState() => GetOutputValue(new PortID() {FieldName = nameof(OUT), Node = this}) as State;
    }
}