﻿using System;
using ScriptableUtility.ActionConfigs;
using UnityEngine;
using nodegraph;
using nodegraph.Attributes;
using nodegraph.Data;
using nodegraph.Operations;

namespace ScriptableUtility.FSM
{
    [DisallowMultipleNodes, 
     NodeTint(0f,0.5f,0.25f),
     NodeWidth(150)]
    public class StateEntryNode : Node, IFullNode
    {
        [HideInInspector, Output]
        public ScriptableActionConnector OnEnter;

        [HideInInspector, Output]
        public ScriptableActionConnector OnUpdate;

        [HideInInspector, Output]
        public ScriptableActionConnector OnExit;

        public override INodeData NodeData => this;
        public INode Node => this;

        public override string Name => name;

        public PortID OnEnter_Port => new PortID(){ FieldName = nameof(OnEnter), Node = this };
        public PortID OnUpdate_Port => new PortID(){ FieldName = nameof(OnUpdate), Node = this };
        public PortID OnExit_Port => new PortID(){ FieldName = nameof(OnExit), Node = this };

        public override object GetOutputValue(PortID port)
        {
            var toNode = Graph?.GetConnectionFrom(port).To.Node as INode;
            return toNode?.NodeData as ScriptableBaseAction;
        }

        public void GetConnectingActions(out ScriptableBaseAction onEnter, out ScriptableBaseAction onUpdate,
            out ScriptableBaseAction onExit)
        {
            onEnter = (ScriptableBaseAction) GetOutputValue(OnEnter_Port);
            onUpdate = (ScriptableBaseAction) GetOutputValue(OnUpdate_Port);
            onExit = (ScriptableBaseAction) GetOutputValue(OnExit_Port);
        }

        static string GetAssetName(string stateName) => $"{stateName} (Entry)";
        public void SetStateName(string stateName)
        {
            var assetName = GetAssetName(stateName);
            if (string.Equals(name, assetName, StringComparison.Ordinal))
                return;
            name = assetName;
        }
    }
}
