﻿using ScriptableUtility.Nodes;
using UnityEngine;

namespace ScriptableUtility.FSM
{
    [CreateAssetMenu(menuName = "Scriptable/FSM/TestEntryNode", fileName = "Entry Node")]
    public class FSMGraphEntryNode : EntryNode<State, StateConnector>{}
}
