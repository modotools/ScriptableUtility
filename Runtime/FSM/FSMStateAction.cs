﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Interface;
using Core.Types;
using Core.Unity.Extensions;
using Core.Unity.Interface;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Actions.FSM;
using UnityEngine;
using nodegraph;
using nodegraph.Attributes;
using nodegraph.Data;

namespace ScriptableUtility.FSM
{
    [RequireNode(typeof(StateEntryNode))]
    public class FSMStateAction : ScriptableObject, INodeGraph, IActionConfig, IDependencies, INamed
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] protected internal ScriptableObject[] m_nodes = new ScriptableObject[0];
        [SerializeField] Connection[] m_connections = new Connection[0];
#pragma warning restore 0649 // wrong warnings for SerializeField

        static string GetAssetName(string stateName) => $"{stateName} (Graph)";

        public void SetStateName(string stateName)
        {
            var assetName = GetAssetName(stateName);
            if (string.Equals(name, assetName, StringComparison.Ordinal))
                return;

            name = assetName;
            var entryNode = EntryNode;
            if (entryNode != null)
                entryNode.SetStateName(stateName);
        }

        public string Name => name;
        public List<NodePort> NodePorts { get; } = new List<NodePort>();
        public Dictionary<Connection, List<Vector2>> ReroutePoints { get; } = new Dictionary<Connection, List<Vector2>>();

        static Type StaticFactoryType => typeof(StateAction);
        public Type FactoryType => StaticFactoryType;


        public ref ScriptableObject[] NodeObjs => ref m_nodes;

        StateEntryNode EntryNode => NodeObjs.FirstOrDefault(a => (a is StateEntryNode)) as StateEntryNode;

        IEnumerable<IActionConfig> Actions =>
            NodeObjs.OfType<INode>().Select(n => n.NodeData as IActionConfig).Where(a => a != null);

        public ref Connection[] Connections => ref m_connections;

        public IBaseAction CreateAction()
        {
            using (var dict = SimplePool<Dictionary<IActionConfig, IBaseAction>>.I.GetScoped())
            {
                CreateActions(dict.Obj);
                LinkActions(dict.Obj);

                EntryNode.GetConnectingActions(out var onEnterConfig, out var onUpdateConfig, out var onExitConfig);
                //Debug.Log($"{Name} {onEnterConfig} {onUpdateConfig} {onExitConfig}");
                var onEnter = onEnterConfig != null ? dict.Obj[onEnterConfig] : null;
                var onUpdate = onUpdateConfig != null ? dict.Obj[onUpdateConfig] : null;
                var onExit = onExitConfig != null ? dict.Obj[onExitConfig] : null;
                return new StateAction(onEnter, onUpdate, onExit);
            }
        }

        void LinkActions(Dictionary<IActionConfig, IBaseAction> dict)
        {
            foreach (var action in Actions)
            {
                if (!(action is ILinkActionConfigs link))
                    continue;

                link.LinkActions(dict);
            }
        }

        void CreateActions(IDictionary<IActionConfig, IBaseAction> dict)
        {
            foreach (var action in Actions)
            {
                Debug.Assert(action != null);

                var baseAction = action.CreateAction();
                dict.Add(action, baseAction);
            }
        }
        
        void OnDestroy()
        {
            foreach (var a in Actions) 
                (a as ScriptableObject).DestroyEx();
            foreach (var node in NodeObjs)
                node.DestroyEx();
        }

        public IEnumerable<ScriptableObject> Dependencies => m_nodes;
    }
}
