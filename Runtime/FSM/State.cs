﻿using System;
using System.Collections.Generic;
using Core.Interface;
using Core.Unity.Extensions;
using ScriptableUtility.ActionConfigs;
using UnityEngine;
using nodegraph;
using nodegraph.Attributes;
using nodegraph.Interface;

namespace ScriptableUtility.FSM
{
    public class State : Node, INodeData, INestedGraph, IGUIColor, INameable
    {
        [SerializeField] Color m_color = Color.gray;

        [Input(ConnectionType.Multiple), HideInInspector] public StateConnector IN;
        public FSMStateAction FSMStateAction;

        [NodeConnectorList(IO.Output, typeof(State), nameof(Event.Name), nameof(Event.TargetState))]
        public List<Event> Events = new List<Event>();
        public string Description;

        public override INodeData NodeData => this;
        public INode Node => this;
        public INodeGraph NestedGraph => FSMStateAction;
        public Color Color
        {
            get => m_color;
            set => m_color = value;
        }
        public void SetName(string newName)
        {
            name = newName;
            if (FSMStateAction != null)
                FSMStateAction.SetStateName(Name);
        }

        public void OnDestroy()
        {
            if (FSMStateAction != null)
                FSMStateAction.DestroyEx();
        }

        public override IEnumerable<ScriptableObject> Dependencies
        {
            get
            {
                foreach (var d in base.Dependencies)
                    yield return d;
                yield return FSMStateAction;
            }
        }
    }

    [Serializable]
    public struct Event
    {
        public string Name;
        public State TargetState;
        public ScriptableBaseAction Condition;
    }

    [Serializable]
    public struct StateConnector : INodeConnector<State> { }
}
