﻿using System;
using System.Linq;
using Core.Interface;
using ScriptableUtility.Actions.FSM;
using ScriptableUtility.FSM;
using UnityEngine;

namespace ScriptableUtility.Behaviour
{
    public class FSMBehaviour : MonoBehaviour, IFSMComponent
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField] ContextComponent m_context;
        [SerializeField] FSMGraph m_fsmGraph;
#pragma warning restore 0649 // wrong warning

        FSMAction m_fsmAction;
        
        public FSMGraph Graph => m_fsmGraph;
        public void Get(out IFSMConfig provided) => provided = m_fsmGraph;

        void Start()
        {
            m_fsmGraph.SetContext(m_context);
            m_fsmAction = (FSMAction) m_fsmGraph.CreateAction();
            m_fsmAction.Init();
        }

        void Update() => m_fsmAction?.Update(Time.deltaTime);

        public void ResetFSM()
        {
            // todo 5: implement
            throw new System.NotImplementedException();
        }

        public void SetState(string stateName) 
            => m_fsmAction.SwitchState(stateName);

        public void SendEvent(string eventName)
            => m_fsmAction.OnEvent(eventName);

        public string ActiveStateName => GetActiveState()?.Name;
        public IFSMConfig Fsm => m_fsmGraph;
        public bool CanInvoke(string eventName)
        {
            if (m_fsmAction.IsGlobal(eventName))
                return true;
            if (m_fsmAction.CurrentState.Events == null)
                return false;
            return Array.FindIndex(m_fsmAction.CurrentState.Events, e 
                => string.Equals(e.Event, eventName)) != -1;
        }

        public State GetActiveState()
        {
            if (m_fsmAction == null)
                return null;
            return m_fsmGraph.NodeObjs.OfType<State>().FirstOrDefault(s =>
                string.Equals(s.Name, m_fsmAction.CurrentState.Name));
        }
    }
}
