﻿using System;
using System.Collections.Generic;
using Core.Interface;
using Core.Unity.Attributes;
using ScriptableUtility.ActionConfigs;
using UnityEngine;

namespace ScriptableUtility.Behaviour
{
    public class ActionBehaviour : MonoBehaviour, IProvider<IContext>
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField] ContextComponent m_context;
#pragma warning restore 0649 // wrong warning

        public List<FSMNamedAction> Actions = new List<FSMNamedAction>();

        readonly Dictionary<string, IDefaultAction> m_actions = new Dictionary<string, IDefaultAction>();

        void Start()
        {
            foreach (var a in Actions)
            {
                var defAction = a.Init() as IDefaultAction;
                m_actions.Add(a.Name, defAction);
            }
        }

        public void Get(out IContext provided) => provided = m_context;

        public void InvokeAction(string actionName)
        {
            if (!m_actions.ContainsKey(actionName))
            {
                return;
            }
            m_actions[actionName].Invoke();
        }
    }

    [Serializable]
    public struct FSMNamedAction : IActionData
    {
        public FSMNamedAction(ScriptableBaseAction config)
        {
            m_config = config;
            m_name = "";
            NoStopOnExit = false;
        }

        [SerializeField] ScriptableBaseAction m_config;
        [SerializeField] string m_name;

        [ShowIf(nameof(IsStoppable))]
        public bool NoStopOnExit;

        // ReSharper disable ConvertToAutoPropertyWithPrivateSetter
        public string Name => m_name;
        public bool IsStoppable => m_config != null && typeof(IStoppableAction).IsAssignableFrom(m_config.FactoryType);
        public ScriptableBaseAction Config => m_config;
        // ReSharper restore ConvertToAutoPropertyWithPrivateSetter

        public IBaseAction Init() => m_config.CreateAction();
    }

    public interface IActionData
    {
        ScriptableBaseAction Config { get; }
        IBaseAction Init();
    }
}
