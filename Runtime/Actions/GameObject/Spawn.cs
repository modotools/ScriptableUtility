﻿using Core.Interface;

using Core.Unity.Utility.PoolAttendant;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.Actions.GameObject
{
    public class SpawnAction : IDefaultAction
    {
        readonly IVar<GO> m_spawnPos;
        readonly IVar<GO> m_spawnPrefab;
        readonly IVar<GO> m_spawnInstance;

        public SpawnAction(IVar<GO> pos, IVar<GO> spawnPrefab, IVar<GO> spawnInstance)
        {
            m_spawnPos = pos;
            m_spawnPrefab = spawnPrefab;
            m_spawnInstance = spawnInstance;
        }

        public void Invoke()
        {
            var transform = m_spawnPos.Value.transform;
            m_spawnInstance.Value = m_spawnPrefab.Value.GetPooledInstance(transform.position, transform.rotation);
        }
    }
}