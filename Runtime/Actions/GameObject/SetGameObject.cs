﻿using Core.Interface;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.Actions.GameObject
{
    public class SetGameObjectAction : IDefaultAction
    {
        readonly IVar<GO> m_variableRef;
        readonly IVar<GO> m_gameObjectRef;

        public SetGameObjectAction(IVar<GO> var, IVar<GO> go)
        {
            m_variableRef = var;
            m_gameObjectRef = go;
        }

        public void Invoke() => m_variableRef.Value = m_gameObjectRef.Value;
    }
}
