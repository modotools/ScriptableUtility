﻿using Core.Interface;

using GO = UnityEngine.GameObject;

namespace ScriptableUtility.Actions.GameObject
{
    public class UnparentAction : IDefaultAction
    {
        readonly IVar<GO> m_unparentObj;

        public UnparentAction(IVar<GO> unparentObj)
            => m_unparentObj = unparentObj;

        public void Invoke()
        {
            var childTr = m_unparentObj.Value.transform;
            childTr.SetParent(null);
        }
    }
}
