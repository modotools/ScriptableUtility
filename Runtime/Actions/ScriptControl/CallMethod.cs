using System;
using System.Reflection;
using Core.Interface;
using UnityEngine;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.Actions.ScriptControl
{
    public class CallMethodAction : IDefaultAction
    {
        readonly IVar<GO> m_gameObject;

        readonly Type m_componentType;
        readonly MethodInfo m_method;
        readonly IContextVariableReference[] m_varRefs;
        readonly IContextVariableReference m_returnVarRef;

        readonly object[] m_tempParams;

        public CallMethodAction(IVar<GO> go, 
            Type componentType, MethodInfo method, 
            IContextVariableReference[] varRefs, 
            IContextVariableReference returnVarRef)
        {
            m_gameObject = go;
            m_componentType = componentType;
            m_method = method;
            m_varRefs = varRefs;
            m_returnVarRef = returnVarRef;
            m_tempParams = new object[varRefs.Length];
        }

        public void Invoke()
        {
            var go = m_gameObject.Value;
            if (go == null)
            {
                Debug.LogError("Cannot Call Method, GameObject-variable is null");
                return;
            }

            go.TryGetComponent(m_componentType, out var c);
            SyncParameters();
            var returnObj = m_method.Invoke(c, m_tempParams);
            if (m_returnVarRef != null && returnObj != null)
                m_returnVarRef.ValueObject = returnObj;
        }

        void SyncParameters()
        {
            for (var i = 0; i < m_varRefs.Length; i++) 
                m_tempParams[i] = m_varRefs[i].ValueObject;
        }
    }
}