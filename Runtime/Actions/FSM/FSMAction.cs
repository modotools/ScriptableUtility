﻿using System;
using System.Collections.Generic;
using Core.Extensions;

namespace ScriptableUtility.Actions.FSM
{
    public class FSMAction : IUpdatingAction, IStoppableAction, IContainingActions
    {
        [Serializable]
        public struct State
        {
            public string Name;
            public StateAction Action;
            public EventData[] Events;
        }

        [Serializable]
        public struct EventData
        {
            public string Event;
            public IBoolAction Condition;
            public int StateIdxTo;
        }

        readonly State[] m_states;
        readonly EventData[] m_globalEvents;

        public FSMAction(State[] states, EventData[] globalEvents)
        {
            m_states = states;
            m_globalEvents = globalEvents;
        }

        int m_currentState;

        public State CurrentState => !m_currentState.IsInRange(0, m_states.Length) 
            ? default : m_states[m_currentState];

        public bool IsFinished => m_currentState == -1;

        public void Init()
        {
            m_currentState = 0;
            Enter();
        }

        void Enter() => CurrentState.Action?.Enter();

        public void Update(float dt)
        {
            // todo 2: fire the event only once?
            if (CurrentState.Action?.IsFinished == true)
                OnEvent("On Finished");

            CurrentState.Action?.Update(dt);
        }

        public void Stop()
        {
            CurrentState.Action?.Stop();
            m_currentState = -1;
        }

        void SwitchState(int stateIdx)
        {
            Stop();
            m_currentState = stateIdx;
            Enter();
        }

        public void SwitchState(string stateName) =>
            SwitchState(Array.FindIndex(m_states, a => string.Equals(stateName, a.Name)));

        public void OnEvent(string eventName)
        {
            var idx = Array.FindIndex(m_globalEvents, e => string.Equals(e.Event, eventName));
            if (idx != -1)
            {
                SwitchState(m_globalEvents[idx].StateIdxTo);
                return;
            }

            if (CurrentState.Action == null)
                return;

            idx = Array.FindIndex(CurrentState.Events, c=>  string.Equals(c.Event, eventName));
            if (idx == -1) 
                return;

            SwitchState(CurrentState.Events[idx].StateIdxTo);
        }

        public IEnumerable<IBaseAction> GetContainedActions()
        {
            foreach (var state in m_states)
            {
                yield return state.Action;
                foreach (var c in state.Events) 
                    if(c.Condition != null)
                        yield return c.Condition;
            }
            foreach (var e in m_globalEvents)
            {
                if (e.Condition != null)
                    yield return e.Condition;
            }
        }

        public bool IsGlobal(string eventName) 
            => Array.FindIndex(m_globalEvents, e 
                => string.Equals(e.Event, eventName, StringComparison.Ordinal)) != -1;
    }
}