﻿using System.Collections.Generic;
using UnityEngine;

namespace ScriptableUtility.Actions.FSM
{
    public class StateAction : IUpdatingAction, IStoppableAction, IContainingActions
    {
        readonly IBaseAction m_onEnter;
        readonly IBaseAction m_onUpdate;
        readonly IBaseAction m_onExit;

        bool m_executedUpdate;

        public StateAction(IBaseAction onEnter, IBaseAction onUpdate, IBaseAction onExit)
        {
            m_onEnter = onEnter;
            m_onUpdate = onUpdate;
            m_onExit = onExit;
        }

        public bool IsFinished
        {
            get
            {
                if (m_onUpdate is IUpdatingAction onUpdate)
                    return onUpdate.IsFinished;

                // todo 4: state-action without nested UpdatingAction is finished after run once, show it to the user
                // options: add updating action that executes the DefaultAction once or until boolean is true
                return (m_onUpdate == null || m_executedUpdate);
            }
        }

        public void Enter()
        {
            //Debug.Log(nameof(Enter));

            if (m_onEnter is IDefaultAction defEnter)
                defEnter.Invoke();
            Init();
        }

        public void Init()
        {
            //Debug.Log(nameof(Init));
            m_executedUpdate = false;

            if (m_onUpdate is IUpdatingAction onUpdate)
                onUpdate.Init();
        }

        public void Update(float dt)
        {
            //Debug.Log(m_onUpdate);
            switch (m_onUpdate)
            {
                case IUpdatingAction onUpdate: onUpdate.Update(dt); break;
                case IDefaultAction defUpdate: defUpdate.Invoke(); break;
            }

            m_executedUpdate = true;
        }

        public void Stop()
        {
            if (m_onUpdate is IStoppableAction stopUpdate)
                stopUpdate.Stop();
            if (m_onExit is IDefaultAction defExit)
                defExit.Invoke();
        }

        public IEnumerable<IBaseAction> GetContainedActions()
            => new[] {m_onEnter, m_onUpdate, m_onExit};
    }
}