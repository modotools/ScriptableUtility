﻿using Core.Interface;
using UnityEngine;
using Random = UnityEngine.Random;

using GO = UnityEngine.GameObject;

namespace ScriptableUtility.Actions.Rigidbody
{
    public class AddArcForceAction : IDefaultAction
    {
        readonly IVar<GO> m_rigidBodyObject;
        readonly IVar<Vector2> m_force;

        public AddArcForceAction(IVar<GO> rigid, IVar<Vector2> arcForce)
        {
            m_rigidBodyObject = rigid;
            m_force = arcForce;
        }

        public void Invoke()
        {
            var forceVal = m_force.Value;
            var force = Vector3.up * forceVal.y;
            if (forceVal.x > 0)
            {
                var angle = Random.Range(0, 360);
                var radian = angle * Mathf.Deg2Rad;
                var x = Mathf.Cos(radian);
                var z = Mathf.Sin(radian);
                var forceAdd = new Vector3(x, 0, z) * forceVal.x;
                force += forceAdd;
            }

            m_rigidBodyObject.Value.GetComponent<UnityEngine.Rigidbody>().AddForce(force, ForceMode.Impulse);
        }
    }
}
