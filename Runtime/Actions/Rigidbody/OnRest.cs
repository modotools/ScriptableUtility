﻿using Core.Interface;
using UnityEngine;

using GO = UnityEngine.GameObject;

namespace ScriptableUtility.Actions.Rigidbody
{
    public class OnRestAction : IUpdatingAction
    {
        readonly IVar<GO> m_rigidBodyObj;
        readonly float m_restingWhenVelocityMagnitudeLower;
        readonly float m_rayCastGroundCheckDistance;

        UnityEngine.Rigidbody m_rigidBody;
        public OnRestAction(IVar<GO> rigid, float velocityMagnitudeLower, float groundCheckDistance)
        {
            m_rigidBodyObj = rigid;
            m_restingWhenVelocityMagnitudeLower = velocityMagnitudeLower;
            m_rayCastGroundCheckDistance = groundCheckDistance;
        }

        public bool IsFinished
        {
            get
            {
                if (m_rigidBody == null)
                    return false;
                // ReSharper disable once ConvertIfStatementToReturnStatement
                if (m_rigidBody.velocity.magnitude > m_restingWhenVelocityMagnitudeLower)
                    return false;

                return Physics.Raycast(m_rigidBody.position, Vector3.down, m_rayCastGroundCheckDistance);
            }
        }

        public void Init()
        {
            m_rigidBody = m_rigidBodyObj.Value.GetComponent<UnityEngine.Rigidbody>();
        }

        public void Update(float dt)
        {
        }
    }
}
