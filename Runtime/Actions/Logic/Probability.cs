﻿using System.Collections.Generic;

using Core.Interface;

namespace ScriptableUtility.Actions.Logic
{
    public class ProbabilityAction : IDefaultAction, IContainingActions
    {
        readonly IVar<float> m_chanceOfAction;
        IDefaultAction m_action;

        public ProbabilityAction(IVar<float> chanceOfAction)
        {
            m_chanceOfAction = chanceOfAction;
        }
        
        public void LinkActions(IDefaultAction action) => m_action = action;

        public void Invoke()
        {
            var randomVal = UnityEngine.Random.value;
            //Debug.Log(randomVal + " " + spawnChance);
            if (randomVal > m_chanceOfAction.Value)
                return;

            m_action.Invoke();
        }
        public IEnumerable<IBaseAction> GetContainedActions()
            => new[]{m_action};
    }
}
