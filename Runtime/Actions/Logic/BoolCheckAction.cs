﻿using System.Collections.Generic;
using Core.Interface;

//using ScriptableUtility.Variables.Reference;

namespace ScriptableUtility.Actions.Logic
{
    public class BoolCheckAction : IDefaultAction
        , IUpdatingAction, IStoppableAction, IInstantCompletableAction
        , IContainingActions
    {
        public enum FinishRule
        {
            ActionOnceDone,
            NothingTodo,
            BreakConditionOnly
        }

        public struct Rules
        {
            public bool BreakByVariable;
            public IVar<bool> BreakValue;
            public FinishRule FinishRule;

            public bool WaitForBoolChange;
            public bool DontUpdateBoolCheck;
        }

        public BoolCheckAction(IVar<bool> boolRef, IVar<bool> breakRef, Rules rules)
        {
            m_boolReference = boolRef;
            m_breakReference = breakRef;

            m_rules = rules;

            m_lastAction = null;
            m_instantCompleted = false;
        }
        public void LinkActions(IBaseAction trueAction, IBaseAction falseAction)
        {
            m_trueAction = trueAction;
            m_falseAction = falseAction;
        }

        readonly IVar<bool> m_boolReference;
        readonly IVar<bool> m_breakReference;

        IBaseAction m_trueAction;
        IBaseAction m_falseAction;

        readonly Rules m_rules;
        IBaseAction m_lastAction;

        bool m_boolTest;
        bool m_instantCompleted;

        bool BreakByVariable => m_rules.BreakByVariable &&
                                m_breakReference.Value == m_rules.BreakValue.Value;
        bool BreakByFinishRule
        {
            get
            {
                if (m_rules.FinishRule == FinishRule.BreakConditionOnly)
                    return false;

                if (m_lastAction is IUpdatingAction up)
                    return up.IsFinished;
                return m_lastAction != null 
                       || m_rules.FinishRule == FinishRule.NothingTodo;
            }
        }

        public bool IsFinished => BreakByVariable || BreakByFinishRule || m_instantCompleted;
        IBaseAction GetAction(bool test) => test ? m_trueAction : m_falseAction;

        public void Invoke() => (GetAction(m_boolReference.Value) as IDefaultAction)?.Invoke();

        public void Init()
        {
            m_boolTest = m_boolReference.Value;

            if (m_rules.WaitForBoolChange)
                return;
            OnBoolChanged();
        }

        public void Update(float dt)
        {
            if (m_lastAction is IUpdatingAction up && !up.IsFinished)
                up.Update(dt);

            if (m_rules.DontUpdateBoolCheck)
                return;
            if (m_boolTest != m_boolReference.Value)
                OnBoolChanged();
        }
        void OnBoolChanged()
        {
            if (m_lastAction is IStoppableAction stop)
                stop.Stop();

            m_lastAction = GetAction(m_boolReference.Value);

            if (m_lastAction is IUpdatingAction up)
                up.Init();
            else if (m_lastAction is IDefaultAction def)
                def.Invoke();

            m_boolTest = m_boolReference.Value;
        }

        public void Stop()
        {
            if (!(m_lastAction is IUpdatingAction up) || up.IsFinished)
                return;
            if (up is IStoppableAction stoppable)
                stoppable.Stop();
        }

        public void InstantComplete() => m_instantCompleted = true;
        public IEnumerable<IBaseAction> GetContainedActions() => new[] { m_trueAction, m_falseAction };
    }
}
