﻿using System.Collections.Generic;
using UnityEngine;

namespace ScriptableUtility.Actions.Logic
{
    public class ActionSequenceAction : 
        IDefaultAction, IUpdatingAction, IStoppableAction, 
        IInstantCompletableAction, IContainingActions,
        IDebugAction
    {
        IBaseAction[] m_actions;
        int m_currentIndex;

        //public ActionSequenceAction() {}
        //public ActionSequenceAction(IBaseAction[] actions) => m_actions = actions;
        
        public void LinkActions(IBaseAction[] actions) //Debug.Log($"Linking {m_debugName} {GetHashCode()} {actions?.Length}"); 
            => m_actions = actions;

        public void Invoke()
        {
            //Debug.Log($"{m_debugName} {GetHashCode()} {m_actions?.Length}");
            foreach (var a in m_actions)
            {
                if (a is IDefaultAction def)
                    def.Invoke();
            }
        }

        public bool IsFinished => m_currentIndex >= m_actions.Length;
        public void Init()
        {
            m_currentIndex = -1;
            NextAction();
            for (; m_currentIndex < m_actions.Length; NextAction())
            {
                var a = m_actions[m_currentIndex];
                if (a is IUpdatingAction)
                    break;
                if (!(a is IDefaultAction def))
                    break;
                def.Invoke();                
            }
        }

        public void Update(float dt)
        {
            for (; m_currentIndex < m_actions.Length; NextAction())
            {
                var a = m_actions[m_currentIndex];
                if (a is IUpdatingAction up)
                {
                    if (up.IsFinished)
                        continue;
                    up.Update(dt);

                    break;
                }
                if (!(a is IDefaultAction def))
                    break;
                def.Invoke();
            }
        }

        void NextAction()
        {
            m_currentIndex++;
            if (m_currentIndex >= m_actions.Length)
                return;
            var action = m_actions[m_currentIndex];
            if (action is IUpdatingAction up)
                up.Init();
        }

        public void Stop()
        {
            if (m_currentIndex >= m_actions.Length)
                return;
            var action = m_actions[m_currentIndex];
            if (action is IUpdatingAction 
                && action is IStoppableAction stop)
                stop.Stop();
        }

        public void InstantComplete()
        {
            if (IsFinished)
                return;
            foreach (var a in m_actions)
                if (a is IInstantCompletableAction instant)
                    instant.InstantComplete();
            //else if(a is IDefaultAction def)
            //    def.Invoke();
        }

        public IEnumerable<IBaseAction> GetContainedActions()
            => m_actions;


        string m_debugName;
        public void SetDebugName(string debugName) 
            => m_debugName = debugName;
        public void DebugAction(ref string debugString)
        { 
            debugString += $"Sequence {m_debugName} at {m_currentIndex} : {m_actions[m_currentIndex].GetType()} \n";
            if (m_actions[m_currentIndex] is IDebugAction da)
                da.DebugAction(ref debugString);
        }
    }
}
