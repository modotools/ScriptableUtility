﻿

using Core.Interface;

namespace ScriptableUtility.Actions.Logic
{
    public class WaitForBoolChangedAction : IUpdatingAction
    {
        readonly IVar<bool> m_bool;
        bool m_prevValue;

        public WaitForBoolChangedAction(IVar<bool> boolVar) => m_bool = boolVar;

        public bool IsFinished => m_bool.Value != m_prevValue;

        public void Init() => m_prevValue = m_bool.Value;

        public void Update(float dt)
        {
            // Debug.Log("Waiting for bool change");
        }
    }
}
