﻿namespace ScriptableUtility.Actions.Logic
{
    public class ANDSequenceAction : IBoolAction
    {
        readonly IBoolAction[] m_actions;
        bool m_result;

        public ANDSequenceAction(IBoolAction[] actions)
        {
            m_actions = actions;
        }

        public void Invoke()
        {
            m_result = true;
            foreach (var a in m_actions)
            {
                a.Invoke(out m_result);
                if (!m_result)
                    break;
            }
        }

        public void Invoke(out bool result)
        {
            Invoke();
            result = m_result;
        }
    }
}
