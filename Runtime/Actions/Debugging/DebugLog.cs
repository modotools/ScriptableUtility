﻿using Core.Interface;

namespace ScriptableUtility.Actions.Debugging
{
    public class DebugLogAction : IDefaultAction
    {
        readonly IVar<string> m_logString;

        public DebugLogAction(IVar<string> logString)
            => m_logString = logString;
        
        public void Invoke() => UnityEngine.Debug.Log(m_logString.Value);
    }
}
