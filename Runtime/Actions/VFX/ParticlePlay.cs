﻿using Core.Interface;
using Core.Unity.Extensions;
using Core.Unity.Utility.PoolAttendant;
using UnityEngine;

using GO = UnityEngine.GameObject;

namespace ScriptableUtility.Actions.VFX
{
    public class ParticlePlayAction : IDefaultAction, IUpdatingAction, IStoppableAction
    {
        readonly IVar<GO> m_particles;
        readonly IVar<GO> m_emitter;

        ParticleSystem m_particleSystem;

        public ParticlePlayAction(IVar<GO> particles, IVar<GO> emitter)
        {
            m_particles = particles;
            m_emitter = emitter;
        }

        public void Init() => Invoke();
        public void Invoke()
        {
            var tr = m_emitter.Value.transform;
            var particleObj = m_particles.Value.GetPooledInstance(tr.position, tr.rotation);

            m_particleSystem = particleObj.GetComponent<ParticleSystem>();
            m_particleSystem.Play();
        }

        public bool IsFinished => !m_particleSystem.isEmitting;

        public void Update(float dt) {}

        public void Stop()
        {
            m_particleSystem.Stop(true);
            m_particleSystem.gameObject.TryDespawn();
            m_particleSystem = null;
        }
    }
}
