using Core.Interface;
using UnityEngine;

using GO = UnityEngine.GameObject;

namespace ScriptableUtility.Actions.VFX
{
    public class TrailEmitAction : IDefaultAction, IStoppableAction
    {
        readonly IVar<GO> m_rendererGO;

        public TrailEmitAction(IVar<GO> goRef)
        {
            m_rendererGO = goRef;
        }

        public void Invoke()
        {
            var renderer = m_rendererGO.Value.GetComponent<TrailRenderer>();

            if (renderer != null && renderer.enabled)
                renderer.emitting = true;
            else // todo 3: make this more obvious
            {
                var rends = m_rendererGO.Value.GetComponentsInChildren<TrailRenderer>();
                foreach (var r in rends)
                    r.emitting = true;
            }
        }

        public void Stop()
        {
            var renderer = m_rendererGO.Value.GetComponent<TrailRenderer>();

            if (renderer != null && renderer.enabled)
                renderer.emitting = false;
            else
            {
                var rends = m_rendererGO.Value.GetComponentsInChildren<TrailRenderer>();
                foreach (var r in rends)
                    r.emitting = false;
            }
        }
    }
}
