﻿using System.Collections.Generic;
using System.Linq;
using Core.Unity.Interface;
using UnityEngine;
using UnityEngine.Serialization;

namespace ScriptableUtility
{
    [CreateAssetMenu(fileName = nameof(ScriptableContainer), menuName = "Scriptable/ScriptableContainer")]
    public class ScriptableContainer : ScriptableObject, IVariableContainer, IDependencies
    {
        [FormerlySerializedAs("Variables")]
        [SerializeField] List<ScriptableObject> m_variables = new List<ScriptableObject>();

        public IEnumerable<IScriptableVariable> Variables => m_variables.OfType<IScriptableVariable>();
        public List<ScriptableObject> VariableObjs => m_variables;
        public IEnumerable<ScriptableObject> Dependencies => m_variables;
    }
}
