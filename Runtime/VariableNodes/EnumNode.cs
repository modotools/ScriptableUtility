using ScriptableUtility.Variables;
using Core.Unity.Types;


namespace ScriptableUtility.VariableNodes
{
    public class EnumNode : VarNode<SerializedEnum> { }
}
