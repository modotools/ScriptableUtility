using ScriptableUtility.Variables;
using UnityEngine;


namespace ScriptableUtility.VariableNodes
{
    public class MeshNode : VarNode<Mesh> { }
}
