using System;
using System.Runtime.InteropServices;
using Core.Interface;
using nodegraph;
using nodegraph.Attributes;
using nodegraph.Data;
using nodegraph.Interface;
using UnityEngine;


namespace ScriptableUtility.VariableNodes
{
    public class GameObjectNode : VarNode<GameObject>, IProvider<IContext>
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [Output(ConnectionType.Multiple), SerializeField]
        ContextConnector m_contextOUT;
#pragma warning restore 0649 // wrong warning

        public void Get(out IContext provided)
        {
            provided = null;
            if (m_varRef.Value.TryGetComponent(out IProvider<IContext> prov))
                prov.Get(out provided);
        }

        public override object GetOutputValue(PortID port) =>
            string.Equals(port.FieldName, nameof(m_contextOUT)) 
                ? this 
                : base.GetOutputValue(port);
    }

    [Serializable]
    public struct ContextConnector : INodeConnector<IContext> {}
}
