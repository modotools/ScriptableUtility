using ScriptableUtility.Variables;
using UnityEngine;


namespace ScriptableUtility.VariableNodes
{
    public class AudioClipNode : VarNode<AudioClip> { }
}
