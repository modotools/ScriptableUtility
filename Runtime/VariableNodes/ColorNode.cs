using ScriptableUtility.Variables;
using UnityEngine;


namespace ScriptableUtility.VariableNodes
{
    public class ColorNode : VarNode<Color> { }
}
