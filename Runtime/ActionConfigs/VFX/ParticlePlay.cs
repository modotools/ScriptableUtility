﻿using System;
using Core.Unity.Attributes;
using ScriptableUtility.Actions.VFX;
using UnityEngine;
using nodegraph.Attributes;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.ActionConfigs.VFX
{
    [EditorIcon("icon-action")]
    // [Tooltip("Play Particle Effect")]
    public class ParticlePlay : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<GO> m_particleSystem;
        [SerializeField, Input] internal VarReference<GO> m_emitter;
#pragma warning restore 0649 // wrong warning

        public override string Name => $"{nameof(ParticlePlay)}";
        static Type StaticFactoryType => typeof(ParticlePlayAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() =>
            new ParticlePlayAction(
                m_particleSystem.Init(nameof(m_particleSystem), Node), 
                m_emitter.Init(nameof(m_emitter), Node));
    }
}
