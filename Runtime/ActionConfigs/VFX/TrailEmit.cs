using System;
using Core.Unity.Attributes;
using ScriptableUtility.Actions.VFX;
using UnityEngine;
using nodegraph.Attributes;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.ActionConfigs.VFX
{
    [EditorIcon("icon-action")]
    public class TrailEmit : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        // [RequiredField]
        // [CheckForComponent(typeof(TrailRenderer))]
        [SerializeField, Input] internal VarReference<GO> m_renderer;
#pragma warning restore 0649 // wrong warning

        public override string Name => $"{nameof(TrailEmit)}";
        static Type StaticFactoryType => typeof(TrailEmitAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() => new TrailEmitAction(m_renderer.Init(nameof(m_renderer), Node));
    }
}
