﻿using System;
using Core.Unity.Attributes;
using Core.Unity.Types;
using Core.Unity.Types.Attribute;
using ScriptableUtility.Actions.ScriptControl;
using UnityEngine;
using nodegraph;
using nodegraph.Attributes;
using nodegraph.Data;
using nodegraph.Operations;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.ActionConfigs.ScriptControl
{
    [EditorIcon("icon-action")]
    [CreateAssetMenu(menuName = "Scriptable/CallMethod")]
    public class CallMethod : ScriptableBaseAction, IFullNode
    {
        public const string ReturnName = "return";

        public INodeData NodeData => this;
        public override INode Node => this;

        public INodeGraph Graph
        {
            get => (INodeGraph) m_graph;
            set => m_graph = (ScriptableObject) value;
        }

        public Vector2 Position
        {
            get => m_position;
            set => m_position = value;
        }

        public override string Name => $"Call: {BehaviourName}.{MethodName}()";
        string BehaviourName => Behaviour?.Type?.Name;
        
#if UNITY_EDITOR
        public string[] Editor_GUIExcludes => new[] { "m_Script", nameof(m_graph), nameof(m_position) };
        public string Editor_MethodSig => $"{MethodName}({Editor_GetMethodSignature()})";
#endif

        static Type StaticFactoryType =>  typeof(CallMethodAction);
        public override Type FactoryType => StaticFactoryType;


#pragma warning disable 0649 // wrong warnings for SerializeField
        [ClassExtends(typeof(Component), AllowNoNamespace = false)]
        public ClassTypeReference Behaviour;
        public string MethodName;

        [Input] public VarReference<GO> GameObjectVar;

        [HideInInspector, SerializeField] ScriptableObject m_graph;

        [SerializeField] string[] m_paramNames;

        [SerializeField] SerializableType[] m_cachedParamTypes;
        //[SerializeField] SerializableType m_cachedReturnType;
        [HideInInspector, SerializeField] Vector2 m_position;
#pragma warning restore 0649 // wrong warnings for SerializeField

        protected void OnEnable() => this.Default_OnEnable();
        public void Init() { }

        // no output
        public object GetOutputValue(PortID port) => null;

        public void OnCreateConnection(PortID port) { }
        public void OnRemoveConnection(PortID port) { }


        public override IBaseAction CreateAction()
        {
            var varRefs = new IContextVariableReference[m_paramNames.Length];
            for (var i = 0; i < m_paramNames.Length; i++)
                varRefs[i] = this.GetInputValue<IContextVariableReference>(m_paramNames[i]);

            var returnVarRef = this.GetInputValue<IContextVariableReference>(ReturnName);
            //Debug.Log($"{Name} has return port: {returnVarRef!= null}");

            //throw new NotImplementedException();
            var goVar = GameObjectVar.Init(nameof(GameObjectVar), this);

            var componentType = Behaviour.Type;
            var method = componentType.GetMethod(MethodName, GetParamTypes());
            return new CallMethodAction(goVar, componentType, method, varRefs, returnVarRef);
        }

        public Type[] GetParamTypes()
        {
            if (m_cachedParamTypes == null)
                return null;
            
            var paramTypes = new Type[m_cachedParamTypes.Length];
            for (var i = 0; i < m_cachedParamTypes.Length; i++)
                paramTypes[i] = m_cachedParamTypes[i].Type;
            return paramTypes;
        }

#if UNITY_EDITOR
        public void Editor_SetParameter(Type[] paramTypes, string[] parameterNames)
        {
            if (paramTypes == null)
                m_cachedParamTypes = null;
            else
            {
                m_cachedParamTypes = new SerializableType[paramTypes.Length];
                for (var i = 0; i < paramTypes.Length; i++) 
                    m_cachedParamTypes[i] = new SerializableType(paramTypes[i]);
            }

            m_paramNames = parameterNames;
        }

        string Editor_GetMethodSignature()
        {
            var argStr = "";
            if (m_cachedParamTypes == null)
                return argStr;
            for (var j = 0; j < m_cachedParamTypes.Length; j++)
            {
                var separator = j == 0 ? "" : ",";
                argStr += $"{argStr}{separator}{m_cachedParamTypes[j].Type.Name}";
            }
            return argStr;
        }
#endif
    }
}