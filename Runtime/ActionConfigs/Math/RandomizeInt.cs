﻿using System;
using Core.Unity.Attributes;
using Core.Unity.Interface;
using ScriptableUtility.Actions.Math;
using UnityEngine;
using nodegraph.Attributes;

namespace ScriptableUtility.ActionConfigs.Math
{
    [EditorIcon("icon-action")]
    public class RandomizeInt : ScriptableBaseAction, IVerify
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input]
        internal VarReference<int> m_variable;
        [SerializeField, Input]
        internal VarReference<int> m_min;
        [SerializeField, Input]
        internal VarReference<int> m_max;
#pragma warning restore 0649 // wrong warning

        public override string Name => $"Random Int";
        static Type StaticFactoryType => typeof(RandomizeIntegerAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction() =>
            new RandomizeIntegerAction(m_variable.Init(nameof(m_variable), Node),
                m_min.Init(nameof(m_min), Node), 
                m_max.Init(nameof(m_max), Node));

        public void Verify(ref VerificationResult result)
        {
            if (m_variable.IsNone())
                result.Error("variable not set", this);
        }
    }
}
