﻿using System;
using System.Globalization;
using Core.Unity.Attributes;
using Core.Unity.Interface;
using ScriptableUtility.Actions;
using ScriptableUtility.Actions.Time;
using ScriptableUtility.Variables;
using UnityEngine;
using nodegraph;
using nodegraph.Attributes;

namespace ScriptableUtility.ActionConfigs.Time
{
    [EditorIcon("icon-action")]
    public class Wait : ScriptableBaseAction, IVerify
    {
        [SerializeField, Input]
        internal VarReference<float> m_timeSeconds = new VarReference<float>();

        string TimeName
        {
            get
            {
                if (m_timeSeconds.ContextType == ReferenceContextType.Intrinsic)
                    return m_timeSeconds.IntrinsicValue.ToString(CultureInfo.CurrentCulture);
                return m_timeSeconds.Scriptable != null ? m_timeSeconds.Scriptable.name : "?";
            }
        }
        public override string Name => $"WAIT {TimeName}";
        static Type StaticFactoryType => typeof(WaitAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction()
        {
            //m_timeSeconds.Init(ctx);
            var action = new WaitAction(m_timeSeconds.Init(nameof(m_timeSeconds), Node));
            action.SetDebugName(Name);
            return action;
        }

        public void Verify(ref VerificationResult result)
        {
            if (m_timeSeconds.IsNone())
                result.Error("Time variable not set", this);
            if (m_timeSeconds.ContextType == ReferenceContextType.Intrinsic && m_timeSeconds.IntrinsicValue <= 0f)
                result.Error("Time has to be a positive value", this);
        }
    }
}