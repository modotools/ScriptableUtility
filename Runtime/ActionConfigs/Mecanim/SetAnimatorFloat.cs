﻿using System;
using Core.Unity.Attributes;
using Core.Unity.Types;
using ScriptableUtility.Actions.Mecanim;
using UnityEngine;
using nodegraph.Attributes;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.ActionConfigs.Mecanim
{
    [EditorIcon("icon-action")]
    public class SetAnimatorFloat : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        //[RequiredField]
        //[CheckForComponent(typeof(Animator))]
        [Tooltip("The target.")]
        [SerializeField, Input] internal VarReference<GO> m_gameObject;

        //[RequiredField]
        [Tooltip("The animator parameter")]
        [SerializeField] internal AnimatorFloatRef m_parameter;

        [Tooltip("The float value to assign to the animator parameter")]
        [SerializeField, Input] internal VarReference<float> m_value;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => $"{nameof(SetAnimatorFloat)}";
        static Type StaticFactoryType => typeof(SetAnimatorFloatAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() =>
            new SetAnimatorFloatAction(
                m_gameObject.Init(nameof(m_gameObject), Node), 
                m_parameter, 
                m_value.Init(nameof(m_value), Node));
    }
}
