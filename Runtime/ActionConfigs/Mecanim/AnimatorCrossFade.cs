﻿using System;
using Core.Unity.Attributes;
using ScriptableUtility.Actions.Mecanim;
using UnityEngine;
using nodegraph.Attributes;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.ActionConfigs.Mecanim
{
    [EditorIcon("icon-action")]
    public class AnimatorCrossFade : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        //[RequiredField]
        //[CheckForComponent(typeof(Animator))]
        [Tooltip("The target. An Animator component is required")]
        [SerializeField, Input] internal VarReference<GO> m_gameObject;

        [Tooltip("The name of the state that will be played.")]
        //AnimatorStateRef
        [SerializeField, Input] internal VarReference<string> m_stateName;

        [Tooltip("The duration of the transition. Value is in source state normalized time.")]
        [SerializeField, Input] internal VarReference<float> m_transitionDuration;

        [Tooltip("Layer index containing the destination state. Leave to none to ignore")]
        [SerializeField, Input] internal VarReference<int> m_layer;

        [Tooltip("Start time of the current destination state. Value is in source state normalized time, should be between 0 and 1.")]
        [SerializeField, Input] internal VarReference<float> m_normalizedTime;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => $"{nameof(AnimatorCrossFade)}";
        static Type StaticFactoryType => typeof(AnimatorCrossFadeAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() =>
            new AnimatorCrossFadeAction(
                m_gameObject.Init(nameof(m_gameObject), Node),
                m_stateName.Init(nameof(m_stateName), Node),
                m_transitionDuration.Init(nameof(m_transitionDuration), Node),
                m_layer.Init(nameof(m_layer), Node),
                m_normalizedTime.Init(nameof(m_normalizedTime), Node));
    }
}