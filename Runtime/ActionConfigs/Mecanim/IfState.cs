﻿using System;
using System.Collections.Generic;
using Core.Unity.Attributes;
using nodegraph;
using ScriptableUtility.Actions.Mecanim;
using UnityEngine;
using nodegraph.Attributes;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.ActionConfigs.Mecanim
{
    [EditorIcon("icon-action")]
    // [Tooltip("Check the current State name on a specified layer, this is more than the layer name, it holds the current state as well.")]
    public class IfState : ScriptableBaseAction, ILinkActionConfigs
    {
        //[RequiredField]
        //[CheckForComponent(typeof(Animator))]
        [Tooltip("The target. An Animator component and a PlayMakerAnimatorProxy component are required")]
        [Input]
        public VarReference<GO> GameObject;

        //[RequiredField]
        [Tooltip("The layer's index")]
        [Input]
        public VarReference<int> LayerIndex;

        //[UIHint(UIHint.Animation)]
        [Tooltip("The name to check the layer against.")]
        [Input]
        //AnimatorStateRef
        public VarReference<string> StateName;

        //[ActionSection("Results")]
        //[UIHint(UIHint.Variable)]
        [Tooltip("True if name matches")]
        [Input]
        public VarReference<bool> IsMatching;

        [Tooltip("Action to do if name matches"), NodeConnector(IO.Output, typeof(ScriptableActionConnector))]
        public ScriptableBaseAction NameMatchAction;

        [Tooltip("Action to do if name doesn't match"), NodeConnector(IO.Output, typeof(ScriptableActionConnector))]
        public ScriptableBaseAction NameDoNotMatchEvent;

        public bool CheckNextInfoDoNotMatchAsWell = true;

        public override string Name => nameof(IfState);
        static Type StaticFactoryType => typeof(IfStateAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() =>
            new IfStateAction(GameObject.Init(nameof(GameObject), Node),
                LayerIndex.Init(nameof(LayerIndex), Node),
                StateName.Init(nameof(StateName), Node), 
                IsMatching.Init(nameof(IsMatching), Node), 
                CheckNextInfoDoNotMatchAsWell);

        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            IDefaultAction defMatchAction = null;
            if (NameMatchAction != null)
                defMatchAction = (IDefaultAction) graphActions[NameMatchAction];

            IDefaultAction defNoMatchAction = null;
            if (NameDoNotMatchEvent != null)
                defNoMatchAction = (IDefaultAction) graphActions[NameDoNotMatchEvent];

            (graphActions[this] as IfStateAction)?.LinkActions(defMatchAction, defNoMatchAction);
        }

        public IEnumerable<IActionConfig> GetActionConfigsLinked() => new[] {NameMatchAction, NameDoNotMatchEvent};
        public IEnumerable<ScriptableObject> Dependencies => new[] {NameMatchAction, NameDoNotMatchEvent};
    }
}
