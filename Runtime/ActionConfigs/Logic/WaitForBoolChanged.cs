﻿using System;
using Core.Unity.Attributes;
using Core.Unity.Interface;
using ScriptableUtility.Actions;
using ScriptableUtility.Actions.Logic;
using ScriptableUtility.Variables;
using UnityEngine;
using nodegraph;
using nodegraph.Attributes;

namespace ScriptableUtility.ActionConfigs.Logic
{
    [EditorIcon("icon-action")]
    public class WaitForBoolChanged : ScriptableBaseAction, IVerify
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField, Input]
        internal VarReference<bool> m_bool;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => nameof(WaitForBoolChanged);
        static Type StaticFactoryType => typeof(WaitForBoolChangedAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() 
            => new WaitForBoolChangedAction(m_bool.Init(nameof(m_bool), Node));

        public void Verify(ref VerificationResult result)
        {
            if (m_bool.Scriptable == null)
                result.Error($"bool variable is not set", this);
        }
    }
}
