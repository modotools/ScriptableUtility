﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Unity.Attributes;
using Core.Unity.Interface;
using nodegraph;
using ScriptableUtility.Actions.Logic;
using UnityEngine;
using nodegraph.Attributes;

namespace ScriptableUtility.ActionConfigs.Logic
{
    [EditorIcon("icon-action")]
    public class IntCheck : ScriptableBaseAction, ILinkActionConfigs, IVerify
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] string m_name;
#pragma warning restore 0649 // wrong warnings for SerializeField

        [Input] 
        public VarReference<int> IntRef;

        [Input]
        public VarReference<int> BreakValue;

        [NodeConnectorList(IO.Output, typeof(ScriptableActionConnector))]
        public ScriptableBaseAction[] Actions;

        public IntCheckAction.FinishRule FinishRule;

        public bool WaitForIntChange;
        public bool DontUpdateIntCheck;

        public override string Name => m_name.IsNullOrEmpty() ? $"{nameof(IntCheck)}" : m_name;
        static Type StaticFactoryType => typeof(IntCheckAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction()
        {
            var brkVal = BreakValue.Init(nameof(BreakValue), Node);
            var rules = new IntCheckAction.Rules()
            {
                BreakByVariable = !brkVal.IsNone(),
                FinishRule = FinishRule,
                BreakValue = brkVal,
                WaitForIntChange = WaitForIntChange,
                DontUpdateIntCheck = DontUpdateIntCheck
            };
            return new IntCheckAction(
                IntRef.Init(nameof(IntRef), Node),
                rules);
        }
        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (!(graphActions[this] is IntCheckAction thisAction)) 
                return;

            var intCheckActions = new IBaseAction[Actions.Length];
            for (var i = 0; i < Actions.Length; i++)
                intCheckActions[i] = graphActions[Actions[i]];

            thisAction.LinkActions(intCheckActions);
        }

        public IEnumerable<IActionConfig> GetActionConfigsLinked() => Actions;
        public IEnumerable<ScriptableObject> Dependencies => Actions;

        public void Verify(ref VerificationResult result)
        {
            if (IntRef.Scriptable == null)
                result.Error($"Int Reference is not set", this);

            switch (FinishRule)
            {
                case IntCheckAction.FinishRule.ActionOnceDone:
                    {
                        if (Actions.Count(a=>a != null) <= 0)
                            result.Error($"FinishRule is action once done but no actions defined!", this);
                        break;
                    }
                case IntCheckAction.FinishRule.BreakConditionOnly:
                    {
                        var brkVal = BreakValue.Init(nameof(BreakValue), Node);
                        if (brkVal.IsNone())
                            result.Error($"FinishRule is BreakConditionOnly, but BreakValue not valid!", this);
                        break;
                    }
                case IntCheckAction.FinishRule.NothingTodo: break;
                default: throw new ArgumentOutOfRangeException();
            }

            var childResult = VerificationResult.Default;
            foreach (var a in Actions)
            {
                if (!(a is IVerify v))
                    continue;
                v.Verify(ref childResult);
            }
            if (childResult.Errors > 0)
                result.Error($"nested actions have errors!", this);
            else if (childResult.Warnings > 0)
                result.Warning($"nested actions have warnings!", this);
        }

        
#if UNITY_EDITOR
        public static string Editor_NameProperty => nameof(m_name);
#endif
    }
}
