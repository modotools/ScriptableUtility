﻿using System;
using System.Collections.Generic;
using Core.Unity.Attributes;
using nodegraph;
using ScriptableUtility.Actions.Logic;
using ScriptableUtility.Attributes;
using UnityEngine;
using nodegraph.Attributes;

namespace ScriptableUtility.ActionConfigs.Logic
{
    [EditorIcon("icon-action")]
    public class Loop : ScriptableBaseAction, ILinkActionConfigs
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField, Input] 
        internal VarReference<int> m_count;
        [SerializeField, Input] 
        internal VarReference<int> m_counter;

        [SerializeField, NodeConnector(IO.Output, typeof(ScriptableActionConnector))]
        internal ScriptableBaseAction m_loopAction;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => $"{nameof(Loop)}";
        static Type StaticFactoryType => typeof(LoopAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() 
            => new LoopAction(m_count.Init(nameof(m_count), Node), 
                m_counter.Init(nameof(m_counter), Node));


        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (graphActions[this] is LoopAction loopAction) 
                loopAction.LinkActions(graphActions[m_loopAction]);
        }

        public IEnumerable<IActionConfig> GetActionConfigsLinked() => new[] {m_loopAction};
        public IEnumerable<ScriptableObject> Dependencies => new[] {m_loopAction};
    }
}
