﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Core.Unity.Attributes;
using nodegraph;
using ScriptableUtility.Actions.Logic;
using ScriptableUtility.Attributes;
using UnityEngine;
using nodegraph.Attributes;

namespace ScriptableUtility.ActionConfigs.Logic
{
    [EditorIcon("icon-action")]
    public class Probability : ScriptableBaseAction, ILinkActionConfigs
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField, Input] 
        internal VarReference<float> m_chanceOfAction = new VarReference<float>();
        [SerializeField, NodeConnector(IO.Output, typeof(ScriptableActionConnector))] 
        internal ScriptableBaseAction m_followUpAction;
#pragma warning restore 0649 // wrong warnings for SerializeField

        string VariableNamePart
        {
            get
            {
                if (m_chanceOfAction.ContextType == ReferenceContextType.Intrinsic)
                    return $"{m_chanceOfAction.IntrinsicValue}";
                return m_chanceOfAction.Scriptable == null 
                    ? "" 
                    : $"{m_chanceOfAction.Scriptable.name}";
            }
        }

        public override string Name => $"{nameof(Probability)} {VariableNamePart}";

        static Type StaticFactoryType => typeof(ProbabilityAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() 
            => new ProbabilityAction(m_chanceOfAction.Init(nameof(m_chanceOfAction), Node));

        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (graphActions[this] is ProbabilityAction thisAction)
                thisAction.LinkActions((IDefaultAction) graphActions[m_followUpAction]);
        }

        public IEnumerable<IActionConfig> GetActionConfigsLinked() => new[] {m_followUpAction};
        public IEnumerable<ScriptableObject> Dependencies => new[] {m_followUpAction};

    }
}
