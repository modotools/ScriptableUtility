﻿using System;
using Core.Unity.Attributes;
using Core.Unity.Types;
using ScriptableUtility.Actions;
using ScriptableUtility.Actions.Logic;
using ScriptableUtility.Variables;
using UnityEngine;
using nodegraph.Attributes;

using GO = UnityEngine.GameObject;

namespace ScriptableUtility.ActionConfigs.Logic
{
    [EditorIcon("icon-action")]
    public class PropertyToBool : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField, Input] 
        internal VarReference<GO> m_gameObjectRef;
        [SerializeField, Input]
        internal VarReference<bool> m_boolRef;

        [SerializeField] internal ClassTypeReference m_behaviour;
        [SerializeField] internal string m_propertyName;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => $"{nameof(PropertyToBool)}";
        static Type StaticFactoryType => typeof(PropertyToBoolAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() =>
            new PropertyToBoolAction(
                m_gameObjectRef.Init(nameof(m_gameObjectRef), Node),
                m_boolRef.Init(nameof(m_boolRef), Node), 
                m_behaviour, 
                m_propertyName);

#if UNITY_EDITOR
        public VarReference<GO> MGameObjectRef
        {
            get => m_gameObjectRef;
            set => m_gameObjectRef = value;
        }
        public VarReference<bool> MBoolRef
        {
            get => m_boolRef;
            set => m_boolRef = value;
        }
        public ClassTypeReference MBehaviour
        {
            get => m_behaviour;
            set => m_behaviour = value;
        }
        public string MPropertyName
        {
            get => m_propertyName;
            set => m_propertyName = value;
        }
#endif
    }
}
