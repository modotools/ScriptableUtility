﻿using System;
using System.Collections.Generic;
using Core.Unity.Attributes;
using Core.Unity.Interface;
using nodegraph;
using ScriptableUtility.Actions.Logic;
using UnityEngine;
using nodegraph.Attributes;

namespace ScriptableUtility.ActionConfigs.Logic
{
    [EditorIcon("icon-action")]
    public class BoolCheck : ScriptableBaseAction, ILinkActionConfigs, IVerify
    {
        [Input]
        public VarReference<bool> BoolReference;
        [Input]
        public VarReference<bool> BreakReference;
        [Input]
        public VarReference<bool> BreakValue;

        [NodeConnector(IO.Output, typeof(ScriptableActionConnector)), HideInInspector]
        public ScriptableBaseAction TrueAction;
        [NodeConnector(IO.Output, typeof(ScriptableActionConnector)), HideInInspector]
        public ScriptableBaseAction FalseAction;

        public BoolCheckAction.FinishRule FinishRule;

        public bool WaitForBoolChange;
        public bool DontUpdateBoolCheck;

        public override string Name => $"{nameof(BoolCheck)}";
        static Type StaticFactoryType => typeof(BoolCheckAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction()
        {
            var brkRef = BreakReference.Init(nameof(BreakReference), Node);
            var boolRef = BoolReference.Init(nameof(BoolReference), Node);
            var brkVal = BreakValue.Init(nameof(BreakValue), Node);

            var rules = new BoolCheckAction.Rules()
            {
                BreakByVariable = !brkVal.IsNone(),
                FinishRule = FinishRule,
                BreakValue = brkVal,
                WaitForBoolChange = WaitForBoolChange,
                DontUpdateBoolCheck = DontUpdateBoolCheck
            };

            var breakVar = brkRef.IsNone() ? boolRef : brkRef;
            return new BoolCheckAction(boolRef, breakVar, rules);
        }
        public void LinkActions(IDictionary<IActionConfig, IBaseAction> actions)
        {
            actions.TryGetValue(TrueAction, out var trueAction);
            actions.TryGetValue(FalseAction, out var falseAction);

            (actions[this] as BoolCheckAction)?.LinkActions(trueAction, falseAction);
        }

        public IEnumerable<IActionConfig> GetActionConfigsLinked() => new[] {TrueAction, FalseAction};
        public IEnumerable<ScriptableObject> Dependencies => new[] {TrueAction, FalseAction};

        public void Verify(ref VerificationResult result)
        {
            if (BoolReference.Scriptable == null)
                result.Error($"Bool Reference is not set", this);

            switch (FinishRule)
            {
                case BoolCheckAction.FinishRule.ActionOnceDone:
                {
                    if (TrueAction == null && FalseAction == null)
                        result.Error($"FinishRule is action once done but no actions defined!", this);
                    break;
                }
                case BoolCheckAction.FinishRule.BreakConditionOnly:
                {
                    var brkVal = BreakValue.Init(nameof(BreakValue), Node);
                    if (brkVal.IsNone())
                        result.Error($"FinishRule is BreakConditionOnly, but BreakValue is not valid!", this);
                    break;
                }
                case BoolCheckAction.FinishRule.NothingTodo: break;
                default: throw new ArgumentOutOfRangeException();
            }

            var childResult = VerificationResult.Default;
            if (TrueAction is IVerify trueVerify)
                trueVerify.Verify(ref childResult);
            if (FalseAction is IVerify falseVerify)
                falseVerify.Verify(ref childResult);
            if (childResult.Errors > 0)
                result.Error($"nested actions have errors!", this);
            else if (childResult.Warnings > 0)
                result.Warning($"nested actions have warnings!", this);
        }
    }
}