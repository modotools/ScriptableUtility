﻿using System;
using Core.Unity.Attributes;
using Core.Unity.Interface;
using ScriptableUtility.Actions;
using ScriptableUtility.Actions.Logic;
using ScriptableUtility.Variables;
using UnityEngine;
using nodegraph;
using nodegraph.Attributes;

namespace ScriptableUtility.ActionConfigs.Logic
{
    [EditorIcon("icon-action")]
    public class SetBool : ScriptableBaseAction, IVerify
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField, Input] 
        internal VarReference<bool> m_variable;
        [SerializeField, Input] 
        internal VarReference<bool> m_value;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => nameof(SetBool);
        static Type StaticFactoryType => typeof(SetBoolAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() 
            => new SetBoolAction(
            m_variable.Init(nameof(m_variable), Node), 
            m_value.Init(nameof(m_value), Node));

        public void Verify(ref VerificationResult result)
        {
            if (m_variable.Scriptable == null)
                result.Error($"bool variable is not set", this);
        }
    }
}
