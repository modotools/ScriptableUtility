﻿using System;
using System.Collections.Generic;
using Core.Unity.Attributes;
using Core.Unity.Interface;
using nodegraph;
using ScriptableUtility.Actions.Logic;
using UnityEngine;
using nodegraph.Attributes;

namespace ScriptableUtility.ActionConfigs.Logic
{
    [EditorIcon("icon-action")]
    public class RepeatUntil : ScriptableBaseAction, ILinkActionConfigs, IVerify
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField, NodeConnector(IO.Output, typeof(ScriptableActionConnector))] 
        internal ScriptableBaseAction m_action;
        [SerializeField, Input]
        internal VarReference<bool> m_breakCondition;
        [SerializeField, Input]
        internal VarReference<bool> m_isValue;
        [SerializeField] internal bool m_abortOnBreak;
        [SerializeField] internal string m_name;
#pragma warning restore 0649 // wrong warnings for SerializeField
        public override string Name => string.IsNullOrEmpty(m_name) ? nameof(RepeatUntil) : $"R|{m_name}";
        static Type StaticFactoryType => typeof(RepeatUntilAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction()
        {
            var action = new RepeatUntilAction(
                m_breakCondition.Init(nameof(m_breakCondition), Node),
                m_isValue.Init(nameof(m_isValue), Node), 
                m_abortOnBreak);

            action.SetDebugName(Name);
            return action;
        }
        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if (graphActions[this] is RepeatUntilAction action)
                action.LinkActions(graphActions[m_action]);
        }
        public IEnumerable<IActionConfig> GetActionConfigsLinked() => new[] { m_action };
        public IEnumerable<ScriptableObject> Dependencies => new[] {m_action};

#if UNITY_EDITOR
        public ScriptableBaseAction Editor_ActionConfig
        {
            get => m_action;
            set => m_action = value;
        }
        public static string Editor_NameProperty => nameof(m_name);
        public static string Editor_BreakConditionProperty => nameof(m_breakCondition);
        public static string Editor_BreakValueProperty => nameof(m_isValue);
        public static string Editor_AbortOnBreakProperty => nameof(m_abortOnBreak);
#endif
        public void Verify(ref VerificationResult result)
        {
            if (m_action == null)
                result.Error($"No action set to repeat", this);
            if (m_breakCondition.Scriptable == null)
                result.Warning($"No BreakCondition variable set", this);

            var childResult = VerificationResult.Default;
            if (m_action is IVerify childVerify)
                childVerify.Verify(ref childResult);
            if (childResult.Errors > 0)
                result.Error($"Repeating action with errors", this);
            if (childResult.Warnings > 0)
                result.Warning($"Repeating action with warnings", this);
        }
    }
}
