﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Extensions;
using Core.Unity.Attributes;
using Core.Unity.Interface;
using nodegraph;
using ScriptableUtility.Actions.Logic;
using UnityEngine;
using nodegraph.Attributes;

namespace ScriptableUtility.ActionConfigs.Logic
{
    [EditorIcon("icon-action")]
    [NodeWidth(100)]
    public class ActionSequence : ScriptableBaseAction, ILinkActionConfigs, IVerify
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField, HideInInspector, NodeConnectorList(IO.Output, typeof(ScriptableActionConnector))] 
        internal List<ScriptableBaseAction> m_actions = new List<ScriptableBaseAction>();
        [SerializeField, HideInInspector] string m_name;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => string.IsNullOrEmpty(m_name) ? "Sequence" : $"S|{m_name}";
        static Type StaticFactoryType => typeof(ActionSequenceAction);
        public override Type FactoryType => StaticFactoryType;

        public List<ScriptableBaseAction> Sequence => m_actions;

        public override IBaseAction CreateAction()
        {
            //Debug.Log($"CreateAction {Name}");
            var action = new ActionSequenceAction();
            action.SetDebugName(Name+ GetInstanceID());
            return action;
        }

        public void LinkActions(IDictionary<IActionConfig, IBaseAction> graphActions)
        {
            if(!(graphActions[this] is ActionSequenceAction sequence))
                return;
            var enabledCount = m_actions.Count(a => a.Enabled);
            var seqActions = new IBaseAction[enabledCount];
            var targetIdx = 0;
            for (var i = 0; i < m_actions.Count; i++)
            {
                if (!m_actions[i].Enabled)
                    continue;
                if (m_actions[i] == null)
                {
                    Debug.LogError($"Action null at index {i} in Sequence {Name}");
                    continue;
                }
                //Debug.Log($"{Name} {i}: {graphActions[m_actions[i]]}");
                seqActions[targetIdx] = graphActions[m_actions[i]];
                targetIdx++;
            }
            sequence.LinkActions(seqActions);
        }

        public IEnumerable<IActionConfig> GetActionConfigsLinked() => m_actions.ToArray();
        public IEnumerable<ScriptableObject> Dependencies => m_actions;

#if UNITY_EDITOR
        public static string Editor_NameProperty => nameof(m_name);
        public static string Editor_ActionsProperty => nameof(m_actions);
#endif
        public void Verify(ref VerificationResult result)
        {
            if (m_actions.IsNullOrEmpty())
            {
                result.Error($"Action Sequence is null or empty!", this);
                return;
            }

            if (m_actions.Any(a => a  == null))
                result.Error($"Action Sequence has empty elements!", this);

            var children = VerificationResult.Default;
            foreach (var a in m_actions)
            {
                if (!(a is IVerify v))
                    continue;
                v.Verify(ref children);
            }

            if (children.Errors > 0)
                result.Error($"Action Sequence child actions with errors!", this);
            else if (children.Warnings > 0)
                result.Warning($"Action Sequence child actions with warnings!", this);
        }
    }
}
