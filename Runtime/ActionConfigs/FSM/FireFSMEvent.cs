﻿using System;
using Core.Interface;
using Core.Unity.Types.Fsm;
using ScriptableUtility.Behaviour;
using UnityEngine;
using nodegraph.Attributes;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.ActionConfigs.FSM
{
    public class FireFSMEvent : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        // todo 3: playmaker style selector for self would be nice
        [SerializeField, Input] VarReference<GO> m_fsmObject;
        // todo 3: remove the need for linking the fsm-component here, maybe like in anim-refs link either Animator or Controller.
        [SerializeField, FSMDrawer] FSMEventRef m_event;
#pragma warning restore 0649 // wrong warning

        public override string Name => nameof(FireFSMEvent);

        static Type StaticFactoryType => typeof(FireFSMEventAction);
        public override Type FactoryType => StaticFactoryType;

        public override IBaseAction CreateAction() =>
            new FireFSMEventAction(
                m_fsmObject.Init(nameof(m_fsmObject), Node),
                m_event.EventName);
    }

    public class FireFSMEventAction: IDefaultAction
    {
        readonly IVar<GO> m_fsmObject;
        readonly string m_eventName;

        public FireFSMEventAction(IVar<GO> fsmObject, string eventName)
        {
            m_fsmObject = fsmObject;
            m_eventName = eventName;
        }

        public void Invoke()
        {
            var fsmGo = m_fsmObject.Value;
            if (fsmGo == null || !fsmGo.TryGetComponent(out FSMBehaviour fsmBehaviour))
                return;
            fsmBehaviour.SendEvent(m_eventName);
        }
    }
}
