﻿using System;
using Core.Unity.Attributes;
using ScriptableUtility.Actions;
using ScriptableUtility.Actions.Rigidbody;
using ScriptableUtility.Variables;
using UnityEngine;
using UnityEngine.Serialization;
using nodegraph.Attributes;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.ActionConfigs.Rigidbody
{
    [EditorIcon("icon-action")]
    public class AddForce : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        //[RequiredField]
        //[CheckForComponent(typeof(Rigidbody))]
        [Tooltip("The GameObject to apply the force to.")]
        [SerializeField, Input] internal VarReference<GO> m_rigidbodyObject;

        //[UIHint(UIHint.Variable)]
        [Tooltip("Optionally apply the force at a position on the object. This will also add some torque. The position is often returned from MousePick or GetCollisionInfo actions.")]
        [SerializeField, Input] internal VarReference<Vector3> m_atPosition;

        //[UIHint(UIHint.Variable)]
        [Tooltip("A Vector3 force to add. Optionally override any axis with the X, Y, Z parameters.")]
        [SerializeField, Input] internal VarReference<Vector3> m_vector;

        [Tooltip("Force along the X axis. To leave unchanged, set to 'None'.")]
        [SerializeField, Input] internal VarReference<float> m_x;

        [Tooltip("Force along the Y axis. To leave unchanged, set to 'None'.")]
        [SerializeField, Input] internal VarReference<float> m_y;

        [Tooltip("Force along the Z axis. To leave unchanged, set to 'None'.")]
        [SerializeField, Input] internal VarReference<float> m_z;

        [Tooltip("Apply the force in world or local space.")]
        [SerializeField] internal Space m_space;

        [Tooltip("The type of force to apply. See Unity Physics docs.")]
        [SerializeField] internal ForceMode m_forceMode;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => nameof(AddForce);
        static Type StaticFactoryType => typeof(AddForceAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() =>
            new AddForceAction(
                m_rigidbodyObject.Init(nameof(m_rigidbodyObject), Node),
                m_atPosition.Init(nameof(m_atPosition), Node),
                m_vector.Init(nameof(m_vector), Node),
                m_x.Init(nameof(m_x), Node),
                m_y.Init(nameof(m_y), Node),
                m_z.Init(nameof(m_z), Node), 
                m_space, m_forceMode);
    }
}
