﻿using System;
using System.Collections.Generic;
using Core.Extensions;
using Core.Interface;
using Core.Unity.Attributes;
using nodegraph;
using nodegraph.Attributes;
using ScriptableUtility.Actions.GameObject;
using UnityEngine;
using Object = UnityEngine.Object;

namespace ScriptableUtility.ActionConfigs.GameObject
{
    [EditorIcon("icon-action")]
    public class DestroyComponents : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        // type: component
        [SerializeField, NodeConnectorList(IO.Input, typeof(VarReference<Object>), syncData: false)] 
        internal List<VarReference<Object>> m_components;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override string Name => $"{nameof(DestroyComponents)}";
        static Type StaticFactoryType => typeof(DestroyComponentsAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() 
            => new DestroyComponentsAction(m_components.Init($"{nameof(m_components)}", Node));
    }
}
