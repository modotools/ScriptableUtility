﻿using System;
using Core.Unity.Attributes;
using ScriptableUtility.Actions.GameObject;
using UnityEngine;
using nodegraph.Attributes;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.ActionConfigs.GameObject
{
    [EditorIcon("icon-action")]
    public class SetGameObject : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField, Input] internal VarReference<GO> m_variableRef;
        [SerializeField, Input] internal VarReference<GO> m_gameObjectRef;
#pragma warning restore 0649 // wrong warnings for SerializeField
        public override string Name => $"{nameof(SetGameObject)}";
        static Type StaticFactoryType => typeof(SetGameObjectAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() =>
            new SetGameObjectAction(
                m_variableRef.Init(nameof(m_variableRef), Node),
                m_gameObjectRef.Init(nameof(m_gameObjectRef), Node));
    }
}
