﻿using System;
using Core.Unity.Attributes;
using ScriptableUtility.Actions;
using ScriptableUtility.Actions.GameObject;
using ScriptableUtility.Variables;
using UnityEngine;
using nodegraph.Attributes;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.ActionConfigs.GameObject
{
    [EditorIcon("icon-action")]
    public class Parent : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<GO> m_newParent;
        [SerializeField, Input] internal VarReference<GO> m_child;
#pragma warning restore 0649 // wrong warning

        public override string Name => $"{nameof(Parent)}";
        static Type StaticFactoryType => typeof(ParentAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() =>
            new ParentAction(
                m_newParent.Init(nameof(m_newParent), Node),
                m_child.Init(nameof(m_child), Node));
    }
}
