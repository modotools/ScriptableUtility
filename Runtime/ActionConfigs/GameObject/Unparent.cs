﻿using System;
using Core.Unity.Attributes;
using ScriptableUtility.Actions.GameObject;
using UnityEngine;
using nodegraph.Attributes;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.ActionConfigs.GameObject
{
    [EditorIcon("icon-action")]
    public class Unparent : ScriptableBaseAction
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<GO> m_unparentObj;
#pragma warning restore 0649 // wrong warning

        public override string Name => $"{nameof(Unparent)}";
        static Type StaticFactoryType => typeof(UnparentAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() 
            => new UnparentAction(m_unparentObj.Init(nameof(m_unparentObj), Node));
    }
}
