﻿using System;
using Core.Unity.Attributes;
using ScriptableUtility.Actions.GameObject;
using UnityEngine;
using nodegraph.Attributes;
using GO = UnityEngine.GameObject;

namespace ScriptableUtility.ActionConfigs.GameObject
{
    [EditorIcon("icon-action")]
    // [Tooltip("Start/Stop emitting trail")]
    public class Spawn : ScriptableBaseAction
    {
        // [RequiredField]
        // [CheckForComponent(typeof(TrailRenderer))]
#pragma warning disable 0649 // wrong warning for SerializeField
        [SerializeField, Input] internal VarReference<GO> m_spawnPosition;
        [SerializeField, Input] internal VarReference<GO> m_spawnPrefab;
        [SerializeField, Input] internal VarReference<GO> m_spawnedInstance;
#pragma warning restore 0649 // wrong warning

        public override string Name => $"{nameof(Spawn)}";
        static Type StaticFactoryType => typeof(SpawnAction);
        public override Type FactoryType => StaticFactoryType;
        public override IBaseAction CreateAction() =>
            new SpawnAction(
                m_spawnPosition.Init(nameof(m_spawnPosition), Node),
                m_spawnPrefab.Init(nameof(m_spawnPrefab), Node),
                m_spawnedInstance.Init(nameof(m_spawnedInstance), Node));
    }
}