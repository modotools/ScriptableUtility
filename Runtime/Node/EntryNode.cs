﻿using System;
using UnityEngine;
using nodegraph;
using nodegraph.Attributes;
using nodegraph.Data;
using nodegraph.Interface;
using nodegraph.Operations;

namespace ScriptableUtility.Nodes
{
    [DisallowMultipleNodes, 
     NodeTint(0f,0.5f,0.25f),
     NodeWidth(150)]
    public abstract class EntryNode<T, TC> : Node, IEntryNode where TC : INodeConnector<T> 
    {
        public Type GetEntryType() => typeof(T);
        public string Out_FieldName => nameof(OUT);

        [HideInInspector, Output] 
        public TC OUT;

        public override INodeData NodeData => this;
        public INode Node => this;

        // Use this for initialization
        //protected override void Init() {
        //    base.Init();
        //}

        // Return the correct value of an output port when requested
        public override object GetOutputValue(PortID port)
        {
            var node = Graph.GetConnectionFrom(port).To.Node as INode;
            return node?.NodeData;
        }

    }

    public interface IEntryNode : IFullNode
    {
        // todo: Entry Node could have multiple outputs
        // Type GetEntryType();
        // string Out_FieldName { get; }
    }

    public interface IEntryNodeGraph<out T> : IEntryNodeGraph
    {
        T Entry { get; }
    }

    public interface IEntryNodeGraph : INodeGraph
    {
        INode EntryNode { get; }
        INodeData EntryNodeData { get; }
    }
}