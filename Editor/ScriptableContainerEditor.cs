﻿using System;
using System.Collections.Generic;
using Core.Editor.Extensions;
using Core.Unity.Extensions;
using UnityEditor;
using UnityEngine;
using static Core.Editor.Settings.CoreSettingsAccess;
using Object = UnityEngine.Object;
using UEditor = UnityEditor.Editor;

namespace ScriptableUtility.Editor
{
    [CustomEditor(typeof(ScriptableContainer))]
    public class ScriptableContainerEditor : UEditor
    {
        // ReSharper disable once InconsistentNaming
        new ScriptableContainer target => base.target as ScriptableContainer;

        List<Type> m_variableTypes = new List<Type>();

        readonly List<SerializedProperty> m_values = new List<SerializedProperty>();

        GenericMenu m_addVariableOfTypeMenu;
        ScriptableContainer m_lastTarget;

        void OnEnable()
        {
            InitTypes();
        }

        public override void OnInspectorGUI()
        {
            if (m_lastTarget != target || m_lastTarget.VariableObjs.Count != m_values.Count)
                InitTarget();

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var deleteIdx = -1;
                for (var i = 0; i < target.VariableObjs.Count; i++)
                {
                    VariableGUI(i, ref deleteIdx);
                }

                DeleteVariable(deleteIdx);
                AddVariable();

                if (!check.changed)
                    return;
                UpdateChanges();
            }
        }

        void UpdateChanges()
        {
            serializedObject.Update();
            EditorUtility.SetDirty(target);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(target));
        }

        void VariableGUI(int i, ref int deleteIdx)
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                if (GUILayout.Button("X", GUILayout.Width(20)))
                    deleteIdx = i;
                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    target.VariableObjs[i].name = EditorGUILayout.DelayedTextField(target.VariableObjs[i].name);
                    if (check.changed)
                        AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(target.VariableObjs[i]));
                }

                if (m_values[i] != null)
                {
                    using (new EditorGUILayout.VerticalScope())
                        EditorGUILayout.PropertyField(m_values[i], new GUIContent(""));
                } 
                else if(target.VariableObjs[i] is IScriptableVariable var)
                    EditorGUILayout.LabelField($"{var.VariableType}");
            }
        }

        void AddVariable()
        {
            if (!GUILayout.Button("Add Variable"))
                return;
            m_addVariableOfTypeMenu.ShowAsContext();
        }

        void DeleteVariable(int deleteIdx)
        {
            if (deleteIdx == -1)
                return;

            target.VariableObjs[deleteIdx].DestroyEx();
            target.VariableObjs.RemoveAt(deleteIdx);

            m_values.RemoveAt(deleteIdx);
            //UpdateChanges();
        }

        void InitTarget()
        {
            m_lastTarget = target;
            target.VariableObjs.Clear();
            m_values.Clear();

            if (target == null)
                return;

            var objs = AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(target));
            foreach (var o in objs)
            {
                if (AssetDatabase.IsMainAsset(o))
                    continue;
                if (!(o is ScriptableObject so))
                    continue;
                if (!(o is IScriptableVariable))
                    continue;
                target.VariableObjs.Add(so);

            }
            target.VariableObjs.Sort((a,b)=> string.Compare(a.name, b.name, StringComparison.Ordinal));
            foreach (var v in target.VariableObjs)
                AddValueProperty(v);
        }

        void AddValueProperty(Object so)
        {
            var serObj = new SerializedObject(so);
            var prop = serObj.FindProperty("m_value");
            m_values.Add(prop);
        }

        void InitTypes()
        {
            EditorExtensions.ReflectionGetAllTypes(typeof(IScriptableVariable), ref m_variableTypes, out var variableNames, out var namespaces);

            m_addVariableOfTypeMenu = EditorExtensions.CreateGenericMenuFromTypes(m_variableTypes, variableNames, namespaces, MenuFilter, OnVariableTypeSelected);
        }

        void OnVariableTypeSelected(object varType)
        {
            var t = (Type) varType;

            var newVariable = CreateInstance(t);
            //if (AssetDatabase.IsSubAsset(target))
            //    newVariable.hideFlags = HideFlags.HideInInspector | HideFlags.HideInHierarchy;
            AssetDatabase.AddObjectToAsset(newVariable, target);
            target.VariableObjs.Add(newVariable);
            AddValueProperty(newVariable);

            UpdateChanges();
        }
    }
}
