﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Core.Editor.Inspector;
using UnityEditor;
using UnityEngine;

using static ScriptableUtility.ScriptableTypesGenerator;

namespace ScriptableUtility
{
    [CustomEditor(typeof(ScriptableTypesGenerator))]
    public class ScriptableTypesGeneratorInspector : BaseInspector<ScriptableTypesGeneratorEditor>
    {
    }

    public class ScriptableTypesGeneratorEditor : BaseEditor<ScriptableTypesGenerator>
    {
        public override string Name => ObjectNames.NicifyVariableName(nameof(ScriptableTypesGenerator));

        // Start is called before the first frame update
        public override void OnGUI(float width)
        {
            base.OnGUI(width);

            GUILayout.Label($"{Target.Variables.Namespace}");
            GUILayout.Label($"{Target.Nodes.Namespace}");

            GenerateButtonGUI();
        }

        void GenerateButtonGUI(string btnText = "Generate")
        {
            if (!GUILayout.Button(btnText))
                return;

            var generatedFiles = Enum.GetNames(typeof(GeneratedTypes)).Length;

            var varArgs = new string[Enum.GetNames(typeof(VarArgs)).Length];
            var defaultUsings = new string[generatedFiles];
            var usings = new string[generatedFiles];

            GetDefaultUsingDeclarations(defaultUsings);

            foreach (var t in Target.Types)
            {
                //var variableOutputPath = Target.VariableOutputFile(t);
                //var nodeOutputPath = Target.NodeOutputFile(t);
                //var eventsOutputPath = Target.EventOutputFile(t);
                //var listenerOutputPath = Target.ListenerOutputFile(t);

                for (var i = 0; i < generatedFiles ; i++) 
                    usings[i] = $"{defaultUsings[i]}";

                var varTextAsset = PickTextAssetForVariable(t);

                varArgs[(int) VarArgs.Name] = t.TypeName;
                var typeRefType = t.Type.Type.Type;
                varArgs[(int) VarArgs.Type] = typeRefType != null ? typeRefType.Name : t.Type.Text;

                AddVarTypeNamespaceUsing(typeRefType, usings);
                AddOptionalUsingDeclarations(t, usings);
                SetConverterType(t, ref usings[(int) GeneratedTypes.Variable], varArgs);

                Generate(varArgs, usings[(int) GeneratedTypes.Variable], Target.Variables.Namespace, varTextAsset, Target.Variables.OutputFile(t));
                Generate(varArgs, usings[(int) GeneratedTypes.Node], Target.Nodes.Namespace, Target.NodesTemplate, Target.Nodes.OutputFile(t));
                Generate(varArgs, usings[(int) GeneratedTypes.Events], Target.Events.Namespace, Target.EventsTemplate, Target.Events.OutputFile(t));
                Generate(varArgs, usings[(int) GeneratedTypes.Listener], Target.Listener.Namespace, Target.ListenerTemplate, Target.Listener.OutputFile(t));

            }
        }

        void GetDefaultUsingDeclarations(IList<string> outStr)
        {
            GetDefaultUsingDeclaration(outStr, GeneratedTypes.Variable, Target.Variables.Usings);
            GetDefaultUsingDeclaration(outStr, GeneratedTypes.Node, Target.Nodes.Usings);
            GetDefaultUsingDeclaration(outStr, GeneratedTypes.Events, Target.Events.Usings);
            GetDefaultUsingDeclaration(outStr, GeneratedTypes.Listener, Target.Listener.Usings);
        }

        static void GetDefaultUsingDeclaration(IList<string> str, GeneratedTypes t, IEnumerable<string> usings) 
            => str[(int) t] = usings.Aggregate("", (current, vn) => current + $"using {vn};\r\n");

        static void Generate(string[] varArgs, string usings, string @namespace, TextAsset textAsset, string outputPath)
        {
            varArgs[(int) VarArgs.Using] = usings;
            varArgs[(int) VarArgs.Namespace] = @namespace;
            GenerateCode(textAsset, outputPath, varArgs);
        }

        static void SetConverterType(ScriptableType t, ref string usingsTextVariable, IList<string> varArgs)
        {
            if (t.ConverterType.Type == null) 
                return;
            usingsTextVariable += $"using {t.ConverterType.Type.Namespace};\r\n";
            varArgs[(int) VarArgs.Converter] = $"{t.ConverterType.Type.Name}";
        }

        static void AddOptionalUsingDeclarations(ScriptableType t, IList<string> usings)
        {
            if (string.IsNullOrEmpty(t.UsingDeclaration)) 
                return;

            var extraUsing = $"{t.UsingDeclaration}\r\n";
            for (var i = 0; i < usings.Count; i++)
                usings[i] += extraUsing;
        }

        void AddVarTypeNamespaceUsing(Type typeRefType, IList<string> outUsings)
        {
            if (typeRefType == null) 
                return;
            var @namespace = typeRefType.Namespace;
            var @usingNamespace = $"using {@namespace};\r\n";

            AddNamespaceIfMissing(@namespace, @usingNamespace, GeneratedTypes.Variable, Target.Variables.Usings, outUsings);
            AddNamespaceIfMissing(@namespace, @usingNamespace, GeneratedTypes.Node, Target.Nodes.Usings, outUsings);
            AddNamespaceIfMissing(@namespace, @usingNamespace, GeneratedTypes.Events, Target.Events.Usings, outUsings);
            AddNamespaceIfMissing(@namespace, @usingNamespace, GeneratedTypes.Listener, Target.Listener.Usings, outUsings);
        }

        static void AddNamespaceIfMissing(string @namespace, string usingNamespace, GeneratedTypes t, ICollection<string> defaultUsings, IList<string> outUsings)
        {
            if (!defaultUsings.Contains(@namespace))
                outUsings[(int) t] += usingNamespace;
        }

        TextAsset PickTextAssetForVariable(ScriptableType t)
        {
            var varTextAsset = Target.VariableTemplateNoOverride;
            if (t.IsObjectType)
                varTextAsset = Target.VariableTemplateObject;
            else if (t.ConverterType.Type != null)
                varTextAsset = Target.VariableTemplateConverter;
            return varTextAsset;
        }

        // ReSharper disable once SuggestBaseTypeForParameter
        static void GenerateCode(TextAsset template, string outputFile, string[] replaceArgs)
        {
            var textAssetPath = AssetDatabase.GetAssetPath(template);
            using (var streamReader = new StreamReader(textAssetPath))
            using (var streamWriter = new StreamWriter(outputFile))
            {
                // ReSharper disable once CoVariantArrayConversion
                while (!streamReader.EndOfStream)
                {
                    var line = string.Format(streamReader.ReadLine() ?? throw new InvalidOperationException(), replaceArgs);
                    //line = line.Replace("\\n", "\n");
                    if (line.Contains("\\n"))
                    {
                        var splitLines = line.Split(new[] { "\\n" }, StringSplitOptions.None);
                        foreach (var l in splitLines) 
                            streamWriter.WriteLine(l);
                    }
                    else
                        streamWriter.WriteLine(line);
                }
            }
            AssetDatabase.Refresh();
        }
    }
}
