﻿using System;
using System.IO;
using Core.Editor.Extensions;
using Core.Extensions;
using Core.Types;
using UnityEditor;
using UnityEngine;
using StrPair = System.Tuple<string, string>;

namespace ScriptableUtility.Editor.Settings
{
    internal class ScriptableSettings : ScriptableObject
    {
#pragma warning disable 0649 // wrong warning for SerializeField
        [Serializable] 
        public struct ReplaceString
        {
            public string Namespace;
            public string Category;
        }
#pragma warning restore 0649 // wrong warning

        const string k_configsPath = "Assets/Settings/";
        internal const string K_ConfigLinksPath = k_configsPath + "ScriptableSettings.asset";

        internal static readonly string K_FullConfigLinksPath = Path.GetFullPath(".") + Path.DirectorySeparatorChar + K_ConfigLinksPath;

        public ReplaceString[] ReplaceNamespaces = new ReplaceString[0];
        public string[] SkipRoots = new string[0];

        public static ScriptableSettings I => AssetDatabase.LoadAssetAtPath<ScriptableSettings>(K_ConfigLinksPath);

        public static EditorExtensions.MenuFromNamespace MenuFilter
        {
            get
            {
                var replace = new StrPair[I.ReplaceNamespaces.Length];
                for (var i = 0; i < I.ReplaceNamespaces.Length; i++)
                    replace[i] = new StrPair(I.ReplaceNamespaces[i].Namespace, I.ReplaceNamespaces[i].Category);
                return new EditorExtensions.MenuFromNamespace()
                {
                    ReplaceNamespaces =replace,
                    SkipRoots = I.SkipRoots
                };
            }
        }
    }

    public static class ScriptableSettingsAccess
    {
        public static EditorExtensions.MenuFromNamespace MenuFilter => ScriptableSettings.MenuFilter;
    }

    internal static class ScriptableSettingsUpdater
    {
        const string k_scriptableSettingsInit = "ScriptableSettingsInitialized";
        const string k_packagePath = "Packages/com.sergejkern.scriptable_utility/";

        [InitializeOnLoadMethod]
        static void InitScriptableUtility()
        {
            if (SessionState.GetBool(k_scriptableSettingsInit, false))
                return;
            if (GetOrCreateScriptableSettings(out _) != OperationResult.OK) 
                return;
            SessionState.SetBool(k_scriptableSettingsInit, true);
        }

        static void EnsureCorrectScriptGuidInAsset()
        {
            var contents = File.ReadAllText(ScriptableSettings.K_FullConfigLinksPath);
            
            var scriptIdx = contents.IndexOf("m_Script: ", StringComparison.Ordinal);
            var guidIdx = scriptIdx + contents.Substring(scriptIdx).IndexOf("guid: ", StringComparison.Ordinal) + 6;
            var endGuidIdx = guidIdx + contents.Substring(guidIdx).IndexOf(",", StringComparison.Ordinal);
            var oldGuid = contents.Substring(guidIdx, endGuidIdx-guidIdx);
            var newGuid = AssetDatabase.AssetPathToGUID(k_packagePath + "Editor/Settings/ScriptableSettings.cs");
            if (string.Equals(oldGuid, newGuid, StringComparison.Ordinal))
                return;
            Debug.LogWarning($"ScriptableSettings with wrong guid: {oldGuid} Should be {newGuid}!");
            contents = contents.Replace(oldGuid, newGuid);
            File.WriteAllText(ScriptableSettings.K_FullConfigLinksPath, contents);
        }

        internal static OperationResult GetOrCreateScriptableSettings(out ScriptableSettings settings)
        {
            if (File.Exists(ScriptableSettings.K_FullConfigLinksPath))
            {
                EnsureCorrectScriptGuidInAsset();
                AssetDatabase.Refresh();
                Debug.Log($"ScriptableSettings exist!");

                //repeat
                settings = AssetDatabase.LoadAssetAtPath<ScriptableSettings>(ScriptableSettings.K_ConfigLinksPath);
                if (settings != null)
                {
                    Debug.Log($"ScriptableSettings found!");
                    return OperationResult.OK;
                }

                EditorApplication.delayCall += InitScriptableUtility;
                return OperationResult.Error;
            }

            Debug.LogWarning("No Scriptable-Settings found... creating");
            settings = ScriptableObject.CreateInstance<ScriptableSettings>();

            var dir = Path.GetDirectoryName(ScriptableSettings.K_ConfigLinksPath);
            // ReSharper disable once AssignNullToNotNullAttribute
            if (!dir.IsNullOrEmpty() && !Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            AssetDatabase.CreateAsset(settings, ScriptableSettings.K_ConfigLinksPath);
            AssetDatabase.SaveAssets();
            return OperationResult.OK;
        }
    }
}