﻿using System;
using System.Collections.Generic;
using Core.Unity.Types;
using Core.Unity.Types.Attribute;
using UnityEditor;
using UnityEngine;

namespace ScriptableUtility
{
    [CreateAssetMenu(fileName = nameof(ScriptableTypesGenerator), menuName = "Scriptable/ScriptableTypesGenerator", order = 0)]
    public class ScriptableTypesGenerator : ScriptableObject
    {
        public enum VarArgs
        {
            Using, 
            Namespace,
            Name, 
            Type,
            Converter,
        }
        public enum GeneratedTypes
        {
            Variable,
            Node,
            Events,
            Listener
        }
        [Serializable]
        public struct ScriptableType
        {
            public string TypeName;

            public string UsingDeclaration;
            public TypeArg Type;

            public bool IsObjectType;

            [ClassImplements(typeof(IOverrideValueConverter))]
            public ClassTypeReference ConverterType;
        }

        //[Serializable]
        //public struct TextArg
        //{
            //public string Text;
            //public TextAsset TextAsset;
        //}

        [Serializable]
        public struct TypeArg
        {
            public string Text;
            [ClassTypeConstraint(AllowInterface = true)]
            public ClassTypeReference Type;
        }

        [Serializable]
        public struct OutputSettings
        {
            public DefaultAsset Folder;
            public string Namespace;
            public string FileNameFormat;

            public List<string> Usings;
            public string OutputFile(ScriptableType t) => $"{AssetDatabase.GetAssetPath(Folder)}/{string.Format(FileNameFormat, t.TypeName)}.cs";
        }

        public OutputSettings Variables;
        public OutputSettings Nodes;
        public OutputSettings Events;
        public OutputSettings Listener;

        public TextAsset VariableTemplateNoOverride;
        public TextAsset VariableTemplateObject;
        public TextAsset VariableTemplateConverter;

        public TextAsset NodesTemplate;

        public TextAsset EventsTemplate;
        public TextAsset ListenerTemplate;

        public List<ScriptableType> Types;
    }
}
