﻿using System.Linq;
using Core.Editor.Inspector;
using Core.Editor.Utility;
using Core.Unity.Interface;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.FSM;
using UnityEditor;
using UnityEngine;
using nodegraph.Editor;
using Event = ScriptableUtility.FSM.Event;

namespace ScriptableUtility.Editor.FSM
{
    [CustomEditor(typeof(State), true)]
    [CanEditMultipleObjects]
    public class StateInspector : BaseInspector<StateInspectorEditor>
    {
        public override bool CanDetach => false;
    }

    public class StateInspectorEditor : BaseEditor<State>
    {
        const float k_smallElementWidth = 20f;
        const float k_defaultLabelWidth = 75f;

        GraphInspectorEditor m_graphInspector;

        #region Init
        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);

            InitParentGraphInspector(parentContainer);
            EnsureFSMStateActionValid();
        }

        void InitParentGraphInspector(object parentContainer)
        {
            m_graphInspector = new GraphInspectorEditor() {TargetObj = (FSMGraph) Target.Graph};
            m_graphInspector.Init(parentContainer);
        }

        void EnsureFSMStateActionValid()
        {
            if (Target.FSMStateAction == null)
            {
                Target.FSMStateAction = ScriptableObject.CreateInstance<FSMStateAction>();
                Target.FSMStateAction.AddToAsset(Target.Parent());
                Target.Parent().Reimport();
            }

            Target.FSMStateAction.SetStateName(Target.Name);
        }
        #endregion

        public override void Terminate()
        {
            m_graphInspector.Terminate();
            m_graphInspector = null;

            base.Terminate();
        }

        public override void OnGUI(float width)
        {
            m_graphInspector.OnGUI(width);
            GUILayout.Space(5f);

            if (m_graphInspector.Current == GraphInspectorEditor.InspectorState.Selected)
                StateGUI(width);
        }

        void StateGUI(float width)
        {
            GetWidths(width, out var nameWidth, out _);

            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Label("State: ", EditorStyles.miniBoldLabel, GUILayout.Width(nameWidth));
                EditStateGUI();
            }

            NameGUI();
            InfoGUI();
            EventsGUI(width);

            //base.OnGUI(width);
        }

        static void GetWidths(float width, out float nameWidth, out float conditionWidth)
        {
            const float spacings = 7f;
            var restWidth = width - k_smallElementWidth - spacings;
            nameWidth = restWidth * 0.2f;
            conditionWidth = restWidth * 0.8f;
        }

        void EditStateGUI()
        {
            if (NodeEditorWindow.Current != null && NodeEditorWindow.Current.IsVisible &&
                ReferenceEquals(NodeEditorWindow.Current.Graph, Target.FSMStateAction)) 
                return;

            if (!GUILayout.Button("Edit state", EditorStyles.toolbarButton)) 
                return;

            var w = NodeEditorWindow.Open(Target.FSMStateAction);
            w.ResetView(); // Focus selected node
        }

        void NameGUI()
        {
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Label("Name: ", GUILayout.Width(k_defaultLabelWidth));

                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    var name = GUILayout.TextField(Target.Name);
                    if (check.changed)
                        Target.SetName(name);
                }

                Target.Color = EditorGUILayout.ColorField(GUIContent.none, Target.Color, 
                    false, false,false, 
                    GUILayout.Width(k_smallElementWidth));
            }
        }

        void InfoGUI()
        {
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Label("Info: ", GUILayout.Width(k_defaultLabelWidth));
                Target.Description = GUILayout.TextArea(Target.Description);
            }
        }

        void EventsGUI(float width)
        {
            if (Target.Events.Count > 0)
            {
                GetWidths(width, out var nameWidth, out var conditionWidth);

                EventsGUITableHeader(nameWidth, conditionWidth);
                ExistingEventsGUI(nameWidth, conditionWidth);
            }

            AddEventGUI();
        }

        void ExistingEventsGUI(float nameWidth, float conditionWidth)
        {
            var deleteIdx = -1;
            for (var i = 0; i < Target.Events.Count; i++)
            {
                var @event = Target.Events[i];
                using (new GUILayout.HorizontalScope())
                {
                    GUILayout.Label(@event.Name, GUILayout.Width(nameWidth));
                    @event.Condition = (ScriptableBaseAction) EditorGUILayout.ObjectField(@event.Condition,
                        typeof(ScriptableBaseAction), false, GUILayout.Width(conditionWidth));
                    if (GUILayout.Button("x", GUILayout.Width(k_smallElementWidth)))
                        deleteIdx = i;
                }
            }

            if (deleteIdx == -1)
                return;
            Target.Events.RemoveAt(deleteIdx);
            OnChanged();
        }

        static void EventsGUITableHeader(float nameWidth, float conditionWidth)
        {
            GUILayout.Label("State Events: ", EditorStyles.miniBoldLabel);
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Label("Event-Name", EditorStyles.miniLabel, GUILayout.Width(nameWidth));
                GUILayout.Label("Condition", EditorStyles.miniLabel, GUILayout.Width(conditionWidth));
                GUILayout.Label("", EditorStyles.miniLabel, GUILayout.Width(k_smallElementWidth));
            }
        }

        void AddEventGUI()
        {
            GUILayout.Space(5f);

            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Label("Add Event: ", GUILayout.Width(k_defaultLabelWidth));

                var graph = (FSMGraph) Target.Graph;
                var newEvents = graph.EventNames.Where(@eventName =>
                        Target.Events.FindIndex(@stateEvent => string.Equals(@stateEvent.Name, @eventName)) == -1)
                    .ToArray();
                var newEvent = EditorGUILayout.Popup(-1, newEvents);
                if (newEvent == -1) 
                    return;

                Target.Events.Add(new Event() {Name = newEvents[newEvent]});
                OnChanged();
            }
        }

    }
}