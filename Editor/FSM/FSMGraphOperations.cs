﻿using System.Linq;
using nodegraph.Editor.Operations;
using ScriptableUtility.FSM;

namespace ScriptableUtility.Editor.FSM
{
    public static class FSMGraphOperations
    {
        public static void RemoveEvent(this FSMGraph fsmGraph, string eventName)
        {
            var globalEvent = fsmGraph.NodeObjs.OfType<GlobalEvent>().FirstOrDefault(e => string.Equals(e.Name, eventName));
            if (globalEvent != null)
                fsmGraph.RemoveNode(globalEvent);

            foreach (var state in fsmGraph.NodeObjs.OfType<State>())
                state.Events.RemoveAll(e => string.Equals(e.Name, eventName));
        }

    }
}
