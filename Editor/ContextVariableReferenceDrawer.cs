﻿using System;
using System.Collections.Generic;
using Core.Editor.Attribute;
using Core.Editor.Extensions;
using Core.Editor.Utility;
using Core.Extensions;
using Core.Interface;
using Core.Types;
using Core.Unity.Attributes;
using Core.Unity.Interface;
using Core.Unity.Types.Attribute;
using Core.Unity.Types.InterfaceContainerBase;
using Core.Unity.Utility.GUITools;
using JetBrains.Annotations;
using ScriptableUtility.Attributes;
using UnityEditor;
using UnityEngine;

using static Core.Utils;
using static Core.Unity.Types.Attribute.ContextDrawerAttribute;

using MessageType = UnityEditor.MessageType;
using Object = UnityEngine.Object;

namespace ScriptableUtility.Editor
{
    [CustomPropertyDrawer(typeof(IContextVariableReference), true)]
    public class ContextVariableFallbackDrawer : PropertyDrawer
    {
        ContextVariableReferenceDrawer.VarProperties m_current;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return ContextVariableReferenceDrawer.GetProperties(null, property, out m_current, out var err) ==
                   OperationResult.Error 
                ? EditorStyles.helpBox.CalcSize(new GUIContent(err)).y
                : ContextVariableReferenceDrawer.GetPropertyHeight(m_current);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
            => ContextVariableReferenceDrawer.DoGUI(null, ref position, property, label);
    }

    [UsedImplicitly]
    public class ContextVariableReferenceDrawer : MultiPropertyEditorDrawer
    {
        public override Type PreviewForAttribute => typeof(VarDrawerAttribute);

        struct ContextVariables
        {
            public List<ScriptableObject> Variables;
            public List<string> Names;
        }
        internal struct VarProperties
        {
            internal SerializedProperty MainProperty;
            internal SerializedProperty ContextType;
            internal SerializedProperty IntrinsicValue;
            internal SerializedProperty ScriptableValue;
            internal SerializedProperty ContextProvider;
            internal SerializedProperty ContextProviderObj;

            internal Type Type;

            internal bool FullHide;
            internal bool ContextNeeded;
            internal bool ShowContextField;
            internal bool IntrinsicOk => IntrinsicValue != null;

            internal ContextDrawerAttributeData Attr;
        }

        static GUIStyle m_contextTypeButtonStyle;
        static GUIContent m_scriptableButton;
        static GUIContent m_intrinsicButton;

        static IVariableContainer m_currentContainer;
        static readonly Dictionary<Type, ContextVariables> k_contextVariables = new Dictionary<Type, ContextVariables>();
        const float k_defaultLineHeight = 18f;
        const float k_defaultIndent = 20f;
        static bool m_showContext;

        #region Public Overrides
        public override float GetPropertyHeight(MultiPropertyAttribute attr, SerializedProperty property, GUIContent label, float height)
        {
            var h =  GetProperties(attr, property, out var varProperties, out var err) == OperationResult.Error 
                ? EditorStyles.helpBox.CalcSize(new GUIContent(err)).y
                : GetPropertyHeight(varProperties);
            return h;
        }
        public override void OnGUI(MultiPropertyAttribute attr, ref Rect position, SerializedProperty property,
            GUIContent label)
            => DoGUI(attr, ref position, property, label);
        #endregion

        #region Height
        internal static float GetPropertyHeight(VarProperties varProperties)
        {
            if (varProperties.FullHide)
                return 0f;

            // Property Height is default drawer for the intrinsic value
            var valueHeight = GetValueHeight(varProperties);
            if (varProperties.Attr.BreakAfterLabel)
                valueHeight += k_defaultLineHeight;

            var ctxType = GetContextType(varProperties);

            if (ctxType != ReferenceContextType.Scriptable) 
                return valueHeight;

            var ctxHeight = k_defaultLineHeight;
            if (!m_showContext || ContextConditionCheck(varProperties.Attr.ShowContext, varProperties.ContextProviderObj) ==
                FlowResult.Stop)
                ctxHeight = 0f;
            // Property Height is just one row for linking global variable + the context for context variable
            return varProperties.ShowContextField ? valueHeight + ctxHeight : valueHeight;
        }

        static float GetValueHeight(VarProperties varProperties)
        {
            switch (GetContextType(varProperties))
            {
                case ReferenceContextType.Intrinsic: // Property Height is default drawer for the intrinsic value
                    return EditorGUI.GetPropertyHeight(varProperties.IntrinsicValue);
                case ReferenceContextType.Scriptable: return k_defaultLineHeight;
                default: throw new ArgumentOutOfRangeException();
            }
        }
        #endregion

        #region Utility Methods
        static T GetTarget<T>(VarProperties varProperties) where T : class
        {
            var target = varProperties.MainProperty.serializedObject.targetObject;
            if (target is Component c)
            {
                return c.TryGetComponent<T>(out var tComp) 
                    ? tComp 
                    : default;
            }
            if (!(target is IScriptableObject childAsset))
                return target as T;

            var parent = childAsset.Parent();
            return parent as T;
        }

        static FlowResult ContextConditionCheck(Condition condition, SerializedProperty prop)
        {
            switch (condition)
            {
                case Condition.Never:
                case Condition.OnlyWhenNull when prop.objectReferenceValue != null:
                    return FlowResult.Stop;
            }

            return FlowResult.Continue;
        }

        static ReferenceContextType GetContextType(VarProperties varProperties) =>
            GetContextType(varProperties.ContextType);
        static ReferenceContextType GetContextType(SerializedProperty contextTypeProp) 
            => (ReferenceContextType) Mathf.Clamp(contextTypeProp.enumValueIndex, 0, 2);
        #endregion

        #region GUI
        internal static void DoGUI(MultiPropertyAttribute attr, ref Rect position, SerializedProperty property, GUIContent label)
        {
            InitStyles();

            var changes = false;

            if (GetProperties(attr, property, out var varProps, out var problems) == OperationResult.Error)
            {
                EditorGUI.HelpBox(position, problems, MessageType.Error);
                return;
            }

            changes |= (NonGUIOperations(varProps) == ChangeCheck.Changed);

            if (!varProps.FullHide)
            {
                using (var scope = new EditorGUI.PropertyScope(position, label, property))
                    changes |= OnGUI_PropertyScoped(ref position, scope, varProps) == ChangeCheck.Changed;
            }

            if (changes)
                varProps.MainProperty.serializedObject.ApplyModifiedProperties();
        }

        static ChangeCheck OnGUI_PropertyScoped(ref Rect position, EditorGUI.PropertyScope scope, VarProperties varProperties)
        {
            var brk = varProperties.Attr.BreakAfterLabel;

            var changes = false;       
            using (var vertical = new VerticalRectScope(ref position, position.height))
            {
                var height = brk ? k_defaultLineHeight : GetValueHeight(varProperties);
                using (var horizontal = new HorizontalRectScope(ref vertical.Rect, height))
                {
                    ContextFoldoutButtonGUI(varProperties, horizontal);
                    // Always Label and ConfigButton on same line
                    DrawLabel(scope, varProperties, horizontal);
                    changes |= (ConfigButtonGUI(horizontal, varProperties) == ChangeCheck.Changed);

                    // usually draw value in same line if it fits
                    var restRect = horizontal.Get(horizontal.Rect.width);
                    if (!brk && ValueGUI(restRect, varProperties) == ChangeCheck.Changed)
                        changes = true;
                }

                if (brk) // if value does not fit, next line
                    using (var horizontal = new HorizontalRectScope(ref vertical.Rect, GetValueHeight(varProperties)))
                    {
                        // some indent
                        horizontal.Get(k_defaultIndent);
                        var rest = horizontal.Get(horizontal.Rect.width);
                        changes |= (ValueGUI(rest, varProperties) == ChangeCheck.Changed);
                    }

                if (!varProperties.ShowContextField)
                    return changes ? ChangeCheck.Changed : ChangeCheck.NotChanged;

                using (var horizontal = new HorizontalRectScope(ref vertical.Rect, k_defaultLineHeight))
                {
                    // some indent
                    horizontal.Get(k_defaultIndent);

                    // draw context field
                    var restRect = horizontal.Get(horizontal.Rect.width);
                    changes |= SetContextGUI(restRect, varProperties) == ChangeCheck.Changed;
                }
            }

            return changes ? ChangeCheck.Changed : ChangeCheck.NotChanged;
        }

        static void ContextFoldoutButtonGUI(VarProperties varProperties, HorizontalRectScope horizontal)
        {
            if (!varProperties.ShowContextField) 
                return;
            var ctxMissing = varProperties.ContextProviderObj.objectReferenceValue == null;
            var ctxMissingWarningColor = (ctxMissing && varProperties.ContextNeeded) ? Color.red : Color.white;
            using (new ColorScope(ctxMissingWarningColor))
            {
                if (ContextConditionCheck(varProperties.Attr.ShowContext, varProperties.ContextProviderObj) !=
                    FlowResult.Stop)
                    m_showContext = (GUI.Toggle(horizontal.Get(20f), m_showContext, "", EditorStyles.foldout));
            }
        }

        static void DrawLabel(EditorGUI.PropertyScope scope, VarProperties varProperties,
            HorizontalRectScope horizontal)
        {
            if (varProperties.Attr.HideLabel)
                return;

            var label = scope.content;
            var size = EditorStyles.label.CalcSize(label);
            size.x = Mathf.Clamp(size.x + 5f, 100f, 200f);
            var labelRect = horizontal.Get(size.x);
            EditorGUI.LabelField(labelRect, label);
        }

        static ChangeCheck ValueGUI(Rect r, VarProperties varProperties)
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                if (GetContextType(varProperties) == ReferenceContextType.Intrinsic)
                    EditorGUI.PropertyField(r, varProperties.IntrinsicValue, GUIContent.none);
                else
                    ScriptableVariableGUI(r, varProperties);

                return check.changed ? ChangeCheck.Changed : ChangeCheck.NotChanged;
            }
        }

        static ChangeCheck ConfigButtonGUI(HorizontalRectScope layout, VarProperties varProperties)
        {
            if (!varProperties.IntrinsicOk)
                return ChangeCheck.NotChanged;
            // Calculate rect for configuration button
            var buttonRect = layout.Get(25 + m_contextTypeButtonStyle.margin.right);
            buttonRect.height = k_defaultLineHeight; // value height can be higher

            //using (new EditorGUI.DisabledGroupScope(!varProperties.IntrinsicOk))
            //{
            var contextType = GetContextType(varProperties);
            var prevContextType = contextType;

            var l = contextType == ReferenceContextType.Scriptable ? m_scriptableButton : m_intrinsicButton;
            if (!varProperties.FullHide)
                contextType = GUI.Toggle(buttonRect, contextType == ReferenceContextType.Intrinsic, l,
                    m_contextTypeButtonStyle)
                    ? ReferenceContextType.Intrinsic
                    : ReferenceContextType.Scriptable;

            if (contextType == prevContextType) 
                return ChangeCheck.NotChanged;

            varProperties.ContextType.enumValueIndex = (int) contextType;
            return ChangeCheck.Changed;
            //}
        }

        static void ScriptableVariableGUI(Rect r, VarProperties varProperties)
        {
            using (var h = new HorizontalRectScope(ref r, k_defaultLineHeight))
            {
                var usePopup = k_contextVariables.Count > 0;
                var popupSpace = usePopup ? 20f : 0f;
                var varRect = h.Get(h.Rect.width - popupSpace);

                EditorGUI.PropertyField(varRect, varProperties.ScriptableValue, GUIContent.none);

                if (usePopup)
                    ScriptablePopupGUI(h.Get(popupSpace), varProperties);
            }
        }

        static void ScriptablePopupGUI(Rect position, VarProperties varProperties)
        {
            if (!k_contextVariables.ContainsKey(varProperties.Type)) 
                return;

            var selIdx = -1;
            var names = k_contextVariables[varProperties.Type].Names;
            if (varProperties.ScriptableValue.objectReferenceValue != null)
                selIdx = names.IndexOf(varProperties.ScriptableValue.objectReferenceValue.name);

            if (!varProperties.FullHide)
                selIdx = EditorGUI.Popup(position, selIdx, names.ToArray());

            if (selIdx != -1)
                varProperties.ScriptableValue.objectReferenceValue =
                    k_contextVariables[varProperties.Type].Variables[selIdx];
        }

        static ChangeCheck SetContextGUI(Rect r, VarProperties varProperties)
        {
            // ReSharper disable once SwitchStatementMissingSomeEnumCasesNoDefault
            if (!m_showContext || ContextConditionCheck(varProperties.Attr.ShowContext, varProperties.ContextProviderObj) == FlowResult.Stop) 
                return ChangeCheck.NotChanged;

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                EditorGUI.PropertyField(r, varProperties.ContextProvider);
                return check.changed ? ChangeCheck.Changed : ChangeCheck.NotChanged;
            }
            //InterfaceContainerDrawer.OnGUI(position, varProperties.ContextProvider, GUIContent.none, 0);
        }
        #endregion

        #region Auto-Config & Helping User
        static ChangeCheck NonGUIOperations(VarProperties varProperties)
        {
            // below operations irrelevant for intrinsic
            if (GetContextType(varProperties) == ReferenceContextType.Intrinsic)
                return ChangeCheck.NotChanged;

            var changes = ContextSelf(varProperties) == ChangeCheck.Changed;
            UpdateContextVariablesForPopup(varProperties);
            changes |= AutoSelectScriptableVarByName(varProperties) == ChangeCheck.Changed;

            return changes ? ChangeCheck.Changed : ChangeCheck.NotChanged;
        }

        static ChangeCheck ContextSelf(VarProperties varProperties)
        {
            if (!varProperties.ContextNeeded)
                return ChangeCheck.NotChanged;

            if (ContextConditionCheck(varProperties.Attr.ContextSelf, varProperties.ContextProviderObj) == FlowResult.Stop)
                return ChangeCheck.NotChanged;

            var ctxProvObjProp = varProperties.ContextProviderObj;
            //if (ctxProvObjProp == null) return ChangeCheck.NotChanged;
            var prev = ctxProvObjProp.objectReferenceValue as IProvider<IContext>;
            var target = GetTarget<IProvider<IContext>>(varProperties);

            if (prev == target || target == null) 
                return ChangeCheck.NotChanged;

            varProperties.ContextProviderObj.objectReferenceValue = (Object) target;
            return ChangeCheck.Changed;
        }

        static ChangeCheck AutoSelectScriptableVarByName(VarProperties varProperties)
        {
            var name = varProperties.Attr.AutoSelectName;
            if (string.IsNullOrEmpty(name))
                return ChangeCheck.NotChanged;

            if (!k_contextVariables.ContainsKey(varProperties.Type))
                return ChangeCheck.NotChanged;

            // todo 1: auto-select only when null setting?
            var idx = k_contextVariables[varProperties.Type].Names.IndexOf(name);
            if (idx == -1)
                return ChangeCheck.NotChanged;

            if (varProperties.ScriptableValue.objectReferenceValue == k_contextVariables[varProperties.Type].Variables[idx])
                return ChangeCheck.NotChanged;

            varProperties.ScriptableValue.objectReferenceValue = k_contextVariables[varProperties.Type].Variables[idx];
            return ChangeCheck.Changed;
        }

        static void UpdateContextVariablesForPopup(VarProperties varProperties)
        {
            if (varProperties.ContextProviderObj == null)
                return;

            var varContainer = varProperties.ContextProviderObj.objectReferenceValue as IVariableContainer ??
                               GetTarget<IVariableContainer>(varProperties);

            if (varContainer == null)
            {
                //Debug.Log($"varContainer == null current target {varProperties.MainProperty.serializedObject.targetObject} ");
                return;
            }
            if (varContainer != m_currentContainer)
                UpdateContextVariablesForPopup(varContainer);
        }

        static void UpdateContextVariablesForPopup(IVariableContainer varContainer)
        {
            m_currentContainer = varContainer;
            k_contextVariables.Clear();

            var vars = m_currentContainer?.VariableObjs;
            if (vars == null)
                return;

            foreach (var v in vars)
            {
                if (!(v is IScriptableVariable conVar))
                    continue;

                if (!k_contextVariables.ContainsKey(conVar.VariableType))
                {
                    k_contextVariables.Add(conVar.VariableType, 
                        new ContextVariables()
                        {
                            Names = new List<string>(),
                            Variables = new List<ScriptableObject>()
                        });
                }
                k_contextVariables[conVar.VariableType].Variables.Add(v);
                k_contextVariables[conVar.VariableType].Names.Add(v.name);
            }
        }
        #endregion

        #region Init
        internal static OperationResult GetProperties(MultiPropertyAttribute attr, SerializedProperty property, 
            out VarProperties varProperties, out string problems)
        {
            varProperties = default;
            problems = "";

            GetAttributeValues(attr, out var data);

            var varRef = property.GetValue<IContextVariableReference>();

            var scriptableValue = property.FindPropertyRelative(VarReference<int>.Editor_ScriptablePropName);
            var contextTypeProp = property.FindPropertyRelative(VarReference<int>.Editor_ContextTypePropName);
            var intrinsicValueProp = property.FindPropertyRelative(VarReference<int>.Editor_IntrinsicPropName);
            var ctxProvProp = property.FindPropertyRelative(VarReference<int>.Editor_ContextProvPropName);
            var ctxProvObjField = ctxProvProp?.FindPropertyRelative(InterfaceContainerBase.Editor_ObjectFieldPropName);

            var problemWithIntrinsic = contextTypeProp == null || (intrinsicValueProp == null &&
                                                                   GetContextType(contextTypeProp) ==
                                                                   ReferenceContextType.Intrinsic);
            if (varRef == null)
            {
                problems = $"varRef == null {property.propertyType} {property.type} ";
                return OperationResult.Error;
            }
            if (problemWithIntrinsic || NULL.IsAny(contextTypeProp, ctxProvProp, ctxProvObjField))
            {
                problems = ($"Not all properties have been found! On {property.propertyPath} \n" +
                            $"FOUND: scriptableValue {scriptableValue != null} contextTypeProp {contextTypeProp != null} \n" +
                            $"intrinsicValueProp {intrinsicValueProp != null} ctxProvProp {ctxProvProp != null} ctxProvObjField {ctxProvObjField != null}");
                return OperationResult.Error;
            }
            // for intellisense/ resharper
            Debug.Assert(ctxProvObjField != null);

            var contextType = GetContextType(contextTypeProp);

            var isIntrinsic = contextType == ReferenceContextType.Intrinsic;
            var scriptableNeeded = !isIntrinsic;
            
            var scriptable = scriptableValue.objectReferenceValue as IScriptableVariable;
            var scriptableIsSet = scriptable != null;

            //var isGlobal = scriptableIsSet && scriptable.ContextType == ScriptableContextType.GlobalContext;
            var isObjCtx = scriptableIsSet && scriptable.ContextType == ScriptableContextType.ObjectContext;

            var contextIsSet = ctxProvObjField.objectReferenceValue != null;
            var contextNeeded = isObjCtx && !isIntrinsic;
            var showContextField = contextNeeded || (scriptableNeeded && !scriptableIsSet);
            var fullyLinked = (!contextNeeded || contextIsSet) && (!scriptableNeeded || scriptableIsSet);
            var fullHide = data.ShowOnlyIfUnlinked && fullyLinked && !isIntrinsic;

            varProperties = new VarProperties()
            {
                MainProperty = property,
                ContextType = contextTypeProp,
                IntrinsicValue = intrinsicValueProp,
                ScriptableValue = scriptableValue,
                ContextProvider = ctxProvProp,
                ContextProviderObj = ctxProvObjField,
                Type = varRef?.VariableType,
                ContextNeeded = contextNeeded,
                ShowContextField = showContextField,
                FullHide = fullHide,
                Attr = data
            };

            return OperationResult.OK;
        }

        static void GetAttributeValues(MultiPropertyAttribute attr, 
            out ContextDrawerAttributeData data)
        {
            data = ContextDrawerAttributeData.Default;
            if (!(attr is VarDrawerAttribute vda))
                return;

            data = vda.Data;
        }

        static void InitStyles()
        {
            if (m_contextTypeButtonStyle != null) 
                return;
            m_contextTypeButtonStyle = // EditorStyles.toolbarButton;
                new GUIStyle(GUI.skin.GetStyle("Button"));
            //imagePosition = ImagePosition.ImageOnly

            m_scriptableButton = EditorGUIUtility.IconContent("ScriptableObject Icon");
            m_intrinsicButton = new GUIContent("->");
        }
        #endregion
    }
}