using Core.Editor.Attribute;
using Core.Editor.PopupWindows;
using Core.Editor.Tools;
using Core.Editor.Utility;
using Core.Interface;
using Core.Types;
using Core.Unity.Extensions;
using Core.Unity.Utility.GUITools;
using ScriptableUtility.ActionConfigs;
using UnityEditor;
using UnityEngine;

using static UnityEditor.Editor;

namespace ScriptableUtility.Editor
{
    public static class CommonActionEditorGUI
    {
        //public static ScriptableBaseAction LastSelection;

        #region Data structs
        // for lists:
        public struct ActionListModifications
        {
            //public int AddIdx;
            public int SetIdx;
            public int DeleteIdx;
            public int CutPasteIdx;
            public int MoveIdxA;
            public int MoveIdxB;

            public static ActionListModifications Default => new ActionListModifications
                { // AddIdx = -1, 
                    SetIdx=-1, CutPasteIdx = -1, DeleteIdx = -1, MoveIdxA = -1, MoveIdxB = -1};
        }
        public struct ActionMenuData
        {
            public SelectTypesPopup Menu;
            public Rect ButtonRect;
            public static ActionMenuData Default => new ActionMenuData {};
        }

        public struct ActionListData
        {
            public SerializedObject SObject;
            public string PropertyPath;
            public SerializedProperty ActionListProperty;

            public Color ActionNotSetColor;
            public int TypeSelectionIdx;

            public UnityEditor.Editor[] CachedNestedEditor;

            public string LabelFormat;
            public GUIContent[] CustomLabels;

            public IGUIData GUIData;
        }

        public struct ActionListIterator
        {
            public int Idx;
            public int Count;
            public ScriptableBaseAction Action;
            public SerializedProperty ActionProperty;
        }
        
        // for single slots:
        public struct ActionData
        {
            public SerializedObject SObject;

            public string PropertyPath;
            public ScriptableBaseAction Action;
            public SerializedProperty ActionProperty;
            public UnityEditor.Editor CachedEditor;
            public bool FoldOut;
            public bool CutPasteEnabled;

            public Color ActionNotSetColor;
            public GUIContent Label;
        }
        #endregion

        #region Utility
        public static Color ActionNotSetColorDefault => new Color(1f, 0.75f, 0f);
        public static void ColorGUI(IGUIData target)
        {                
            target.GUIColor = EditorGUILayout.ColorField(GUIContent.none, target.GUIColor,
                false, false,false, GUILayout.Width(6f));
            target.UseColor = EditorGUILayout.Toggle(target.UseColor, GUILayout.Width(15f));

            //if (!target.GUIData.UseColor)
            //    target.GUIData.GUIColor = Color.white;
        }

        public static void SafeDestroy(Object mainAsset, Object asset)
        {
            var mainAssetPath = AssetDatabase.GetAssetPath(mainAsset);
            var path = AssetDatabase.GetAssetPath(asset);
            var partOfAsset = string.Equals(path, mainAssetPath);

            // when not part of main asset, nothing is destroyed, only reference is removed
            if (!partOfAsset)
                return;

            var mainHierarchy = SubAssetHierarchy.GetSubAssetHierarchy(mainAsset);
            // when not part of linear hierarchy, nothing is destroyed, other asset might still reference the asset in question
            if (SubAssetHierarchy.TryFindSubAssetInHierarchy(asset, mainHierarchy, out var subAssetHierarchy) ==
                FindResult.NotFound)
                return;

            SafeDestroy(subAssetHierarchy);
        }

        static void SafeDestroy(SubAssetHierarchy.SubAssetData data)
        {
            foreach (var subHierarchy in data.SubAssets) 
                SafeDestroy(subHierarchy);

            if (data.Asset != null)
                data.Asset.DestroyEx();
        }

        #endregion

        #region GUI
        public static ChangeCheck DefaultActionListElementHeader(
            ref ActionListData listData,
            ref ActionListIterator it, 
            ref ActionListModifications mod,
            ref ActionMenuData menuData)
        {
            var changed = false;
            using (new EditorGUILayout.HorizontalScope())
            {
                if (GUILayout.Button("x", GUILayout.Width(25)))
                    mod.DeleteIdx = it.Idx;

                GUILayout.Space(10);

                var prevColor = GUI.color;
                var prevBGColor = GUI.backgroundColor;

                if (it.Action != null)
                {
                    if (it.Action.UseColor)
                        GUI.backgroundColor = it.Action.GUIColor;

                    it.Action.Enabled = EditorGUILayout.Toggle(it.Action.Enabled,GUILayout.Width(25f));

                    var prevFoldOut = it.ActionProperty.isExpanded;
                    it.ActionProperty.isExpanded = EditorGUILayout.BeginFoldoutHeaderGroup(it.ActionProperty.isExpanded, it.Action.Name);
                    it.ActionProperty.isExpanded = true;
                    if (prevFoldOut != it.ActionProperty.isExpanded)
                    {
                        if (it.ActionProperty.serializedObject.targetObject is INamed n)
                            Debug.Log($"{n.Name} | {it.ActionProperty.propertyPath} | {it.Action.Name}\n" +
                                      $"prev: {prevFoldOut} {it.ActionProperty.isExpanded}");
                        it.ActionProperty.serializedObject.ApplyModifiedProperties();
                        changed = true;
                    }                        //    LastSelection = it.Action;

                    ColorGUI(it.Action);
                }
                else
                {
                    GUI.color = listData.ActionNotSetColor;
                    var path = it.ActionProperty.propertyPath.Split('.');
                    EditorGUILayout.LabelField(path[path.Length-1], GUILayout.Width(50f));
                    GUILayout.FlexibleSpace();
                }

                GUILayout.Space(10);

                var nested = IsNested(listData.SObject, it.Action);
                using (new EditorGUILayout.HorizontalScope(EditorStyles.helpBox, GUILayout.Width(nested ? 235f:385f)))
                {
                    ActionReferenceGUI(listData.SObject, it.Action, it.ActionProperty, nested, GUIContent.none);

                    SetBehaviourGUI(it.Action, ref listData, ref menuData, out var setBehaviour, it.Idx);
                    MoveElementGUI(it, ref mod);

                    CutPasteGUI(it.Action, out var cutPaste);
                    if (cutPaste)
                        mod.CutPasteIdx = it.Idx;
                    if (setBehaviour)
                        mod.SetIdx = it.Idx;
                }

                GUI.backgroundColor = prevBGColor;
                GUI.color = prevColor;
                EditorGUILayout.EndFoldoutHeaderGroup();
            }

            return changed ? ChangeCheck.Changed : ChangeCheck.NotChanged;
        }
        
        public static void ActionGUI(ref ActionData actionData, ref ActionMenuData menuData, 
            out bool removeBehaviour, out bool setBehaviour, out bool cutPaste)
        {
            removeBehaviour = false;
            setBehaviour = false;

            if (actionData.Action == null)
                ActionGUINoAction(ref actionData, ref menuData, out setBehaviour, out cutPaste);
            else
                ActionGUIWithAction(ref actionData, out removeBehaviour, out cutPaste);
        }
        #endregion

        #region Private GUI Methods
        static void MoveElementGUI(ActionListIterator it, ref ActionListModifications mod)
        {
            using (new EditorGUI.DisabledGroupScope(it.Idx == 0))
                if (GUILayout.Button("^", GUILayout.Width(25)))
                {
                    mod.MoveIdxA = it.Idx - 1;
                    mod.MoveIdxB = it.Idx;
                }

            using (new EditorGUI.DisabledGroupScope(it.Idx == it.Count - 1))
                if (GUILayout.Button("v", GUILayout.Width(25)))
                {
                    mod.MoveIdxA = it.Idx;
                    mod.MoveIdxB = it.Idx + 1;
                }
        }

        static void ActionGUIWithAction(ref ActionData actionData, out bool removeBehaviour, out bool cutPaste)
        {
            cutPaste = false;
            CreateCachedEditor(actionData.Action, null, ref actionData.CachedEditor);
            using (new EditorGUILayout.HorizontalScope())
            {
                if (actionData.Label != null)
                    EditorGUILayout.LabelField(actionData.Label, GUILayout.Width(50f));
                GUILayout.Space(10);

                removeBehaviour = GUILayout.Button("x", GUILayout.Width(25));
                GUILayout.Space(10);

                //var prevFoldOut = actionData.FoldOut;
                actionData.FoldOut = EditorGUILayout.Foldout(actionData.FoldOut, actionData.Action.Name, true);
                //if (actionData.FoldOut!= prevFoldOut)
                //    LastSelection = actionData.Action;

                ActionReferenceGUI(actionData.SObject, actionData.Action, actionData.ActionProperty, actionData.Label);
                if (actionData.CutPasteEnabled)
                    CutPasteGUI(actionData.Action, out cutPaste);

                if (!actionData.FoldOut)
                    return;
            }

            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Space(20);

                using (new EditorGUILayout.VerticalScope())
                    actionData.CachedEditor.OnInspectorGUI();
            }
        }

        static void ActionReferenceGUI(SerializedObject serializedObject, ScriptableBaseAction action,
            SerializedProperty property, GUIContent content = null)
        {
            var nested = IsNested(serializedObject, action);
            ActionReferenceGUI(serializedObject, action, property, nested, content);
        }

        static void ActionReferenceGUI(SerializedObject serializedObject, ScriptableBaseAction action, SerializedProperty property, 
            bool nested, GUIContent label)
        {
            if (nested)
            {
                var extract = GUILayout.Button("Extract", GUILayout.Width(100f));
                if (extract)
                    AssetExtraction.Extract(serializedObject, action, property);
            }
            else
            {
                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    var width = 140f;
                    if (label != null && label != GUIContent.none)
                    {
                        const float labelWidth = 70f;
                        EditorGUILayout.LabelField(label, GUILayout.Width(labelWidth));
                        width -= labelWidth;
                    }
                    EditorGUILayout.PropertyField(property, GUIContent.none, GUILayout.Width(width));
                    if (check.changed)
                        serializedObject.ApplyModifiedProperties();
                }
                if (action != null && GUILayout.Button("Integrate", GUILayout.Width(70f)))
                    AssetExtraction.Integrate(serializedObject, action, property);
            }
        }

        static bool IsNested(SerializedObject serializedObject, Object action)
        {
            var mainAssetPath = AssetDatabase.GetAssetPath(serializedObject.targetObject);

            var nested = false;
            if (action != null)
                nested = AssetDatabase.GetAssetPath(action).Equals(mainAssetPath);
            return nested;
        }

        static void ActionGUINoAction(ref ActionData actionData, ref ActionMenuData menuData, 
            out bool setBehaviour, 
            out bool cutPaste)
        {
            cutPaste = false;
            using (new EditorGUILayout.HorizontalScope())
            {
                using (new ColorScope(actionData.Action == null ? actionData.ActionNotSetColor : Color.white))
                {
                    ActionReferenceGUI(actionData.SObject, actionData.Action, actionData.ActionProperty, actionData.Label);
                    SetBehaviourGUI(actionData.Action, ref menuData, out setBehaviour);
                    if (actionData.CutPasteEnabled)
                        CutPasteGUI(actionData.Action, out cutPaste);
                }
            }

            if (actionData.CachedEditor != null)
                actionData.CachedEditor.DestroyEx();
        }

        
        static void SetBehaviourGUI(Object action, 
            ref ActionMenuData menuData, 
            out bool setBehaviour)
        {
            setBehaviour = action == null && GUILayout.Button("Set Behaviour");
            if (Event.current.type == EventType.Repaint)
                menuData.ButtonRect = GUILayoutUtility.GetLastRect();
        }

        static void SetBehaviourGUI(Object action, 
            ref ActionListData listData,
            ref ActionMenuData menuData, 
            out bool setBehaviour, 
            int setTypeIdx =-1)
        {
            SetBehaviourGUI(action, ref menuData, out setBehaviour);
            if (setBehaviour) 
                listData.TypeSelectionIdx = setTypeIdx;
        }

        static void CutPasteGUI(Object action, out bool cutPaste)
        {
            var cutPasteText = action != null
                ? "Cut" : "Paste";
            using (new EditorGUI.DisabledGroupScope(action == null 
                                                    && !CopyPasteOperation.HasData))
                cutPaste = GUILayout.Button(cutPasteText, GUILayout.Width(60));
        }
        #endregion
    }
}