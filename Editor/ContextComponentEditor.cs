﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Core.Editor.Inspector;
using Core.Editor.PopupWindows;
using Core.Types;
using ScriptableUtility.ActionConfigs;
using UnityEditor;
using UnityEngine;

using static Core.Editor.Extensions.EditorExtensions;
using static Core.Editor.Settings.CoreSettingsAccess;
using Object = UnityEngine.Object;

namespace ScriptableUtility.Editor
{
    [CustomEditor(typeof(ContextComponent))]
    public class ContextComponentInspector : BaseInspector<ContextComponentEditor>{}

    public class ContextComponentEditor : BaseEditor<ContextComponent>
    {
        // ReSharper disable once InconsistentNaming
        ContextComponent m_lastTarget;

        IList<ScriptableObject> TargetVariables => Target.VariableObjs;

        struct VariableSerializedData
        {
            //public SerializedObject Obj;
            public SerializedProperty GlobalValueProp;
            public IOverrideValueConverter OverrideConverter;
        }
        readonly List<VariableSerializedData> m_variableSerializedData = new List<VariableSerializedData>();

        readonly Dictionary<Type, Type> m_converterTypes = new Dictionary<Type, Type>();

        Type m_filterType = typeof(ScriptableObject);

        //string[] m_variableNames;
        static GUIStyle m_toggleButtonStyleNormal;
        static GUIStyle m_toggleButtonStyleToggled;

        UnityEditor.Editor m_scriptableContainerEditor;

        SelectTypesPopup m_typeSelectionPopup;
        Rect m_typeSelectionButtonRect;
        bool m_isExpanded;

        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);
            InitTypes();
        }

        public override void OnGUI(float width)
        {
            // base.OnGUI(width);
            if (Target == null)
            {
                Terminate();
                return;
            }

            InitStyles();

            if (m_lastTarget != Target 
                || m_variableSerializedData.Count != TargetVariables.Count)
                InitTarget();

            var containerProp = SerializedObject.FindProperty(ContextComponent.Editor_ContainerPropName);
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                EditorGUILayout.PropertyField(containerProp);
                if (check.changed)
                {
                    Target.MVariables.Clear();
                    InitTarget();

                    EditorUtility.SetDirty(Target);
                }
            }

            GUILayout.Label("Variables: ");

            var deleteIdx = -1;
            for (var i = 0; i < TargetVariables.Count; i++)
            {
                var asset = TargetVariables[i];
                ScriptableVariableSelectionGUI(ref asset, i, ref deleteIdx);

                if (!(asset is IScriptableVariable var))
                    continue;

                TargetVariables[i] = asset;

                InitialValuesGUI(i, var);
                GUILayout.Space(5);
            }

            DeleteVariable(deleteIdx);

            if (Target.MContainer == null)
            {
                GUILayout.Label("Add Variable: ");
                AddVariableGUI();
            }

            ScriptableContainerEditor();
        }

        void ScriptableContainerEditor()
        {
            var container = Target.VarContainer as ScriptableContainer;
            UnityEditor.Editor.CreateCachedEditor(container, typeof(ScriptableContainerEditor), ref m_scriptableContainerEditor);
            if (m_scriptableContainerEditor == null)
                return;

            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                Debug.Assert(container != null);
                m_isExpanded = EditorGUILayout.Foldout(m_isExpanded, container.name, true, EditorStyles.foldoutHeader);
                if(!m_isExpanded)
                    return;
                
                GUILayout.Label("Container Inspector: ");

                m_scriptableContainerEditor.OnInspectorGUI();
            }
        }

        void AddVariableGUI()
        {
            ScriptableObject toAddObject = null;
            ScriptableVariableSelectionGUI(ref toAddObject);
            if (toAddObject == null)
                return;
            // todo 3: report problem to user!
            if (!(toAddObject is IScriptableVariable))
                return;
            if (TargetVariables.Contains(toAddObject))
                return;
            TargetVariables.Add(toAddObject);

            CacheVariableData(toAddObject);
        }
        
        void InitialValuesGUI(int i, IScriptableVariable var)
        {
            var asset = TargetVariables[i];
            var serializedData = m_variableSerializedData[i];

            var idx = Target.MOverrideVariables.IndexOf(asset);

            //using (new EditorGUI.IndentLevelScope())
            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Space(25);
                if (idx == -1)
                    GlobalValueGUI(var, serializedData);
                else
                    OverrideGUI(var, idx, serializedData);
            }
        }

        void GlobalValueGUI(IScriptableVariable var, VariableSerializedData serializedData)
        {
            if (var is IOverrideMapping && GUILayout.Button("->", GUILayout.Width(25)))
            {
                Target.MOverrideVariables.Add((ScriptableObject) var);
                Target.MOverrideValues.Add(new OverrideValue(){StringValue = ""});
            }

            if (serializedData.GlobalValueProp == null)
                return;
            using (new EditorGUI.DisabledGroupScope(true))
            //using (var check = new EditorGUI.ChangeCheckScope())
            {
                EditorGUILayout.PropertyField(serializedData.GlobalValueProp, new GUIContent(""), true);
                //if (check.changed) serializedData.Obj.ApplyModifiedProperties();
            }
        }

        void OverrideGUI(IScriptableVariable var, int idx, VariableSerializedData serializedData)
        {
            var deleteOverride = GUILayout.Button("X", GUILayout.Width(20));

            if (typeof(Object).IsAssignableFrom(var.VariableType))
                ObjectOverrideGUI(idx, var.VariableType);
            else if (var is IOverrideMapping)
            {
                if (serializedData.OverrideConverter != null)
                {
                    var overrideVal = Target.MOverrideValues[idx];
                    if (serializedData.OverrideConverter.GUI(ref overrideVal)== ChangeCheck.Changed)
                    {
                        Target.MOverrideValues[idx] = overrideVal;
                        //Debug.Log("Changed" + overrideVal.StringValue);
                        EditorUtility.SetDirty(Target);
                        SerializedObject.Update();
                    }
                }
                else deleteOverride = true;
            }

            if (!deleteOverride)
                return;

            Target.MOverrideVariables.RemoveAt(idx);
            Target.MOverrideValues.RemoveAt(idx);
        }

        void ObjectOverrideGUI(int idx, Type objectType)
        {
            var overrideValue = Target.MOverrideValues[idx];
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                overrideValue.ObjectValue = EditorGUILayout.ObjectField(overrideValue.ObjectValue, objectType, true);

                if (!check.changed)
                    return;
                Target.MOverrideValues[idx] = overrideValue;
                EditorUtility.SetDirty(Target);
                //serializedObject.Update();
            }
        }

        void ScriptableVariableSelectionGUI(ref ScriptableObject asset)
        {
            var _ = -1;
            ScriptableVariableSelectionGUI(ref asset, -1, ref _);
        }

        void ScriptableVariableSelectionGUI(ref ScriptableObject asset, int i, ref int deleteIdx)
        {
            using (new EditorGUILayout.HorizontalScope())
            using (new EditorGUI.DisabledGroupScope(Target.MContainer != null))
            {
                if (i != -1 && GUILayout.Button("X", GUILayout.Width(20)))
                    deleteIdx = i;

                var typeFilter = m_filterType;
                if (asset != null)
                    typeFilter = asset.GetType();

                asset = (ScriptableObject)EditorGUILayout.ObjectField(asset, typeFilter, false);

                var filterEnabled = (m_filterType != typeof(ScriptableObject));

                if (asset != null)
                    return;

                if (GUILayout.Button("t",
                    filterEnabled ? m_toggleButtonStyleToggled : m_toggleButtonStyleNormal,
                    GUILayout.Width(20)))
                {
                    if (filterEnabled)
                        m_filterType = typeof(ScriptableObject);
                    else
                        PopupWindow.Show(m_typeSelectionButtonRect, m_typeSelectionPopup);
                }
                if (Event.current.type == EventType.Repaint)
                    m_typeSelectionButtonRect = GUILayoutUtility.GetLastRect();
            }
        }

        void DeleteVariable(int deleteIdx)
        {
            if (deleteIdx == -1)
                return;
        
            var var = TargetVariables[deleteIdx];
            TargetVariables.RemoveAt(deleteIdx);
            m_variableSerializedData.RemoveAt(deleteIdx);

            var ovrIdx = Target.MOverrideVariables.IndexOf(var);
            if (ovrIdx == -1)
                return;

            Target.MOverrideVariables.RemoveAt(ovrIdx);
            Target.MOverrideValues.RemoveAt(ovrIdx);
        }

        #region Init
        void InitTypes()
        {
            var types = new List<Type>();
            ReflectionGetAllTypes(typeof(ScriptableBaseAction), ref types, out var names,
                out var namespaces);
            m_typeSelectionPopup = CreateTypeSelectorPopup(types, names, namespaces, 
                MenuFilter, OnVariableTypeSelected);

            ReflectionGetAllTypes(typeof(IOverrideValueConverter), ref types, out names,
                out namespaces);

            foreach (var c in types)
            {
                //Debug.Log($"InitTypes {c}");
                var prop = c.GetProperty(nameof(IOverrideValueConverter.ValueType), 
                    BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);
                if (prop == null)
                    continue;
                var type = prop.GetMethod.Invoke(null, null);
                //Debug.Log($"{c} : {type}");
                m_converterTypes.Add((Type) type, c);
            }
        }

        void OnVariableTypeSelected(object varType)
        {
            var t = (Type) varType;
            m_filterType = t;
        }

        void InitTarget()
        {
            m_lastTarget = Target;
            m_variableSerializedData.Clear();

            foreach (var v in TargetVariables) 
                CacheVariableData(v);
        }

        static void InitStyles()
        {
            if (m_toggleButtonStyleNormal != null)
                return;

            m_toggleButtonStyleNormal = "Button";
            m_toggleButtonStyleToggled = new GUIStyle(m_toggleButtonStyleNormal);
            m_toggleButtonStyleToggled.normal.background = m_toggleButtonStyleToggled.active.background;
        }

        void CacheVariableData(Object toAddObject)
        {
            if (!(toAddObject is IScriptableVariable var))
                return;
            var serialized = new SerializedObject(toAddObject);
            var globalProp = serialized.FindProperty(ScriptableVariable<int>.Editor_ValuePropName);

            IOverrideValueConverter converter = null;
            if (m_converterTypes.TryGetValue(var.VariableType, out var converterType))
                converter = (IOverrideValueConverter) ScriptableObject.CreateInstance(converterType);
            //Debug.Log(var.VariableType+ " " + converter);
            m_variableSerializedData.Add(new VariableSerializedData()
            {
                //Obj = serialized,
                GlobalValueProp = globalProp,
                OverrideConverter = converter
            });
        }

        #endregion
    }
}
