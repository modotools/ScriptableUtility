using System;
using System.Collections.Generic;
using Core.Editor.Inspector;
using Core.Editor.PopupWindows;
using Core.Unity.Extensions;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.ActionConfigs.Logic;
using UnityEditor;
using UnityEngine;
using static Core.Editor.Extensions.EditorExtensions;
using static Core.Editor.Settings.CoreSettingsAccess;

namespace ScriptableUtility.Editor.Actions.Logic
{
    [CustomEditor(typeof(RepeatUntil))]
    public class RepeatUntilInspector : BaseInspector<RepeatUntilEditor>{}
    public class RepeatUntilEditor : BaseEditor<RepeatUntil>
    {
        UnityEditor.Editor m_cachedNestedEditor;
        List<Type> m_behaviourTypes = new List<Type>();
        SelectTypesPopup m_typeSelectionPopup;
        Rect m_typeSelectionButtonRect;

        Vector2 m_scrollPos;
        bool m_foldOut;

        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);

            ReflectionGetAllTypes(typeof(ScriptableBaseAction), ref m_behaviourTypes, out var names,
                out var namespaces);
            m_typeSelectionPopup = CreateTypeSelectorPopup(m_behaviourTypes, names, namespaces, MenuFilter, TypeSelected);
        }

        public override void Terminate()
        {
            base.Terminate();

            if (m_cachedNestedEditor != null)
                m_cachedNestedEditor.DestroyEx();
        }

        public override void OnGUI(float width)
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var prop = SerializedObject.FindProperty(RepeatUntil.Editor_NameProperty);
                EditorGUILayout.PropertyField(prop);
                prop = SerializedObject.FindProperty(RepeatUntil.Editor_BreakConditionProperty);
                EditorGUILayout.PropertyField(prop);
                prop = SerializedObject.FindProperty(RepeatUntil.Editor_BreakValueProperty);
                EditorGUILayout.PropertyField(prop);
                prop = SerializedObject.FindProperty(RepeatUntil.Editor_AbortOnBreakProperty);
                EditorGUILayout.PropertyField(prop);

                if (check.changed)
                {
                    SerializedObject.ApplyModifiedProperties();
                    OnChanged();
                }
            }

            using (var scroll = new EditorGUILayout.ScrollViewScope(m_scrollPos))
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                m_scrollPos = scroll.scrollPosition;
                ActionGUI();

                if (check.changed)
                    OnChanged();
            }

            VerificationGUI();
        }

        void ActionGUI()
        {
            var removeBehaviour = false;

            if (Target.Editor_ActionConfig == null)
            {
                if (GUILayout.Button("Set Behaviour"))
                    PopupWindow.Show(m_typeSelectionButtonRect, m_typeSelectionPopup);
                if (Event.current.type == EventType.Repaint)
                    m_typeSelectionButtonRect = GUILayoutUtility.GetLastRect();
            }
            else using (new EditorGUILayout.HorizontalScope())
            {
                removeBehaviour = GUILayout.Button("x", GUILayout.Width(25));
                GUILayout.Space(10);
                m_foldOut = EditorGUILayout.Foldout(m_foldOut, Target.Editor_ActionConfig.Name, true);
            }

            if (m_foldOut)
                NestedEditorGUI();
            if (removeBehaviour)
                OnRemoveBehaviour();
        }

        void NestedEditorGUI()
        {
            if (Target.Editor_ActionConfig == null && m_cachedNestedEditor != null)
                m_cachedNestedEditor.DestroyEx();

            UnityEditor.Editor.CreateCachedEditor(Target.Editor_ActionConfig, null, ref m_cachedNestedEditor);
            if (m_cachedNestedEditor == null)
                return;

            using (new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Space(20);
                using (new EditorGUILayout.VerticalScope())
                    m_cachedNestedEditor.OnInspectorGUI();
            }
        }

        void OnRemoveBehaviour()
        {
            Target.Editor_ActionConfig.DestroyEx();
            Target.Editor_ActionConfig = null;
            AssetDatabase.SaveAssets();
            OnChanged();
        }

        void TypeSelected(object data)
        {
            var type = (Type)data;
            var obj = ScriptableObject.CreateInstance(type);
            obj.hideFlags = HideFlags.HideInHierarchy;

            AssetDatabase.AddObjectToAsset(obj, Target);
            var platformBehaviourBase = (ScriptableBaseAction)obj;

            var prevActionConfig = Target.Editor_ActionConfig;
            if (prevActionConfig != null)
                prevActionConfig.DestroyEx();
            Target.Editor_ActionConfig = platformBehaviourBase;

            AssetDatabase.SaveAssets();
            OnChanged();
        }
    }
}
