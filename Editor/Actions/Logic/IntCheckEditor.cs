﻿using Core.Editor.Inspector;
using ScriptableUtility.ActionConfigs.Logic;
using UnityEditor;
using UnityEngine;

namespace ScriptableUtility.Editor.Actions.Logic
{
    [CustomEditor(typeof(IntCheck))]
    public class IntCheckInspector : BaseInspector<IntCheckEditor>{}
    public class IntCheckEditor : ScriptableBaseActionEditor<IntCheck>
    {
        public override string Name => "Int Check";

        CommonActionEditorGUI.ActionMenuData m_menuData = CommonActionEditorGUI.ActionMenuData.Default;
        Vector2 m_scrollPos;
        
        CommonActionEditorGUI.ActionListData m_actionsList;

        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);

            InitActionListDataDefault(out m_actionsList, nameof(IntCheck.Actions), "{0}:");
            CreateMenu(ref m_menuData, (data) => OnTypeSelected(m_actionsList, data));
        }

        public override void OnGUI(float width)
        {
            NameGUI(IntCheck.Editor_NameProperty);

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var intProp = SerializedObject.FindProperty(nameof(Target.IntRef));

                var finishRule = SerializedObject.FindProperty(nameof(Target.FinishRule));
                var waitForIntChange = SerializedObject.FindProperty(nameof(Target.WaitForIntChange));
                var dontUpdateIntCheck = SerializedObject.FindProperty(nameof(Target.DontUpdateIntCheck));

                var breakVal = SerializedObject.FindProperty(nameof(Target.BreakValue));

                EditorGUILayout.PropertyField(intProp);
                EditorGUILayout.PropertyField(finishRule);
                EditorGUILayout.PropertyField(breakVal);
                EditorGUILayout.PropertyField(waitForIntChange);
                EditorGUILayout.PropertyField(dontUpdateIntCheck);

                if (check.changed)
                {
                    SerializedObject.ApplyModifiedProperties();
                    OnChanged();
                }
            }

            using (var scroll = new EditorGUILayout.ScrollViewScope(m_scrollPos))
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                m_scrollPos = scroll.scrollPosition;

                UpdateActionListData(ref m_actionsList);
                ListGUI(ref m_actionsList, ref m_menuData);

                if (check.changed)
                    OnChanged();
            }
        }
    }
}
