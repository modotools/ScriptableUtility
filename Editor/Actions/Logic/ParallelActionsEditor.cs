﻿using Core.Editor.Inspector;
using ScriptableUtility.ActionConfigs.Logic;
using UnityEditor;
using UnityEngine;

namespace ScriptableUtility.Editor.Actions.Logic
{
    [CustomEditor(typeof(ParallelActions))]
    public class ParallelActionsInspector : BaseInspector<ParallelActionsEditor>{}
    public class ParallelActionsEditor : ScriptableBaseActionEditor<ParallelActions>
    {
        public override string Name => "Parallel Actions";

        CommonActionEditorGUI.ActionMenuData m_menuData = CommonActionEditorGUI.ActionMenuData.Default;
        Vector2 m_scrollPos;

        CommonActionEditorGUI.ActionListData m_actionsList;

        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);
            InitActionListDataDefault(out m_actionsList, ParallelActions.Editor_ActionProperty, "•");
            CreateMenu(ref m_menuData, (data) => OnTypeSelected(m_actionsList, data));
        }

        public override void Terminate()
        {
            base.Terminate();
            ReleaseEditor(ref m_actionsList);
        }

        public override void OnGUI(float width)
        {
            NameGUI(ParallelActions.Editor_NameProperty);

            using (var scroll = new EditorGUILayout.ScrollViewScope(m_scrollPos))
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                m_scrollPos = scroll.scrollPosition;

                UpdateActionListData(ref m_actionsList);
                ListGUI(ref m_actionsList, ref m_menuData);

                if (check.changed)
                    OnChanged();
            }
        }
    }
}
