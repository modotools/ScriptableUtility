﻿using ScriptableUtility.ActionConfigs;
using nodegraph.Editor.Attributes;
using nodegraph.Editor.Graph;

namespace ScriptableUtility.Editor.Actions
{
    [CustomNodeEditor(typeof(ScriptableBaseAction))]
    public class ScriptableBaseActionNodeEditor : NodeEditor
    {
    }
}
