﻿using System;
using System.Collections.Generic;
using Core.Editor.Attribute;
using Core.Editor.Extensions;
using Core.Editor.Inspector;
using Core.Editor.Utility;
using Core.Extensions;
using Core.Unity.Extensions;
using Core.Unity.Utility.GUITools;
using ScriptableUtility.ActionConfigs;
using UnityEditor;
using UnityEngine;

using static Core.Editor.Settings.CoreSettingsAccess;

namespace ScriptableUtility.Editor.Actions
{
    public abstract class ScriptableBaseActionEditor<T> : BaseEditor<T> where T : ScriptableObject
    {
        #region Init Release
        public static void CreateMenu(ref CommonActionEditorGUI.ActionMenuData menuData, GenericMenu.MenuFunction2 menu)
        {
            var platformBehaviourTypes = new List<Type>();
            EditorExtensions.ReflectionGetAllTypes(typeof(ScriptableBaseAction), ref platformBehaviourTypes, out var names,
                out var namespaces);
            menuData.Menu =
                EditorExtensions.CreateTypeSelectorPopup(platformBehaviourTypes, names, namespaces, MenuFilter, menu);
        }

        public static void InitActionDataDefault(out CommonActionEditorGUI.ActionData actionData, string label,
            string propertyPath)
        {
            actionData = new CommonActionEditorGUI.ActionData()
            {
                ActionNotSetColor = CommonActionEditorGUI.ActionNotSetColorDefault,
                CutPasteEnabled = true,
                Label = new GUIContent(label),
                PropertyPath = propertyPath
            };
        }
        protected void InitActionListDataDefault(out CommonActionEditorGUI.ActionListData actionData, string propertyPath, string labelFormat)
        {
            actionData = new CommonActionEditorGUI.ActionListData()
            {
                ActionNotSetColor = CommonActionEditorGUI.ActionNotSetColorDefault,
                PropertyPath = propertyPath,
                TypeSelectionIdx = -1,
                CachedNestedEditor = new UnityEditor.Editor[SerializedObject.FindProperty(propertyPath).arraySize],
                LabelFormat = labelFormat
            };
        }

        protected static void ReleaseEditor(ref CommonActionEditorGUI.ActionData actionData)
        {
            if (actionData.CachedEditor != null)
                actionData.CachedEditor.DestroyEx();
            actionData.CachedEditor = null;
        }
        protected static void ReleaseEditor(ref CommonActionEditorGUI.ActionListData actionListData)
        {
            foreach (var nestedEd in actionListData.CachedNestedEditor)
                nestedEd.DestroyEx();
        }
        #endregion

        #region GUI
        protected void NameGUI(string namePropPath)
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                var prop = SerializedObject.FindProperty(namePropPath);
                EditorGUILayout.PropertyField(prop);

                if (!check.changed) 
                    return;
                SerializedObject.ApplyModifiedProperties();
                OnChanged();
            }
        }
        protected void DefaultActionGUI(ref CommonActionEditorGUI.ActionData actionData, ref CommonActionEditorGUI.ActionMenuData menuData, Action contextAction = null)
        {
            UpdateActionData(ref actionData);
            CommonActionEditorGUI.ActionGUI(ref actionData, ref menuData, out var remove, out var set, out var cutPaste);

            var anyAction = remove || cutPaste || set;
            if (anyAction)
                contextAction?.Invoke();

            if (remove)
                OnRemoveBehaviour(ref actionData);
            if (cutPaste)
                OnCutPaste(ref actionData);
            if (set)
                PopupWindow.Show(menuData.ButtonRect, menuData.Menu);
        }

        static void NestedEditorGUI(ref CommonActionEditorGUI.ActionListData listData, int i, SerializedProperty seqElProp, ScriptableBaseAction seq)
        {
            if (seq == null && listData.CachedNestedEditor[i] != null)
                listData.CachedNestedEditor[i].DestroyEx();

            if (!seqElProp.isExpanded || seq == null)
                return;

            UnityEditor.Editor.CreateCachedEditor(seq, null, ref listData.CachedNestedEditor[i]);
            if (listData.CachedNestedEditor[i] == null)
                return;
            using (new EditorGUI.DisabledGroupScope(!seq.Enabled))
                listData.CachedNestedEditor[i].OnInspectorGUI();
        }
        #endregion

        #region Utility
        protected void UpdateActionData(ref CommonActionEditorGUI.ActionData actionData)
        {
            actionData.SObject = SerializedObject;
            actionData.ActionProperty = SerializedObject.FindProperty(actionData.PropertyPath);
            actionData.Action =  actionData.ActionProperty.objectReferenceValue as ScriptableBaseAction;
        }

        protected void UpdateActionListData(ref CommonActionEditorGUI.ActionListData actionData)
        {
            actionData.SObject = SerializedObject;
            actionData.ActionListProperty = SerializedObject.FindProperty(actionData.PropertyPath);
        }
        #endregion

        #region Actions
        protected void OnCutPaste(ref CommonActionEditorGUI.ActionListData listData, int cutIdx)
        {
            var prop = listData.ActionListProperty.GetArrayElementAtIndex(cutIdx);
            if (prop.objectReferenceValue != null)
                CopyPasteOperation.Cut(prop);
            else
                CopyPasteOperation.Paste(prop);
        }

        protected void OnDelete(ref CommonActionEditorGUI.ActionListData listData, int deleteIdx)
        {
            CommonActionEditorGUI.SafeDestroy(Target, listData.ActionListProperty.GetArrayElementAtIndex(deleteIdx).objectReferenceValue);

            listData.ActionListProperty.DeleteArrayElementAtIndex(deleteIdx);
            listData.SObject.ApplyModifiedProperties();
        }

        protected static void OnMove(ref CommonActionEditorGUI.ActionListData listData, int moveIdxA, int moveIdxB)
        {
            var arrAProp = listData.ActionListProperty.GetArrayElementAtIndex(moveIdxA);
            var arrBProp = listData.ActionListProperty.GetArrayElementAtIndex(moveIdxB);

            var seqA = arrAProp.objectReferenceValue;
            var seqB = arrBProp.objectReferenceValue;

            arrAProp.objectReferenceValue = seqB;
            arrBProp.objectReferenceValue = seqA;

            listData.SObject.ApplyModifiedProperties();
        }

        protected static void OnAdd(ref CommonActionEditorGUI.ActionListData listData)
        {
            var count = listData.ActionListProperty.arraySize;
            listData.ActionListProperty.InsertArrayElementAtIndex(count);
            listData.ActionListProperty.GetArrayElementAtIndex(count).objectReferenceValue = null;
            listData.SObject.ApplyModifiedProperties();
        }

        protected void OnRemoveBehaviour(ref CommonActionEditorGUI.ActionData actionData)
        {
            actionData.ActionProperty.objectReferenceValue.DestroyEx();
            actionData.ActionProperty.objectReferenceValue = null;
            actionData.SObject.ApplyModifiedProperties();

            AssetDatabase.SaveAssets();
            OnChanged();
        }

        protected static void OnCutPaste(ref CommonActionEditorGUI.ActionData actionData)
        {
            var prop = actionData.ActionProperty;
            var isPaste = prop.objectReferenceValue == null;
            
            if (isPaste)
                CopyPasteOperation.Paste(prop);
            else
                CopyPasteOperation.Cut(prop);
        }

        protected void OnTypeSelected(CommonActionEditorGUI.ActionData actionData, object typeData)
        {
            var action = CreateScriptableAction(typeData);

            if (actionData.Action != null)
                actionData.Action.DestroyEx();
            actionData.ActionProperty.objectReferenceValue = action;
            actionData.SObject.ApplyModifiedProperties();

            AssetDatabase.SaveAssets();
            OnChanged();
        }

        protected void OnTypeSelected(CommonActionEditorGUI.ActionListData it, object data)
        {
            var action = CreateScriptableAction(data);

            var elementProp = it.ActionListProperty.GetArrayElementAtIndex(it.TypeSelectionIdx);
            if (elementProp.objectReferenceValue != null)
                elementProp.objectReferenceValue.DestroyEx();
            elementProp.objectReferenceValue = action;
            it.SObject.ApplyModifiedProperties();

            AssetDatabase.SaveAssets();
            OnChanged();
        }

        protected ScriptableBaseAction CreateScriptableAction(object data)
        {
            var type = (Type) data;
            var obj = ScriptableObject.CreateInstance(type);
            obj.hideFlags = HideFlags.HideInHierarchy;

            AssetDatabase.AddObjectToAsset(obj, Target);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(obj));

            return (ScriptableBaseAction) obj;
        }
        #endregion

        #region GUI
        protected void ListGUI(ref CommonActionEditorGUI.ActionListData listData, ref CommonActionEditorGUI.ActionMenuData menuData,
            Action contextAction = null)
        {
            var mod = CommonActionEditorGUI.ActionListModifications.Default;
            var data = listData.GUIData ?? Target as IGUIData;
  
            if (data?.UseColor == true)
            {
                EditorGUILayout.BeginHorizontal();
                CustomGUI.VSplitter(data.GUIColor, 2f);
            }
            using (new EditorGUILayout.VerticalScope())
            {
                if (listData.CachedNestedEditor.Length < listData.ActionListProperty.arraySize)
                    Array.Resize(ref listData.CachedNestedEditor, listData.ActionListProperty.arraySize);

                for (var i = 0; i < listData.ActionListProperty.arraySize; i++)
                    SequenceElementGUI(ref listData, i, ref mod, ref menuData);
            }
            if (data?.UseColor == true)
                EditorGUILayout.EndHorizontal();

            GUILayout.Space(5);
            using (new EditorGUILayout.HorizontalScope())
            {
                if (data != null)
                    CommonActionEditorGUI.ColorGUI(data);
                GUILayout.Space(15f);

                GUILayout.Label($"{listData.ActionListProperty.arraySize} elements");
                var name = data?.Name ?? listData.ActionListProperty.name;
                var add = GUILayout.Button($"+ Add to {name}");
                GUILayout.FlexibleSpace();

                var move = mod.MoveIdxA != -1 && mod.MoveIdxB != -1;
                var delete = mod.DeleteIdx != -1;
                var cutPaste = mod.CutPasteIdx != -1;
                var setBehaviour = mod.SetIdx != -1;

                var any = add || move || delete || cutPaste;
                if (any)
                    contextAction?.Invoke();

                if (add)
                    OnAdd(ref listData);
                else if (move)
                    OnMove(ref listData, mod.MoveIdxA, mod.MoveIdxB);
                else if (delete)
                    OnDelete(ref listData, mod.DeleteIdx);
                else if (cutPaste)
                    OnCutPaste(ref listData, mod.CutPasteIdx);
                else if (setBehaviour)
                    PopupWindow.Show(menuData.ButtonRect, menuData.Menu);

                if (any)
                    OnChanged();
            }

            VerificationGUI();

            GUILayout.Space(10);
        }

        void SequenceElementGUI(ref CommonActionEditorGUI.ActionListData listData, int i, 
            ref CommonActionEditorGUI.ActionListModifications mod, ref CommonActionEditorGUI.ActionMenuData menuData)
        {
            var data = listData.GUIData ?? Target as IGUIData;
            var col = data?.GUIColor ?? default;

            var listProp = listData.ActionListProperty;
            var elementProp = listProp.GetArrayElementAtIndex(i);

            var it = new CommonActionEditorGUI.ActionListIterator
            {
                Idx = i,
                Action = elementProp.objectReferenceValue as ScriptableBaseAction,
                ActionProperty = elementProp,
                Count = listProp.arraySize
            };
            using (new EditorGUILayout.HorizontalScope())
            {
                var hasCustomLabel = !listData.CustomLabels.IsNullOrEmpty() && listData.CustomLabels.IsIndexInRange(i);
                    using (new ColorScope(col))
                {
                    if (!hasCustomLabel)
                        GUILayout.Label(string.Format(listData.LabelFormat, i, i + 1), GUILayout.Width(25f));
                }

                using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    if (hasCustomLabel)
                        GUILayout.Label(listData.CustomLabels[i]);

                    CommonActionEditorGUI.DefaultActionListElementHeader(ref listData, ref it, ref mod, ref menuData);
                    NestedEditorGUI(ref listData, i, it.ActionProperty, it.Action);
                }
            }
        }
        #endregion
    }
}
