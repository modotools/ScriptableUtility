﻿using System;
using System.Collections.Generic;
using Core.Editor.Inspector;
using Core.Editor.Utility;
using Core.Unity.Extensions;
using Core.Unity.Types;
using ScriptableUtility.ActionConfigs;
using ScriptableUtility.Behaviour;
using ScriptableUtility.Editor.Obsolete;
using UnityEditor;
using UnityEngine;
using UEditor = UnityEditor.Editor;

namespace ScriptableUtility.Editor.Old_FSM
{
    [CustomEditor(typeof(Old_FSMActionBehaviour))]
    public class Old_FSMActionBehaviourInspector : BaseInspector<Old_FSMActionBehaviourEditor>{}

    public class Old_FSMActionBehaviourEditor : BaseEditor<Old_FSMActionBehaviour>
    {
        enum ContextActionType
        {
            Action, UpdateAction, ExitAction, None
        }

        Old_FSMActionBehaviour m_lastTarget;
        string m_path;

        readonly List<UEditor> m_cachedActionEditors = new List<UEditor>();
        readonly List<UEditor> m_cachedUpdateActionEditors = new List<UEditor>();
        readonly List<UEditor> m_cachedExitActionEditors = new List<UEditor>();

        GenericMenu m_allActionTypeMenu;
        GenericMenu m_defaultActionTypeMenu;

        ContextActionType m_ctxActionType;

        bool AllActionsForMenu => m_ctxActionType == ContextActionType.UpdateAction ||
                           m_ctxActionType == ContextActionType.None;

        public override void OnGUI(float width)
        {
            base.OnGUI(width);
            if (m_lastTarget != Target)
                InitTarget();
            
            if (Target == null)
                return;

            using (var check = new EditorGUI.ChangeCheckScope())
            {
                ActionsGUI(nameof(Old_FSMActionBehaviour.Actions), Target.Actions, m_cachedActionEditors, ContextActionType.Action);
                ActionsGUI(nameof(Old_FSMActionBehaviour.UpdateActions), Target.UpdateActions, m_cachedUpdateActionEditors, ContextActionType.UpdateAction);
                ActionsGUI(nameof(Old_FSMActionBehaviour.ExitActions), Target.ExitActions, m_cachedExitActionEditors, ContextActionType.ExitAction);

                if (check.changed)
                    EditorUtility.SetDirty(Target);
            }

            AddContainerGUI();
            AddActionsButtonGUI("Add Action", ContextActionType.None);
        }

        void AddContainerGUI()
        {
            if (!GUILayout.Button("Add ScriptableContainer"))
                return;
            var container = ScriptableObject.CreateInstance(typeof(ScriptableContainer));
            container.name = "VariableContainer";
            AssetDatabase.AddObjectToAsset(container, AssetDatabase.GetAssetPath(Target));
        }

        #region Init

        public override void Init(object parentContainer)
        {
            base.Init(parentContainer);

            m_defaultActionTypeMenu = TypeHelper.InitActionTypeMenuDefaultOnly(OnActionTypeSelected);
            m_allActionTypeMenu = TypeHelper.InitActionTypeMenuAll(OnActionTypeSelected);
        }

        public override void Terminate()
        {
            base.Terminate();
            ClearTarget();
        }

        void ClearTarget()
        {
            m_lastTarget = null;
            m_path = null;
        }

        void InitTarget()
        {
            m_lastTarget = Target;
            if (Target == null)
            {
                ClearTarget();
                return;
            }

            m_path = AssetDatabase.GetAssetPath(Target);
            var ctrl = (RuntimeAnimatorController) AssetDatabase.LoadAssetAtPath(m_path, typeof(RuntimeAnimatorController));
            Target.MFinishTrigger = new AnimatorTriggerRef(ctrl, Target.MFinishTrigger.ParameterName); 
            InitCachedEditors();
        }

        void InitCachedEditors()
        {
            m_cachedActionEditors.Clear();
            foreach (var a in Target.Actions) 
                m_cachedActionEditors.Add(UEditor.CreateEditor(a.Config));

            m_cachedUpdateActionEditors.Clear();
            foreach (var a in Target.UpdateActions) 
                m_cachedUpdateActionEditors.Add(UEditor.CreateEditor(a.Config));

            m_cachedExitActionEditors.Clear();
            foreach (var a in Target.ExitActions) 
                m_cachedExitActionEditors.Add(UEditor.CreateEditor(a.Config));
        }

        #endregion

        void OnActionTypeSelected(object actionType)
        {
            var t = (Type) actionType;
            var actionConfig = ScriptableObject.CreateInstance(t) as ScriptableBaseAction;
            AssetDatabase.AddObjectToAsset(actionConfig, m_path);
            var editor = UEditor.CreateEditor(actionConfig);

            switch (m_ctxActionType)
            {
                case ContextActionType.Action:
                    Target.Actions.Add(new OldFSMAction(actionConfig));
                    m_cachedActionEditors.Add(editor);
                    break;
                case ContextActionType.UpdateAction:
                    Target.UpdateActions.Add(new OldFSMUpdateAction(actionConfig));
                    m_cachedUpdateActionEditors.Add(editor);
                    break;
                case ContextActionType.ExitAction:
                    Target.ExitActions.Add(new OldFSMExitAction(actionConfig));
                    m_cachedExitActionEditors.Add(editor);
                    break;
                case ContextActionType.None: break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            SerializedObject.Update();
        }


        struct ActionUIControlData
        {
            public bool MoveUpPossible;
            public bool MoveDownPossible;
            public bool MoveUpClicked;
            public bool MoveDownClicked;
            public bool DeleteClicked;
        }

        void ActionsGUI<T>(string propertyName, IList<T> actions, IList<UEditor> editors, ContextActionType actionType) where T : IOldFSMActionData
        {
            AddActionsButtonGUI(propertyName, actionType);

            var deleteIdx = -1;
            var swapAIdx = -1;
            var swapBIdx = -1;
            var actionsProp = SerializedObject.FindProperty($"{propertyName}");
            for (var i = 0; i < actions.Count; i++)
            {
                var actionProp = actionsProp.GetArrayElementAtIndex(i);
                var config = actions[i].Config;
                if (config != null && !string.Equals(config.Name, config.name))
                    config.name = config.Name;
                var editor = editors[i];

                var control = new ActionUIControlData() { MoveDownPossible = i + 1 < actions.Count, MoveUpPossible = i != 0 };
                ActionGUI(actionProp, config, editor, ref control);
                UIControlToIdx(control, i, ref deleteIdx, ref swapAIdx, ref swapBIdx);
            }

            UIControlAction(actions, editors, deleteIdx, swapAIdx, swapBIdx);
        }

        void AddActionsButtonGUI(string propertyName, ContextActionType actionType)
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                EditorGUILayout.LabelField($"{propertyName}: ", EditorStyles.boldLabel);
                if (!GUILayout.Button("+"))
                    return;

                m_ctxActionType = actionType;
                var menu = AllActionsForMenu
                    ? m_allActionTypeMenu
                    : m_defaultActionTypeMenu;
                menu.ShowAsContext();
            }
        }

        void UIControlAction<T>(IList<T> actions, IList<UEditor> editors, int deleteIdx, int swapAIdx, int swapBIdx) where T : IOldFSMActionData
        {
            if (deleteIdx != -1)
            {
                // todo 1 FSM 3: we don't guarantee, that asset is not re-used!
                actions[deleteIdx].Config.DestroyEx();
                actions.RemoveAt(deleteIdx);

                editors[deleteIdx].DestroyEx();
                editors.RemoveAt(deleteIdx);

                SerializedObject.Update();
            }
            else if (swapAIdx != -1)
            {
                var temp = actions[swapAIdx];
                actions[swapAIdx] = actions[swapBIdx];
                actions[swapBIdx] = temp;

                var tempEditor = editors[swapAIdx];
                editors[swapAIdx] = editors[swapBIdx];
                editors[swapBIdx] = tempEditor;

                SerializedObject.Update();
            }
        }

        static void UIControlToIdx(ActionUIControlData control, int i, ref int deleteIdx, ref int swapAIdx, ref int swapBIdx)
        {
            if (control.DeleteClicked)
                deleteIdx = i;
            else if (control.MoveUpClicked)
            {
                swapAIdx = i;
                swapBIdx = i - 1;
            }
            else if (control.MoveDownClicked)
            {
                swapAIdx = i;
                swapBIdx = i + 1;
            }
        }

        void ActionGUI(SerializedProperty actionProp, ScriptableBaseAction action, UEditor editor, 
            ref ActionUIControlData uiControl)
        {
            using (new EditorGUILayout.VerticalScope(EditorStyles.helpBox))
            {
                ActionGUIHeader(actionProp, action.Name, ref uiControl);

                if (!actionProp.isExpanded)
                    return;
                GUILayout.Space(5);

                var childProps = actionProp.GetEnumerator();

                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    while (childProps.MoveNext())
                        EditorGUILayout.PropertyField(childProps.Current as SerializedProperty, true);
                    
                    if (check.changed)
                    {
                        SerializedObject.ApplyModifiedProperties();
                        SerializedObject.Update();
                    }
                }

                //EditorGUILayout.PropertyField(actionProp, true);
                CustomGUI.HSplitter();
                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    editor.OnInspectorGUI();

                    if (check.changed)
                        SerializedObject.Update();
                }
            }
        }

        static void ActionGUIHeader(SerializedProperty actionProp, string actionName, ref ActionUIControlData uiControl)
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                uiControl.DeleteClicked = GUILayout.Button("X", GUILayout.Width(20));
                GUILayout.Space(20);
                actionProp.isExpanded = EditorGUILayout.Foldout(actionProp.isExpanded, actionName, true);

                using (new EditorGUI.DisabledScope(!uiControl.MoveUpPossible))
                    uiControl.MoveUpClicked = GUILayout.Button("^", GUILayout.Width(20));

                using (new EditorGUI.DisabledScope(!uiControl.MoveDownPossible))
                    uiControl.MoveDownClicked = GUILayout.Button("v", GUILayout.Width(20));
            }
        }
    }
}
