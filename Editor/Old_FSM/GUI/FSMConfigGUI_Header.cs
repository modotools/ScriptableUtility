using Core.Editor.Utility;
using UnityEditor;
using UnityEngine;

namespace ScriptableUtility.Editor.Old_FSM.GUI
{
    public partial class FSMConfigGUI
    {
        const float k_headerHeight = 16f;
        Rect m_headerRect;
        void InitHeaderRect(Rect drawRect) => m_headerRect = new Rect(0, 0, drawRect.width, k_headerHeight);

        void HeaderGUI()
        {
            if (m_isNested)
                return;
            using (new GUILayout.AreaScope(m_headerRect, "", EditorStyles.toolbar))
            {
                using (new GUILayout.HorizontalScope())
                {
                    using (new GUILayout.HorizontalScope(GUILayout.Width(m_grid.DrawRect.width - 14)))
                    {
                        GUILayout.Label("Header1");
                    }
                    CustomGUI.VSplitter();
                    using (new GUILayout.HorizontalScope(GUILayout.Width(m_sideBarWidth)))
                    {
                        GUILayout.Label("Header2");
                    }
                }
            }
        }

    }
}