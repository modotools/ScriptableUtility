﻿using Core.Editor.Interface.GUI;
using Core.Editor.Utility.NodeGUI;
using UnityEditor;
using UnityEngine;

namespace ScriptableUtility.Editor.Old_FSM.GUI.Elements
{
    public class FSM_GUIEventNode : IGUISelectable, IGUIConnectable, IGUIWithText, IGUIWithContextMenu
    {
        internal static readonly Vector2 K_DefaultSize = new Vector2(200, 20);
        public string Name;

        public Vector2 Pos => m_state.Pos + (1+m_posIdx) * K_DefaultSize.y * Vector2.up;
        public Vector2 Size => K_DefaultSize;
        public string Text { get => Name; set => Name = value; }
        public GUIStyle Style { get; set; }
        public bool IsSelected { get; set; }

        readonly IGUIElement m_state;
        int m_posIdx;

        public ConnectionPos IncomingPos => ConnectionPos.None;
        public ConnectionPos OutgoingPos => ConnectionPos.Right;
        public GenericMenu ContextMenu { get; }

        public int TargetIdx;

        public FSM_GUIEventNode(IGUIElement state, int targetIdx, string name, GUIStyle nodeStyle, GUIElementContextMenuFactory contextMenu = null)
        {
            m_state = state;

            TargetIdx = targetIdx;
            Name = name;

            Style = nodeStyle;
            IsSelected = false;

            ContextMenu = contextMenu?.CreateMenu(this);
        }
    }
}