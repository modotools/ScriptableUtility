﻿using Core.Editor.Interface.GUI;
using Core.Editor.Utility.NodeGUI;
using ScriptableUtility.FSM.Old_FSM;
using UnityEditor;
using UnityEngine;

namespace ScriptableUtility.Editor.Old_FSM.GUI.Elements
{
    public class FSM_GUIStateNode : IGUISelectable, IGUIConnectable, IGUIDraggable, IGUIWithText, IGUIWithContextMenu
    {
        internal static readonly Vector2 K_DefaultSize = new Vector2(200, 20);
        public string Name;

        public Vector2 Pos { get; set; }
        public Vector2 Size => K_DefaultSize;
        public string Text { get => Name; set => Name = value; }
        public GUIStyle Style { get; set; }
        public bool IsSelected { get; set; }

        public ConnectionPos IncomingPos => ConnectionPos.Left;
        public ConnectionPos OutgoingPos => ConnectionPos.None;
        public GenericMenu ContextMenu { get; }

        public OldFSMConfig.State SerializedState;

        public FSM_GUIStateNode(OldFSMConfig.State state, GUIStyle nodeStyle, GUIElementContextMenuFactory contextMenu = null)
        {
            SerializedState = state;
            Name = state.Name;
            Pos = state.LastGraphPos;
            Style = nodeStyle;
            IsSelected = false;

            ContextMenu = contextMenu?.CreateMenu(this);
        }
    }
}