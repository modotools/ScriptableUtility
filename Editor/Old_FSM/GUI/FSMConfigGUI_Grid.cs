using Core.Editor.Utility.NodeGUI;
using UnityEditor;
using UnityEngine;
using GUITools = Core.Unity.Utility.GUITools.GUITools;

namespace ScriptableUtility.Editor.Old_FSM.GUI
{
    public partial class FSMConfigGUI
    {
        internal static class GridContextStrings
        {
            internal const string AddState = "Add State";

            internal const string Copy = "Copy";
            internal const string Paste = "Paste";
            internal const string AddGlobalTransition = "Add Global Transition";

            internal const string AddLayer = "Add Layer";
            internal const string RemoveLayer = "Remove Layer";

            internal const string Settings = "Settings";

        }

        // the grid
        readonly GUIGrid m_grid = new GUIGrid()
        {
            CoarseGridSpacing = 100,
            CoarseGridOpacity = 0.5f,
            GridColor = Color.gray,
            Zoom = 1f
        };
        // ---

        // zoom:
        float m_zoom = 1f;
        const float k_minZoom = 0.25f;
        const float k_maxZoom = 4f;
        // ----
        static readonly Color k_selectionRectColor = new Color(0.75f, 0.5f, 0f, 1f);
        static readonly Color k_selectionRectColorFill = new Color(0.75f, 0.5f, 0f, 0.2f);

        GenericMenu m_gridMenu;

        public void SetZoomFactor(float zoom = 1f) =>
            m_grid.Zoom = Mathf.Clamp(m_zoom * zoom, k_minZoom, k_maxZoom);

        void InitGridContextMenu()
        {
            m_gridMenu = new GenericMenu();

            m_gridMenu.AddItem(new GUIContent(GridContextStrings.AddState), false, OnAddNewState);
            m_gridMenu.AddItem(new GUIContent(GridContextStrings.AddGlobalTransition), false, OnAddGlobalTransition);

            m_gridMenu.AddItem(new GUIContent(GridContextStrings.Copy), false, OnCopy);
            m_gridMenu.AddItem(new GUIContent(GridContextStrings.Paste), false, OnPaste);

            m_gridMenu.AddItem(new GUIContent(GridContextStrings.AddLayer), false, OnAddLayer);
            m_gridMenu.AddItem(new GUIContent(GridContextStrings.RemoveLayer), false, OnRemoveLayer);

            m_gridMenu.AddItem(new GUIContent(GridContextStrings.Settings), false, OnSettings);
        }

        void GridAreaGUI()
        {
            using (new GUILayout.AreaScope(m_grid.DrawRect))
            {
                m_grid.DrawGrid();
                DrawNodes();
                SelectedStateNodeGUI();
                if (!m_grid.Selecting) 
                    return;

                var sel = m_grid.CurrentSelection;
                GUITools.DrawRect(sel, k_selectionRectColorFill);
                GUITools.DrawRect(new Rect(sel.x, sel.y, sel.width,1), k_selectionRectColor);
                GUITools.DrawRect(new Rect(sel.x, sel.y, 1, sel.height), k_selectionRectColor);
                GUITools.DrawRect(new Rect(sel.x+sel.width, sel.y, 1, sel.height), k_selectionRectColor);
                GUITools.DrawRect(new Rect(sel.x, sel.y+sel.height, sel.width, 1), k_selectionRectColor);
            }
        }

        void ProcessZoom()
        {
            if (Event.current == null)
                return;
            if (Event.current.type != EventType.ScrollWheel)
                return;
            var zoomDelta = 0.01f * Event.current.delta.y;
            // clamp zoom
            var newZoom = Mathf.Clamp(m_zoom - zoomDelta, k_minZoom, k_maxZoom);
            // actual change after clamping 
            zoomDelta = m_zoom - newZoom;
            // apply zoom
            m_zoom = newZoom;
            m_grid.Zoom = m_zoom;

            // todo 1 FSM 3: feature zoom-out mouse influence
            if (zoomDelta > 0)
                zoomDelta *= 0.25f;
            //if (zoomDelta < 0)
            //{
            var gridSize = m_grid.DrawRect.size;
            var halfGridSize = 0.5f * gridSize;
            var zoomedSize = gridSize / m_zoom;
            var mp = Event.current.mousePosition - halfGridSize;
            var relativeMouse = new Vector2(mp.x / halfGridSize.x, mp.y / halfGridSize.y);
            var mouseInfluence = -Mathf.Abs(zoomDelta) * new Vector2(zoomedSize.x * relativeMouse.x,
                                     zoomedSize.y * relativeMouse.y);
            m_grid.Offset += mouseInfluence;
            //}

            Event.current.Use();
            m_repaintable.Repaint();
        }

        void ProcessContextMenu()
        {
            const int rightClick = 1;
            if (Event.current.type != EventType.MouseDown || Event.current.button != rightClick)
                return;
            m_lastMouseClickPos = Event.current.mousePosition.FromGUIPos(m_grid);
            m_gridMenu.ShowAsContext();
        }

        public void ProcessGrid()
        {
            var mouseContained = m_grid.EventRect.Contains(Event.current.mousePosition);
            // ReSharper disable once InvertIf
            if (mouseContained)
            {
                ProcessZoom();
                ProcessNestedConfigGUI();
                ProcessNodeEvents();
                ProcessContextMenu();

                if (m_draggedNode == null)
                    m_grid.UpdateDragGrid();
                if (m_grid.Selecting)
                    UpdateGridSelection();
            }
            //if (m_draggedNode == null)
            //    m_grid.UpdateDragGrid();
            //if (m_grid.Selecting)
            //    UpdateGridSelection();
        }

        void UpdateGridSelection()
        {
            m_selectedNodes.Clear();
            
            var selectionRect = m_grid.GetNodeSelection();
            foreach (var n in m_stateNodes)
            {
                if (selectionRect.Contains(n.Pos + 0.5f*n.Size))
                    m_selectedNodes.Add(n);
            }
        }
    }
}