using System;
using System.Collections.Generic;
using System.Linq;
using Core.Editor.Interface.GUI;
using Core.Editor.Utility.GUIStyles;
using Core.Editor.Utility.NodeGUI;
using Core.Extensions;
using Core.Unity.Extensions;
using ScriptableUtility.FSM.Old_FSM;
using ScriptableUtility.Editor.Old_FSM.GUI.Elements;
using UnityEngine;

namespace ScriptableUtility.Editor.Old_FSM.GUI
{
    public partial class FSMConfigGUI
    {
        internal static class NodeContextStrings
        {
            internal const string AddConnection = "Add Connection";
            internal const string DeleteStates = "Delete States";
        }


        // context menu
        readonly GUIElementContextMenuFactory m_contextMenuFactory = new GUIElementContextMenuFactory();
        // current elements
        readonly List<FSM_GUIStateNode> m_stateNodes = new List<FSM_GUIStateNode>();
        readonly List<FSM_GUIEventNode> m_stateEvents = new List<FSM_GUIEventNode>();
        readonly List<FSM_GUIGlobalEventMarker> m_globalEventMarkers = new List<FSM_GUIGlobalEventMarker>();
        readonly List<GUIConnection> m_connections = new List<GUIConnection>();

        // ---

        // selection & editing:
        readonly List<IGUISelectable> m_selectedNodes = new List<IGUISelectable>();
        IGUIDraggable m_draggedNode;


        // properties
        FSM_GUIStateNode SelectedNode => m_selectedNodes.IsNullOrEmpty() || m_selectedNodes.Count > 1 ? null : m_selectedNodes[0] as FSM_GUIStateNode;

        ref OldFSMConfig.State SelectedState => ref SelectedNode.SerializedState;
        // ----

        void OnAddNewState()
        {
            if (m_stateNodes == null)
                return;

            var state = new OldFSMConfig.State() { Name = "New State", LastGraphPos = m_lastMouseClickPos.Vector2Int() };
            m_stateNodes.Add(new FSM_GUIStateNode(state, SquareNodes.DarkGray, m_contextMenuFactory));
        }

        void PasteStates()
        {
            throw new System.NotImplementedException();
        }

        void InitTarget()
        {
            m_stateNodes.Clear();
            m_globalEventMarkers.Clear();
            m_connections.Clear();

            GetData(out var states, out var globalEvents);

            foreach (var s in states)
                m_stateNodes.Add(new FSM_GUIStateNode(s, SquareNodes.DarkGray, m_contextMenuFactory));

            foreach (var ge in globalEvents)
            {
                var marker = new FSM_GUIGlobalEventMarker(ge, m_stateNodes[ge.StateIdxTo]);
                m_globalEventMarkers.Add(marker);
                m_connections.Add(new GUIConnection() { NodeFrom = marker, NodeTo = m_stateNodes[0], Color = Color.white });
            }
        }

        void GetData(out List<OldFSMConfig.State> states, out List<OldFSMConfig.EventData> globalEvents)
        {
            states = Target.States.ToList();
            if (states.IsNullOrEmpty())
                states.Add(new OldFSMConfig.State() { Name = "Default" });
            globalEvents = Target.GlobalEvents.ToList();
            if (globalEvents.IsNullOrEmpty())
                globalEvents.Add(new OldFSMConfig.EventData() { Event = "Start", Condition = null, StateIdxTo = 0 });
        }

        void InitNodeActions()
        {
            m_contextMenuFactory.GUIElementActions.Clear();
            m_contextMenuFactory.GUIElementActions.Add(NodeContextStrings.AddConnection, AddConnection);
            m_contextMenuFactory.GUIElementActions.Add(NodeContextStrings.DeleteStates, DeleteStates);
        }

        void DrawNodes()
        {
            foreach (var ge in m_globalEventMarkers)
                ge.DrawElement(m_grid);
            foreach (var e in m_stateEvents)
                e.DrawElement(m_grid);
            foreach (var n in m_stateNodes)
                n.DrawElement(m_grid);
            foreach (var c in m_connections)
                c.Draw(m_grid);
        }

        void ProcessNodeEvents()
        {
            foreach (var n in m_stateNodes)
                n.ProcessEvents(m_grid, m_selectedNodes, ref m_draggedNode);

            UpdateSelectedState();
        }
        void AddConnection(IGUIElement guiStateNode)
        {
            //var pos = guiStateNode.Pos + new Vector2(0, guiStateNode.Size.y - 2);
            m_stateEvents.Add(new FSM_GUIEventNode(guiStateNode, -1, "", SquareNodes.DarkGray));
        }

        void DeleteStates(IGUIElement guiStateNode)
        {
            if (!m_selectedNodes.Contains(guiStateNode))
            {
                Debug.LogError("This should not happen");
                return;
            }
            foreach (var n in m_selectedNodes)
            {
                var fsm_guiStateNode = (FSM_GUIStateNode) n;
                var guiNodeIdx = m_stateNodes.FindIndex(item => item == fsm_guiStateNode);
                m_stateNodes.RemoveAt(guiNodeIdx);
            }
            m_selectedNodes.Clear();
        }

        void OnAddGlobalTransition()
        {
            throw new NotImplementedException();
        }
    }
}