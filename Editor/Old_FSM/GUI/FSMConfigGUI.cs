using Core.Editor.Interface.GUI;
using Core.Unity.Utility.GUITools;
using ScriptableUtility.FSM.Old_FSM;
using UnityEngine;

namespace ScriptableUtility.Editor.Old_FSM.GUI
{
    public partial class FSMConfigGUI
    {
        public FSMConfigGUI(OldFSMConfig target, IGUIRepaintable repaintTarget, bool isNested = false)
        {
            Target = target;
            m_repaintable = repaintTarget;
            m_isNested = isNested;

            if (Target == null)
                return;

            InitNodeActions();
            InitTarget();
            InitGridContextMenu();
        }

        // current context
        public OldFSMConfig Target { get; }
        readonly IGUIRepaintable m_repaintable;
        // ---

        //Rect m_drawRectFull;
        Vector2 m_lastMouseClickPos;
        bool m_selectionRect;
        public void SetGUIRects(Rect drawRect, Rect parentEventRect)
        {
            //m_drawRectFull = drawRect;
            var gridRect = !m_isNested ? InitAllRects(drawRect)
                : drawRect;

            m_grid.DrawRect = gridRect;
            m_grid.EventRect = GetEventRect(gridRect, parentEventRect);
        }
        Rect InitAllRects(Rect drawRect)
        {
            const float topBottomSpace = FSMConfigGUI.k_headerHeight + FSMConfigGUI.k_footerHeight;
            var gridRect = new Rect(drawRect.x, drawRect.y + FSMConfigGUI.k_headerHeight, drawRect.width - m_sideBarWidth, drawRect.height - topBottomSpace);
            InitHeaderRect(drawRect);
            InitFooterRect(drawRect);
            InitSidebarRect(gridRect);
            return gridRect;
        }
        static Rect GetEventRect(Rect gridRect, Rect parentEventRect)
        {
            var evPos = parentEventRect.position + gridRect.position;
            var maxSize = parentEventRect.size - gridRect.position;
            var evSize = new Vector2(Mathf.Min(maxSize.x, gridRect.size.x), Mathf.Min(maxSize.y, gridRect.size.y));
            var evRect = new Rect(evPos, evSize);

            return evRect;
        }

        public void OnGUI()
        {
            GUITools.DrawRect(m_grid.DrawRect, Color.black);

            GridAreaGUI();
            SidebarGUI();
            HeaderGUI();
            FooterGUI();
        }
        public void ProcessGUI() => ProcessGrid();

        void OnCopy()
        {
            //todo 1 FSM 3: copy selected states or state machine if none selected
            throw new System.NotImplementedException();
        }
        void OnPaste()
        {
            //todo 1 FSM 3: figure out whether we have states or state machine copied
            throw new System.NotImplementedException();
        }

        void OnAddLayer()
        {
            //todo 1 FSM 3: layers
            throw new System.NotImplementedException();
        }

        void OnRemoveLayer()
        {
            //todo 1 FSM 3: layers
            throw new System.NotImplementedException();
        }

        void OnSettings()
        {
            //todo 1 FSM 3: settings
            throw new System.NotImplementedException();
        }
    }
}