using Core.Editor.Utility.GUIStyles;
using UnityEngine;

namespace ScriptableUtility.Editor.Old_FSM.GUI
{
    public partial class FSMConfigGUI
    {
        Rect m_sideBarRect;
        float m_sideBarWidth = 256f;

        void InitSidebarRect(Rect gridRect) => m_sideBarRect = new Rect(gridRect.width, k_headerHeight, m_sideBarWidth, gridRect.height);

        void SidebarGUI()
        {
            if (m_isNested)
                return;
            using (new GUILayout.AreaScope(m_sideBarRect, "!", CustomStyles.Gray))
            {
                using (new GUILayout.VerticalScope())
                {

                }
            }
        }
    }
}